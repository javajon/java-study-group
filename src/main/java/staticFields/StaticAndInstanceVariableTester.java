package staticFields;

import collections.ArrayListTester;
import org.apache.log4j.Logger;
import zoo.animal.Zebra;

import java.time.LocalDate;

/**
 * StaticAndInstanceVariableTester - used to demonstrate static and non-static members
 * <p>
 * Created by jhead on 05/12/2017.
 * <p>
 * Uses of static methods: -
 * <p>
 * 1. To provide an entry point into a program without requiring an instance of the class (main method).
 * <p>
 * 2. For utility or helper methods that don’t require any object state.
 * Since there is no need to access instance variables,
 * having static methods eliminates the need for the caller
 * to instantiate the object just to call the method.
 * See also com.jon.javastudy.zoo.data.AnimalListDataManager
 * <p>
 * 3. For state that is shared by all instances of a class, like a counter on websites.
 * All instances must share the same state.
 * Methods that merely use that state should be static as well.
 * See also com.jon.javastudy.zoo.animal.Animal
 * The variable refNoSequence does this.
 */
public class StaticAndInstanceVariableTester {

    //Logger is static - one instance is owned by all instances of the class - logging co-ordinated.

    final static Logger logger = Logger.getLogger(StaticAndInstanceVariableTester.class);

    //A major use of static final variables is for representing constants.
    public static final LocalDate START_DATE_OF_FRENCH_REVOLUTION = LocalDate.of(1789, 5, 5);

    //This variable can be changed as it is not final.
    static int ONE_HUNDRED = 100; // static variable
    int dayOfMonth = 5; // instance variable

    public static void main(String[] args) {      // static method
        logger.info("\nThe French Revolution started on " + START_DATE_OF_FRENCH_REVOLUTION);
        logger.info(ONE_HUNDRED);

        //Does not compile - non-static variables cannot be accessed statically.
        //logger.debug(dayOfMonth);

        //How do you reference dayOfMonth here?
        //By creating an instance.
        StaticAndInstanceVariableTester tester = new StaticAndInstanceVariableTester();
        logger.info(tester.dayOfMonth); //Better with a getter.
        //The static can be referenced from all references and from the class name itself.

        //Note that the main method can be called programmatically.
        //The following has the same effect as running Zebra directly.
        Zebra.main(null);
        Zebra.main(new String[] {"Hello World"});  //Fine but args is ignored.

        //Zebra has a static count variable, incremented each time we instantiate a Zebra.
        //It has static methods to interact with this, which we can call here.
        Zebra.printCount();

        //Create some instances and call this method statically each time.
        Zebra zane = new Zebra("zane");
        Zebra.printCount();
        Zebra sue = new Zebra("sue");
        Zebra.printCount();

        //What if we call this via an instance (not best practice).
        zane.printCount();
        sue.printCount();

        //Create another instance to demonstrate further.
        Zebra jo = new Zebra("jo");

        //Call the static method via static and instance references.
        Zebra.printCount();
        zane.printCount();
        sue.printCount();
        jo.printCount();

        //So static methods can be called from instance methods (although not ideal),
        //but the reverse is not true: -
        //JH - make the relevant members in Zebra static and then re-run.
        //Zebra.printMyName(); //DOES NOT COMPILE - an individual animal's name means nothign at a class level.
        zane.printMyName();;
        sue.printMyName();
        jo.printMyName();

        //JH - add Gorilla class for next session.



    }
}
