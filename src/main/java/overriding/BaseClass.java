package overriding;

import exception.InvalidAnimalException;
import zoo.animal.Animal;
import zoo.data.AnimalListDataManager;

/**
 * Created by rc00028 on 27/06/2017.
 */
public class BaseClass {

    int myInt = 21;

    protected Animal getAnimal(String type) throws InvalidAnimalException {
        System.out.println("getAnimal in B_ParentClass");
        Animal animal = AnimalListDataManager.getAnimal(type);
        if (animal == null){
            throw new InvalidAnimalException(type + " is an invalid animal. There are no animals of that type in this zoo.");
        }
        return animal;
    }

    public static void publicStaticMethod() {
        System.out.println("publicStaticMethod in B_ParentClass");
    }

    private void privateMethod() {
        System.out.println("privateMethod in B_ParentClass");
    }

    public final boolean hasFeathers() {
        return true;
    }

    public int getMyInt() {
        return myInt;
    }

    public int getBaseClassInt() {
        return myInt;
    }
}
