package overriding;

import exception.InvalidAnimalException;
import zoo.animal.Animal;

/**
 * OverridingTester
 * <p>
 * RULES OF METHOD OVERRIDING
 * <p>
 * 1. The method in the child class must have the same signature as the method in the parent class.
 * 2. The method in the child class must be at least as accessible or more accessible than the method in the parent class.
 * 3. The method in the child class may not throw a checked exception that is new or broader than the class of any exception thrown in the parent class method.
 * 4. If the method returns a value, it must be the same or a subclass of the method in the parent class, known as covariant return types.
 * 5. The method defined in the child class must be marked as static if it is marked as static in the parent class (method hiding).
 * Likewise, the method must not be marked as static in the child class if it is not marked as static in the parent class (method overriding).
 * <p>
 * HIDING STATIC METHODS
 * Though you might see questions on the exam that contain hidden static methods that are syntactically correct,
 * avoid hiding static methods in your own work, since it tends to lead to confusing and difficult-to-read code.
 * You should not reuse the name of a static method in your class if it is already used in the parent class.
 * <p>
 * HIDING VARIABLES
 * Although Java allows you to hide a variable deﬁned in a parent class with one deﬁned in a child class,
 * it is considered an extremely poor coding practice
 * <p>
 * Created by rc00028 on 15/06/2017.
 */

public class OverridingTester extends BaseClass {

    //Variables cannot be overridden, but they can be hidden - i.e. a copy exists in both parent and child class
    //and the one in the child class hides the ones in the parent class.

    //@Override //DOES NOT COMPILE
    int myInt = 42;


    @Override
    protected Animal getAnimal(String type) throws InvalidAnimalException {
        //Due to rule 2, access could be public or protected but not default or private.
        System.out.println("getAnimal in OverridingTester");

        //The overridden method can be called using super.
        try {
            return super.getAnimal(type);
        } catch (InvalidAnimalException e) {
            e.printStackTrace();
        }

        //But beware of this. Infinite loop and or java.lang.StackOverflowError would result: -
        //return getAnimal(type);

        //Aside - you can always return null from a method that returns an object.
        return null;
    }

    //Note that is this method in static in parent and child class, this is not considered as overriding, but hiding.
    //@Override  //DOES NOT COMPILE
    public static void publicStaticMethod() {
        System.out.println("publicStaticMethod in OverridingTester");
    }

    //Note that you cannot override a private method due to accessibility rules. However, you can define a method with exactly the same signature.
    //@Override  //DOES NOT COMPILE
    private void privateMethod() {
        System.out.println("privateMethod in B_ParentClass");
    }

    @Override
    public int getMyInt() {
        return myInt;
    }

    //Note that you cannot override a final method. The following does not compile.
    /*
    @Override
    public final boolean hasFeathers() {
        return true;
    }
    */


    public static void main(String[] args) {

        OverridingTester overridingTester = new OverridingTester();

        //overridingTester.getAnimal("elephant");
        Animal animal = null;
        try {
            animal = overridingTester.getAnimal("elephant");
        } catch (InvalidAnimalException e) {
            e.printStackTrace();
        }
        System.out.println("Hello there. I am " + animal.getName() + " and I am an " + animal.getType());

        //Static methods do not "override" methods in the base class, they "hide" them.
        publicStaticMethod();
        BaseClass.publicStaticMethod();
        OverridingTester.publicStaticMethod();

        System.out.println("overridingTester.myInt: " + overridingTester.myInt);
        System.out.println("overridingTester.getMyInt(): " + overridingTester.getMyInt());
        System.out.println("overridingTester.getBaseClassInt(): " + overridingTester.getBaseClassInt());

    }

}
