package wrappers;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * IntegerTester
 * <p>
 * Created by jon on 05/06/2017.
 * <p>
 * This class demonstrates usage of java.lang.Integer, which representative
 * of all the Java Wrapper classes. Each primitive type has a corresponding wrapper class.
 * <p>
 * Wrapper classes
 * <p>
 * Integer
 * Short
 * Long
 * Byte
 * Float
 * Double
 * Character
 * Boolean
 * <p>
 * Each of these wraps the underlying primitive data type.
 * <p>
 * Table 3.2 in page 134 of the study guide shows the Wrapper classe.
 * <p>
 *
 */
public class IntegerTester {

    final static Logger logger = Logger.getLogger(IntegerTester.class);

    public static void main(String[] args) {

        String fiveAsString = "5";
        int fiveAsInt;

        //parseInt method.
        fiveAsInt = Integer.parseInt(fiveAsString);

        //Wrapper constructor.
        Integer fiveAsWrapper = new Integer(fiveAsInt);

        //These all print the same - a String, int primitive and Integer
        logger.info("fiveAsString: " + fiveAsString);
        logger.info("fiveAsInt: " + fiveAsInt);
        logger.info("fiveAsWrapper: " + fiveAsWrapper);

        //valueOf method (this acts just the same as previous example)
        fiveAsWrapper = Integer.valueOf(fiveAsString);
        logger.info("Integer.valueOf(fiveAsString): " + fiveAsWrapper);

        //Incompatible types will result in an Exception
        //JH - demo with an invalid and a valid value. First with nothing in args
        int intFromArgsArray = Integer.parseInt(args[1]);
        logger.info("\nintFromArgsArray:" + intFromArgsArray);

        //ints and Integers can be treated in the same way, to prevent unnecessary code - this is called autoboxing.
        logger.debug("\nAutoboxing working examples");
        printPrimitiveIntValue(fiveAsInt);
        printPrimitiveIntValue(fiveAsWrapper);
        printIntegerValue(fiveAsInt);
        printIntegerValue(fiveAsWrapper);

        //This shows autoboxing/intercompatibility
        fiveAsInt = fiveAsWrapper;
        fiveAsWrapper = fiveAsInt;

        //This, of course, is not allowed
        //fiveAsInt = fiveAsString;

        //More interchangeability of int and Integer
        fiveAsWrapper = Integer.valueOf(fiveAsString);
        fiveAsInt =     Integer.valueOf(fiveAsString);

        //We can get an int from an Integer by autoboxing, it is rarely necessary: -
        fiveAsInt = fiveAsWrapper.intValue();

        //This is because either primitive or wrapper can be used in method calls.
        logger.debug("\nUsing intValue() - usually redundant");
        printPrimitiveIntValue(fiveAsInt);
        printPrimitiveIntValue(fiveAsWrapper);

        //Similar methods in corresponding wrappers for other primitives
        double d = Double.parseDouble("89803.9998");
        logger.info("\nDouble.parseDouble(\"89803.9998\"): " + d);

        //Autoboxing can be used when constructing a List
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);

        logger.info("\nnumbers ArrayList:\t\t\t\t" + numbers);

        //Take care with autoboxing - example below: -
        //The following two lines will give different results: -
        numbers.remove(1);
        logger.info("numbers.remove(1):\t\t\t\t" + numbers);

        //Comment above two lines, then uncomment the following 2.
        //This uses the equals method (not operator ==) to determine if an element is present before removing
        numbers.remove(new Integer(1));
        logger.info("numbers.remove(new Integer(1):\t" + numbers);
        //JH - finally uncomment both the above blocks and re-run

    }

    public static void printPrimitiveIntValue(int value) {
        logger.info("printPrimitiveIntValue method: " + value);
    }

    public static void printIntegerValue(Integer value) {
        logger.info("printIntegerValue method: " + value);
    }

}
