/**
 * Calc
 *
 * Implementation of a simple calculator using java.awt API.
 *
 * @author Jon
 *
 */
package calculator;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Calc extends Frame implements ActionListener {
    Button addButton = new Button("Add");
    Button subtractButton = new Button("Subtract");
    Button mulitplyButton = new Button("Multiply");
    Button divideButton = new Button("Divide");
    Label number1Label = new Label("Number 1");
    Label number2Label = new Label("Number 2");
    Label resultLabel = new Label("Result");
    TextField number1TextField = new TextField(20);
    TextField number2TextField = new TextField(20);
    TextField resultTextField = new TextField(20);

    public Calc() {
        super("Calculator");
        setLayout(null);
        number1Label.setBounds(20, 50, 55, 25);
        add(number1Label);
        number2Label.setBounds(20, 100, 55, 25);
        add(number2Label);
        resultLabel.setBounds(20, 150, 55, 25);
        add(resultLabel);
        number1TextField.setBounds(160, 50, 100, 25);
        add(number1TextField);
        number2TextField.setBounds(160, 100, 100, 25);
        add(number2TextField);
        resultTextField.setBounds(160, 150, 100, 25);
        add(resultTextField);
        addButton.setBounds(20, 200, 80, 25);
        add(addButton);
        subtractButton.setBounds(100, 200, 80, 25);
        add(subtractButton);
        mulitplyButton.setBounds(180, 200, 80, 25);
        add(mulitplyButton);
        divideButton.setBounds(260, 200, 80, 25);
        add(divideButton);
        addButton.addActionListener(this);
        subtractButton.addActionListener(this);
        mulitplyButton.addActionListener(this);
        divideButton.addActionListener(this);
        addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                }
        );
    }

    public void actionPerformed(ActionEvent AE) {
        if (AE.getSource() == addButton) {
            sum();
        } else if (AE.getSource() == subtractButton) {
            subt();
        } else if (AE.getSource() == mulitplyButton) {
            mult();
        } else if (AE.getSource() == divideButton) {
            div();
        }
    }

    public void sum() {
        int i = Integer.parseInt(number1TextField.getText());
        int j = Integer.parseInt(number2TextField.getText());
        int val = i + j;
        resultTextField.setText("" + val);
    }

    public void subt() {
        int i = Integer.parseInt(number1TextField.getText());
        int j = Integer.parseInt(number2TextField.getText());
        int val = i - j;
        resultTextField.setText("" + val);
    }

    public void mult() {
        int i = Integer.parseInt(number1TextField.getText());
        int j = Integer.parseInt(number2TextField.getText());
        int val = i * j;
        resultTextField.setText("" + val);
    }

    public void div() {
        int i = Integer.parseInt(number1TextField.getText());
        int j = Integer.parseInt(number2TextField.getText());
        int val = i / j;
        resultTextField.setText("" + val);
    }

    public static void main(String args[]) {
        Calc MC = new Calc();
        MC.setVisible(true);
        MC.setSize(600, 500);
    }
}