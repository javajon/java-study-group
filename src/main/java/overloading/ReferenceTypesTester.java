package overloading;

//The JVM will call the closest match overloaded method.
public class ReferenceTypesTester {
  //  public void fly(String s) {
  //      System.out.print("string ");
 //   }

    public void fly(Object o) {
        System.out.print("object ");
    }

       public void fly(short i) {
          System.out.print("short ");
       }
    //   public void fly(int i) {
    //      System.out.print("int ");
    //   }

    public void fly(long l) {
        System.out.print("long ");
    }

    public static void main(String[] args) {
        ReferenceTypesTester r = new ReferenceTypesTester();
        r.fly("test");
        r.fly(56);
        r.fly(123);
        r.fly(123L);

        short s= 3;
        r.fly(s);
    }
}
