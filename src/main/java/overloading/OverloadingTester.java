package overloading;

/**
 * Created by rc00028 on 23/06/2017.
 */
public class OverloadingTester {

    public void fly(int numMiles) { }
    public void fly(short numFeet) { }
    public boolean fly() { return false; }
    //String fly(int numMiles, short numFeet) { }
    public void fly(short numFeet, int numMiles) throws Exception { }
    public void fly(int numFeet, short numMiles) throws Exception { }

    //public String fly(int numMiles) { return "Hello"; } //Causes a clash
    //public static String fly(int numMiles) { return "Hello"; } //Causes a clash


    //Array and Vaargs
    //public void fly(int[] lengths) { }
    //public void fly(int... lengths) { }

    //Autoboxing. This does not clash with line 8.
    public void fly(Integer numMiles) { }


}
