package encapsulation;

/**
 * Created by rc00028 on 26/06/2017.
 *
 * Omit the setters to make the class immutable.
 * Some care needs to be taken here - see page 207
 */
public class ImmutableSwan {
    private int numberEggs;

    public ImmutableSwan(int numberEggs) {
        this.numberEggs = numberEggs;
    }

    public int getNumberEggs() {
        return numberEggs;
    }
}
