package encapsulation;

/**
 * Created by rc00028 on 26/06/2017.
 */
public class Swan {
    private int numberEggs;

    public int getNumberEggs() {
        return numberEggs;
    }

    //This protects the numberEggs variable being set to a negative number.
    public void setNumberEggs(int numberEggs) {
        if (numberEggs >= 0) {
            this.numberEggs = numberEggs;
        } else {
            //System.out.println("Request ignored. Negative number not allowed for numberEggs");
            //Change to: -
            if (numberEggs < 0) {
                throw new IllegalArgumentException("# eggs must not be negative");
            }
        }
    }
}