package encapsulation;

/**
 * Created by rc00028 on 26/06/2017.
 * <p>
 * Encapsulation involves making properties private and restricting access to them
 * through getter and setter methods. This is called the javabeans convention.
 */
public class EncapsulationTester {

    private boolean playing;
    private String name;

    //Follows javabeans naming conventions
    public boolean getPlaying() {
        return playing;
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    //Follows javabeans naming conventions because getters for booleans should start with is. getPlaying would probably be ok too!
    public boolean isPlaying() {
        return playing;
    }

    //To following javabeans naming conventions, this should be called getName
    public String name() {
        return name;
    }

    //To following javabeans naming conventions, this should be called setName
    public void updateName(String n) {
        name = n;
    }

    //To following javabeans naming conventions, this should be called setName
    public void setname(String n) {
        name = n;
    }


    public static void main(String[] args) {
        Swan swan = new Swan();

        //Encapsulation prevents the following being possible.
        //swan.numberEggs = -1;

        //It is possible to call this: -
        swan.setNumberEggs(-1);

        //but the logic in setNumberEggs protects it.
        System.out.println(swan.getNumberEggs());


        //ImmutableSwan MUST be given a start value.
        ImmutableSwan immutableSwan = new ImmutableSwan(4);

        //But there is no setter to change this afterwards.
        //immutableSwan.setNumberEggs(3); //DOES NOT COMPILE.


    }

}
