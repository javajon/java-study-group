package loops;

/**
 * Created by jon on 25/05/2017.
 *
 * A while loop is constructed like this:
 *
 * while(boolean expression){
 *     perform the steps in these brackets.
 * }
 *
 */
public class WhileLoopTester {
    public static void main(String[] args) {

        //While loop using an explicit counter variable. The int loopCounter has been created specifically for executing this loop, similar to int i in a standard for loop.
        int loopCounter = 0;
        /*
        while (loopCounter < 10) {
        //    System.out.println("This is iteration " + (loopCounter + 1) + " of the while loop.");
            //loopCounter++;
        }
        */

      //  System.out.println("After while loop execution.");

        //Beware of infinite loops. Remember to increment the loop counter.


        String str = "Hello world! ";
        loopCounter = 0;
        while(loopCounter < 10){
       //     System.out.println(str + (loopCounter + 1));

            //Demonstrate out of memory if either of the following two lines are uncommented.
            //Or infinite loop if they are commented.
            str = str + str;
            //or
            // str+=str;   //Compound operator

            //Add this to make the loop work fine.
            loopCounter++;

        }


        //This time the loop is controlled by the arguments in the args array.
        int argsSize = args.length;
        //if(argsSize == 0){
          //  System.out.println("No arguments to print");
        //}

        loopCounter = 0;
        while (loopCounter < argsSize) {
            System.out.println("This is iteration " + (loopCounter + 1) + " of the while loop. args[" + loopCounter + "] = " + args[loopCounter]);
            loopCounter++;
        }



    }
}
