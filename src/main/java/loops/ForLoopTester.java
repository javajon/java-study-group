package loops;

/**
 * Created by jon on 25/05/2017.
 *
 *
 * A for loop is constructed like this:
 *
 * for(initialisation; boolean expression; update statement) {
 *     perform the steps in these brackets.
 * }
 *
 */
public class ForLoopTester {
    public static void main(String[] args) {

        for(int i = 0; i < 10; i++){
            //Show how you can change displayed value for i without changing value of i
            //Demo i + 1 with or without brackets.
            System.out.println("This is iteration " + (i + 1) + " of the loop.");
        }

        //There is no difference in behaviour if pre or post increment operator is used.
        for(int i = 0; i < 10; ++i){
            System.out.println("This is iteration " + (i + 1) + " of the loop.");
        }

        //Multiple variables can be used in a for loop.
        for(int i = 0, j=0; i < 10 && j < 40; i++, j+=i){
            System.out.println("This is iteration " + i + " of the loop. j = " + j);
        }

        //Using args.
        for(int i = 0; i < args.length; i++){
            System.out.println("This is iteration " + (i + 1) + " of the for loop. args[" + i + "] = " + args[i]);
        }
    }
}
