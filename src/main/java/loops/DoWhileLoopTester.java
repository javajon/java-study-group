package loops;

/**
 * Created by jon on 25/05/2017.
 *
 *
 * A do-while loop is constructed like this:
 *
 * do {
 *     perform the steps in these brackets.
 * } while(boolean expression);
 *
 * This is very much like a while statement, but guarantees to execute the body at least once.
 *
 *
 */
public class DoWhileLoopTester {
    public static void main(String[] args) {

        int loopCounter = 100;
        do {
        //    System.out.println("This is iteration " + (loopCounter + 1) + " of the do while loop.");
            loopCounter++;
        } while (loopCounter < 10);

        //As with while loops, again, watch out for infinite loops.
/*
        loopCounter = 0;
        do {
            System.out.println("This is iteration " + (loopCounter + 1) + " of the potentially infinite do while loop.");
            //loopCounter++;
        } while (loopCounter < 20);
*/


        //Do while provides a guarantee that the statement will be executed at least once.
        //However, this example shows that, if combined with extra logic, it can be made to behave in exactly the same way.
        //First, demonstrate this in a while loop
        int x = 2;

        while (x > 10) {
            System.out.println("x: " + x + ". This is generated from a while loop.");
            x--;
        }

        //Same login in an if/do while combination.
        //Remove the if and it is guaranteed to execute the body once at least.
        x = 2;

        if (x > 10) {
            do {
                System.out.println("x: " + x + ". This is generated from a do while loop.");
                x--;
            } while (x > 10);
        }

    }
}
