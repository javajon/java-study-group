package loops;

import zoo.animal.Animal;
import zoo.ZooFirstEffort;

import java.util.Iterator;
import java.util.List;

/**
 * Created by jon on 10/05/2017.
 * <p>
 * This class demontrates advanced features of loop control: -
 * <p>
 *  - Nested loops
 *  - Loop labels
 *  - break
 *  - continue
 */
public class AdvancedControlLoopTester {

    //JH - create data so that these can actually be run.
    public static void main(String[] args) {

        //Any type of loop can be nested inside any other type of loop.
        //We will return to this once we have covered topics that are relevant to this, such as multi-dimensional arrays.
        //Illustrative example where zoos is a collection of zoos: -
        List<ZooFirstEffort> zoos = null;  //we will populate this in a later session
        for (int i = 0; i < zoos.size(); i++) {
            ZooFirstEffort zoo = zoos.get(i);
            System.out.println("This is zoo " + zoo.getName());

            List<Animal> animals = zoo.getAnimals();

            Iterator<Animal> animalIterator = animals.iterator();
            while (animalIterator.hasNext()) {
                Animal animal = animalIterator.next();
                System.out.println("Animal details:");
                System.out.println(animal.getName());
                System.out.println(animal.getType());
            }

        }

        //Illustrative example of labelling loops and breaking loops: -
        //There is one animal called Jeremy who needs to be found.
        //Note different loops can be broken. If there is only one animal called Jeremy, the outer loop can be broken.
        zoosLoop: for (int i = 0; i < zoos.size(); i++) {
            ZooFirstEffort zoo = zoos.get(i);
            System.out.println("This is zoo " + zoo.getName());
            List<Animal> animals = zoo.getAnimals();

            Iterator<Animal> animalIterator = animals.iterator();
            animalsLoop: while (animalIterator.hasNext()) {
                Animal animal = animalIterator.next();
                String name = animal.getName();
                if (name.equals("Jeremy")) {
                    System.out.println("Jeremy found! He is a " + animal.getType() + " and lives in " + zoo.getName());
                    break zoosLoop;
                }
            }
        }

        //Illustrative example of continuing and/or breaking loops: -
        //Ignore and snakes by continuing them.
        //If you want to ignore a zoo with snakes in it, the inner loop can be broken.
        zoosLoop: for (int i = 0; i < zoos.size(); i++) {
            ZooFirstEffort zoo = zoos.get(i);
            System.out.println("This is zoo " + zoo.getName());
            List<Animal> animals = zoo.getAnimals();

            Iterator<Animal> animalIterator = animals.iterator();
            animalsLoop: while (animalIterator.hasNext()) {
                Animal animal = animalIterator.next();
                String type = animal.getType();
                if (type.equalsIgnoreCase("Snake")) {
                    continue;
                    //If you want to leave the zoo as soon as a snake is found,
                    //break can be u/sed instead of continue;
                    //break animalsLoop;
                    //To leave all zoos, break the outer loop as soon as a snake is found.
                    //break zoosLoop;
                    //break on it's own without a label will default to the
                    //lowest level loop where execution currently is.
                    //break;
                    //In this example, this is the same as: -
                    //break animalsLoop;
                } else {
                    System.out.println("Animal details:");
                    System.out.println(animal.getName());
                    System.out.println(animal.getType());
                }
            }
        }
    }

}
