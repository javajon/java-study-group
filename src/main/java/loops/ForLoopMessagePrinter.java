package loops;
/**
 * Created by jon on 08/05/2017.
 *
 * This compares a traditional for loop with a for-each (or shortcut) for loop.
 *
 *  A foreach loop is constructed like this:
 *
 * for(datatype identifier : collection) {
 *     perform the steps in these brackets for each iteration.
 * }
 */

public class ForLoopMessagePrinter {


    //Using a for loop to print out command line params.
    public static void main(String args[]) {


        ForLoopMessagePrinter g = new ForLoopMessagePrinter();

        //Traditional for loop.
        System.out.println("Traditional for loop output: \n");
        for(int i = 0; i < args.length; i++){
            String message = args[i];
            g.printOut(message);
        }

        //Shortcut for loop executes exactly the same way.
        System.out.println("\nShortcut for loop output: \n");
       for(String s : args) {
            g.printOut(s);
        }
    }


    void printOut(String message){
        System.out.println(message);
    }

}
