package simplecalculator;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by u.7829872 on 07/12/2017.
 */
interface MathInterface extends Remote
{
    public int add(int a, int b) throws RemoteException;
    public int subt(int a, int b) throws RemoteException;
    public int mult(int a, int b) throws RemoteException;
    public int div(int a, int b) throws RemoteException;
}
