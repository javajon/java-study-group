package garbagecollector;

/**
 * Created by rc00028 on 05/06/2017.
 */
public class GarbageTester {

    public static void main(String[] args) {
        GarbageTester garbageTester = new GarbageTester();
        garbageTester.sayHello();

        garbageTester = new GarbageTester();
        garbageTester.sayHello();

        garbageTester = new GarbageTester();
        garbageTester.sayHello();

        garbageTester = new GarbageTester();
        garbageTester.sayHello();

        garbageTester = new GarbageTester();
        garbageTester.sayHello();

        garbageTester = new GarbageTester();
        garbageTester.sayHello();

        System.gc();

        //Nullify garbageTester to make it available for garbage collection.
        garbageTester = null;
        System.out.println("The final garbageTester instance is now eligible for garbage collection");

        System.gc();
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Goodbye, World!");
    }

    public void sayHello(){
        System.out.println("Hello, World!");
    }

}
