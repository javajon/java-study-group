package accessmodifiersandinheritance.packageA;


import accessmodifiersandinheritance.packageB.B_ChildClass;
import org.apache.log4j.Logger;
import zoo.animal.Animal;
import zoo.animal.Tiger;

/**
 *
 * A_IndependentClass.
 *
 * This class is used with along with other classes in this package and a sibling package to
 * demonstrate inheritance and the use and rules of access modifiers.
 *
 * This class does not extend, nor is it extended, by any other class in the package.
 *
 * It can modify all it's own members but only private members on other instances.
 *
 * Created by jon on 15/06/2017.
 */
public class A_IndependentClass {

    //Fields
    final static Logger logger = Logger.getLogger(A_IndependentClass.class);

    public int publicIndependentInt = 1;
    protected int protectedIndependentInt = 2;
    int defaultIndependentInt = 3;
    private int privateIndependentInt = 4;

    //Methods
    public void publicIndependentMethod(Animal a) {
        logger.debug("publicIndependentMethod");
    }

    protected void protectedIndependentMethod() {
        logger.debug("publicIndependentMethod");
    }

    void defaultIndependentMethod() {
        logger.debug("publicIndependentMethod");
    }

    private void privateIndependentMethod() {
        logger.debug("publicIndependentMethod");
    }

    //Instance method to check visibility of the above test methods and methods on the parent class.
    public void checkAccessDirectly() {
        //All its own methods can be called directly.
        publicIndependentMethod(null);
        protectedIndependentMethod();
        defaultIndependentMethod();
        privateIndependentMethod();

        logger.debug(publicIndependentInt);
        logger.debug(protectedIndependentInt);
        logger.debug(defaultIndependentInt);
        logger.debug(privateIndependentInt);
    }

    public void checkAccessViaInstance() {
        //All its own methods and variables can be called directly.
        A_IndependentClass instanceOfMyClass = new A_IndependentClass();
        instanceOfMyClass.privateIndependentMethod();
        instanceOfMyClass.protectedIndependentMethod();
        instanceOfMyClass.defaultIndependentMethod();
        instanceOfMyClass.publicIndependentMethod(null);

        logger.debug(instanceOfMyClass.publicIndependentInt);
        logger.debug(instanceOfMyClass.protectedIndependentInt);
        logger.debug(instanceOfMyClass.defaultIndependentInt);
        logger.debug(instanceOfMyClass.privateIndependentInt);

        //Only the public methods of the unrelated class in another package can be called via a variable reference.
        B_ChildClass instanceOfUnrelatedClassInAnotherPackage = new B_ChildClass();
        instanceOfUnrelatedClassInAnotherPackage.publicChildMethod(new Tiger("Tyrone"));
        //instanceOfUnrelatedClassInAnotherPackage.protectedChildMethod();
        //instanceOfUnrelatedClassInAnotherPackage.defaultChildMethod();
        //instanceOfUnrelatedClassInAnotherPackage.privateChildMethod();

    }
}