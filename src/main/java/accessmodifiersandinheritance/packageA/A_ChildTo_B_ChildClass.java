package accessmodifiersandinheritance.packageA;


import accessmodifiersandinheritance.packageB.B_ChildClass;
import accessmodifiersandinheritance.packageB.B_GrandchildClass;
import accessmodifiersandinheritance.packageB.B_ParentClass;
import accessmodifiersandinheritance.packageB.subpackageB.B_B_GrandchildClass;
import org.apache.log4j.Logger;
import zoo.animal.Animal;

/**
 * A_ChildTo_B_ChildClass.
 *
 * This class is used with along with other classes in this package and a sibling package to
 * demonstrate inheritance and the use and rules of access modifiers.
 *
 * This class extends B_ChildClass, which extends B_ParentClass. A_ChildTo_B_ChildClass, however,
 * is not in the same package as the parent and grandparent classes.
 *
 * This class is very similar to B_B_GrandchildClass in its relation to the parent and grandarent.
 * It demonstrates that B_B_GrandchildClass being in a subpackage of ts parent is no different in
 * this regard from being in a totally unrelated package.

 * Inheritance is the process by which the new child subclass automatically
 * includes any public or protected members defined in parent classes.
 *
 * This is seen here in the following two methods: -
 *
 * protectedChildMethod() - inherited from parent
 * protectedParentMethod() - inherited from grandparent
 *
 * However, as with B_B_GrandchildClass, when it creates an instance of the parent and grandparent classes,
 * it only has access to public members as this access in not through inheritance but by treating the parent
 * and grandparent classes as independent objects.
 *
 * Java supports single inheritance. It is not possible to extend from more than one parent class.
 * Java supports single inheritance, by which a class may inherit from only one direct parent class.
 * Java also supports multiple levels of inheritance, by which one class may extend another class,
 * which in turn extends another class. You can extend a class any number of times,
 * allowing each descendent to gain access to its ancestor’s members.
 *
 * Java uses Interfaces to get round the limitations of single inheritance.
 *
 * Created by jon on 15/06/2017.
 *
 *
 */

//Not allowed - implies multiple inheritance
//public class A_ChildTo_B_ChildClass extends B_ChildClass, Animal {

//Not allowed as String is marked final
//public class A_ChildTo_B_ChildClass extends String {
public class A_ChildTo_B_ChildClass extends B_ChildClass {

    //Logger
    final static Logger logger = Logger.getLogger(B_ChildClass.class);

    public void checkAccessDirectly() {

        //Only public and protected members are accessible to a parent in a different package.
        publicChildMethod(null);
        protectedChildMethod();
        //defaultChildMethod();
        //privateChildMethod();

        //The same applies to the grandparent
        publicParentMethod();
        protectedParentMethod();
        //defaultParentMethod();
        //privateParentMethod();

    }

    public void checkAccessViaInstance() {
        //As the parent is in a different package, only public and protected
        //members are accessible on an instance of this class.
        A_ChildTo_B_ChildClass instanceOfMyClass = new A_ChildTo_B_ChildClass();
        instanceOfMyClass.publicChildMethod(null);
        instanceOfMyClass.protectedChildMethod();
        //instanceOfMyClass.defaultChildMethod();
        //instanceOfMyClass.privateChildMethod();

        //As the grandparent is in a different package, only public and protected
        //members are accessible on an instance of this class.
        instanceOfMyClass.publicParentMethod();
        instanceOfMyClass.protectedParentMethod();
        //instanceOfMyClass.defaultParentMethod();
        //JH show how we can have the method below.
        //instanceOfMyClass.privateParentMethod();

        logger.debug(instanceOfMyClass.publicChildInt);
        logger.debug(instanceOfMyClass.protectedChildInt);
        //logger.debug(instanceOfMyClass.defaultChildInt);
        //logger.debug(instanceOfMyClass.privateChildInt);

        //As the parent is in a different package, only public members are accessible
        //when using a reference to an instance of the parent itself.
        B_ChildClass instanceOfMyParentClass = new B_ChildClass();
        instanceOfMyParentClass.publicChildMethod(null);
        //instanceOfMyParentClass.protectedChildMethod();
        //instanceOfMyParentClass.defaultChildMethod();
        //instanceOfMyParentClass.privateChildMethod();

        logger.debug(instanceOfMyParentClass.publicChildInt);
        //logger.debug(instanceOfMyParentClass.protectedChildInt);
        //logger.debug(instanceOfMyParentClass.defaultChildInt);
        //logger.debug(instanceOfMyParentClass.privateChildInt);

        //As the grandparent is in a different package, only public members are accessible
        //when using a reference to an instance of the grandparent itself.
        B_ParentClass instanceOfMyGrandparentClass = new B_ParentClass();
        instanceOfMyGrandparentClass.publicParentMethod();
        //instanceOfMyGrandparentClass.protectedParentMethod();
        //instanceOfMyGrandparentClass.defaultParentMethod();
        //instanceOfMyGrandparentClass.privateParentMethod();

    }

    /*
    //@Override
    public void privateParentMethod() {
        System.out.println("This is not an override");

    }
    */

    //
    public static void main(String[] args) {
        A_ChildTo_B_ChildClass instanceOfMyClass = new A_ChildTo_B_ChildClass();
        instanceOfMyClass.checkAccessDirectly();
        instanceOfMyClass.checkAccessViaInstance();
    }
}
