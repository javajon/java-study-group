package accessmodifiersandinheritance;

import java.io.File;

public class FileProvider {

    public File getFileByName(String user, String file) {
        checkUserAccess();
        checkFileExists();
        return retrieveFile();
    }


    private void checkUserAccess(){
        //throw exception is user does not have access.
    }


    private void checkFileExists(){
        //throw exception if file does not exist.
    }

    private File retrieveFile(){
        return null;
    }


}