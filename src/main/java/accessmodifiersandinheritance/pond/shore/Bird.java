package accessmodifiersandinheritance.pond.shore;

/**
 *
 * Bird.
 *
 * This class is used with along with other classes in this package to
 * demonstrate inheritance and the use and rules of access modifiers.
 *
 * This class does not extend, nor is it extended, by any other class in the package.
 *
 * It exposes a protected field and a protected method, which classes in it's package
 * and its subclasses in a different package can access through inheritance.
 *
 * Created by jon on 16/06/2017.
 *
 */
public class Bird {

    protected String text = "floating"; // protected access

    protected void floatInWater() { // protected access
        System.out.println(text);
    }
}
