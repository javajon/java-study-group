package accessmodifiersandinheritance.pond.goose;


import accessmodifiersandinheritance.pond.shore.Bird;

/**
 *
 * Goose.
 *
 * This class is used with along with other classes in this package to
 * demonstrate inheritance and the use and rules of access modifiers.
 *
 * This class extends Bird, which is in another package.
 *
 * Through inheritance, it can access the protected field and the protected method
 * defined in Bird.
 *
 * However, it cannot access those mebers through object references to Bird instances.
 *
 * Created by jon on 16/06/2017.
 *
 */
public class Goose extends Bird {
    public void helpGooseSwim() {
        //Direct access to members via inheritance without variable.
        floatInWater();
        System.out.println(text);

        //Access to members via inheritance with variable.
        Goose other = new Goose();
        other.floatInWater();
        System.out.println(other.text);
    }

    public void helpOtherGooseSwim() {
        //Access to members in superclass with variable.
        Bird other = new Bird();
        //other.floatInWater();
        //System.out.println(other.text);

        //Access to members in superclass with variable. Even though the actual object is a Goose, the object reference is of type Bird.
        Bird another = new Goose();
        //Goose another = new Goose();
        //another.floatInWater();
        //System.out.println(another.text);
    }
}
