package accessmodifiersandinheritance.packageB;

import org.apache.log4j.Logger;

/**
 *
 * B_IndependentClass.
 *
 * This class is used with along with other classes in this package and a sibling package to
 * demonstrate inheritance and the use and rules of access modifiers.
 *
 * This class does not extend, nor is it extended, by any other class in the package.
 *
 * It can access public, protected and default members of other classes within its package.
 *
 * This class demonstrates,as long as a class can access all but private members on another
 * classes in the same package, whether the class is a subclass that other class or not.
 *
 * The behaviour demonstrated in this class is very similar to that seen in B_ChildClass, for example.
 *
 * Created by jon on 15/06/2017.
 *
 */
public class B_IndependentClass {

    final static Logger logger = Logger.getLogger(B_IndependentClass.class);

    public void checkAccessViaInstance() {
        //All the parent methods and variables can be called via an instance, except the private ones,
        // which are not visible here.
        B_ParentClass instanceOfAnotherClass = new B_ChildClass();
        instanceOfAnotherClass.publicParentMethod();
        instanceOfAnotherClass.protectedParentMethod();
        instanceOfAnotherClass.defaultParentMethod();
        //instanceOfAnotherClass.privateParentMethod();

    }
}
