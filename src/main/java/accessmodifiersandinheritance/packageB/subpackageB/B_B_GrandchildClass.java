package accessmodifiersandinheritance.packageB.subpackageB;


import accessmodifiersandinheritance.packageB.B_ChildClass;
import accessmodifiersandinheritance.packageB.B_GrandchildClass;
import accessmodifiersandinheritance.packageB.B_ParentClass;
import org.apache.log4j.Logger;
import zoo.animal.Tiger;

/**
 * B_B_GrandchildClass.
 *
 * This class is used with along with other classes in this package and a sibling package to
 * demonstrate inheritance and the use and rules of access modifiers.
 *
 * This class extends B_ChildClass, which extends B_ParentClass. B_B_GrandchildClass, however,
 * is not in the same package as the parent and grandparent classes.
 *
 * The principles demonstrated in this class are probably the most difficult to grasp in this area.
 *
 * As it is a subclass of B_ChildClass, it has access to it's public and protected
 * members only through inheritance.
 *
 * However, as with A_ChildTo_B_ChildClass, when it creates an instance of the parent and grandparent classes,
 * it only has access to public members as this access in not through inheritance but by treating the parent
 * and grandparent classes as independent objects.
 *
 *
 */
public class B_B_GrandchildClass extends B_ChildClass {

    //Logger
    final static Logger logger = Logger.getLogger(B_GrandchildClass.class);

    //Instance method to check visibility of the above test methods and methods on the parent class.
    public void checkAccessDirectly() {

        //As this class is in a different package to its parent, it can only access
        //public and protected parent members through inheritance.
        publicChildMethod(null);
        protectedChildMethod();
        //defaultChildMethod();
        //privateChildMethod();

        logger.debug(publicChildInt);
        logger.debug(protectedChildInt);
        //logger.debug(defaultChildInt);
        //logger.debug(privateChildInt);

        //As this class is in a different package to its parent, it can only access
        //public and protected grandparent members through inheritance.
        publicParentMethod();
        protectedParentMethod();
        //defaultParentMethod();
        //privateParentMethod();

    }

    public void checkAccessViaInstance() {
        //As the parent is in a different package, only public and protected
        //members are accessible on an instance of this class.
        B_B_GrandchildClass instanceOfMyClass = new B_B_GrandchildClass();
        instanceOfMyClass.publicChildMethod(null);
        instanceOfMyClass.protectedChildMethod();
        //instanceOfMyClass.defaultChildMethod();
        ///instanceOfMyClass.privateChildMethod();

        logger.debug(instanceOfMyClass.publicChildInt);
        logger.debug(instanceOfMyClass.protectedChildInt);
        //logger.debug(instanceOfMyClass.defaultChildInt);
        //logger.debug(instanceOfMyClass.privateChildInt);

        //As the parent is in a different package, only public members are accessible
        //when using a reference to an instance of the parent itself.
        B_ChildClass instanceOfMyParentClass = new B_ChildClass();
        instanceOfMyParentClass.publicChildMethod(null);
        //instanceOfMyParentClass.protectedChildMethod();
        //instanceOfMyParentClass.defaultChildMethod();
        //instanceOfMyParentClass.privateChildMethod();

        logger.debug(instanceOfMyParentClass.publicChildInt);
        //logger.debug(instanceOfMyParentClass.protectedChildInt);
        //logger.debug(instanceOfMyParentClass.defaultChildInt);
        //logger.debug(instanceOfMyParentClass.privateChildInt);

        //As the grandparent is in a different package, only public members are accessible
        //when using a reference to an instance of the grandparent itself.
        B_ParentClass instanceOfMyGrandparentClass = new B_ParentClass();
        instanceOfMyGrandparentClass.publicParentMethod();
        //instanceOfMyGrandparentClass.protectedParentMethod();
        //instanceOfMyGrandparentClass.defaultParentMethod();
        //instanceOfMyGrandparentClass.privateParentMethod();


    }

    /*
    @Override
    private void privateChildMethod(){

    }
    */
}
