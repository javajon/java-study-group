package accessmodifiersandinheritance.packageB;


import org.apache.log4j.Logger;
import zoo.animal.Animal;

/**
 * B_ChildClass.
 *
 * This class is used with along with other classes in this package and a sibling package to
 * demonstrate inheritance and the use and rules of access modifiers.
 *
 * This class extends B_ParentClass, in the same package.
 *
 * This class shows that all methods defined in this class are accessible either on the instance
 * itself or another instance of the same class.
 *
 * It also explores accessibility to methods on it's parent class in two ways: -
 *
 * 1. via inheritance.
 * 2. via object references.
 *
 * As the parent class is in the same package, it can access all but the private members.
 *
 * Created by jon on 15/06/2017.
 *
 */
public class B_ChildClass extends B_ParentClass {

    //Fields
    final static Logger logger = Logger.getLogger(B_ChildClass.class);

    public int publicChildInt = 1;
    protected int protectedChildInt = 2;
    int defaultChildInt = 3;
    private int privateChildInt = 4;

    //Methods
    public void publicChildMethod(Animal a) {
        logger.debug("publicChildMethod");
    }

    protected void protectedChildMethod() {
        logger.debug("protectedChildMethod");
    }

    void defaultChildMethod() {
        logger.debug("defaultChildMethod");
    }

    private void privateChildMethod() {
        logger.debug("privateChildMethod");
    }

    //Instance method to check visibility of the above test methods and methods on the parent class.
    public void checkAccessDirectly() {
        //All its own methods can be called directly.
        privateChildMethod();
        protectedChildMethod();
        defaultChildMethod();
        publicChildMethod(null);

        //Check access to inherited methods.
        //All the methods in the base class can be called via inheritance except the private one.
        publicParentMethod();
        protectedParentMethod();
        defaultParentMethod();
        //privateParentMethod();
    }

    public void checkAccessViaInstance() {
        //All its own methods and variables can be called directly.
        B_ChildClass instanceOfMyClass = new B_ChildClass();
        instanceOfMyClass.privateChildMethod();
        instanceOfMyClass.protectedChildMethod();
        instanceOfMyClass.defaultChildMethod();
        instanceOfMyClass.publicChildMethod(null);

        logger.debug(instanceOfMyClass.publicChildInt);
        logger.debug(instanceOfMyClass.protectedChildInt);
        logger.debug(instanceOfMyClass.defaultChildInt);
        logger.debug(instanceOfMyClass.privateChildInt);

        //All the emthods of the parent class can be called via a variable reference except the private one.
        B_ParentClass instanceOfMyParentClass = new B_ChildClass();
        instanceOfMyParentClass.publicParentMethod();
        instanceOfMyParentClass.protectedParentMethod();
        instanceOfMyParentClass.defaultParentMethod();
        //instanceOfMyParentClass.privateParentMethod();

    }

}
