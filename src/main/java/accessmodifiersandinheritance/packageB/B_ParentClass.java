package accessmodifiersandinheritance.packageB;

import dateandtime.DateTimeTester;
import org.apache.log4j.Logger;

/**
 * B_ParentClass.
 *
 * This class is used with along with other classes in this package and a sibling package to
 * demonstrate inheritance and the use and rules of access modifiers. This is the first class
 * in the demonstration and hence contains most documentation.
 *
 * Instance methods are defined with the following access modifiers: -
 *
 * public - The method/Field can be called from any class.
 * protected - The method/Field can only be called from classes in the same package or subclasses.
 * Default (Package Private) - The method/Field can only be called from classes in the same package.
 * private - The method/Field can only be called from within the same class.
 *
 * General informal rules: -
 * 1. Access public members from anywhere.
 * 2. Access private members ONLY in instances of the class in
 * which they are defined.
 * 3. Access to default members is restricted to instances of
 * classes within the package in which they are defined.
 * 4. Access to protected members is restricted to instances of
 * classes within the package in which they are defined and
 * subclasses in other packages.
 *
 * This class shows that all methods are accessible either on the instance itself or another instance.
 *
 */
public class B_ParentClass {

    final static Logger logger = Logger.getLogger(B_ParentClass.class);

    public void publicParentMethod(){
        logger.debug("publicParentMethod");
    }

    protected void protectedParentMethod(){
        logger.debug("protectedParentMethod");
    }

    void defaultParentMethod(){
        logger.debug("defaultParentMethod");
    }

    private void privateParentMethod(){
        logger.debug("privateParentMethod");
    }

    //Instance method to check visibility of the above test methods.
    public void checkAccessDirectly() {

        //All the test methods can be called directly on the instance itself.
        publicParentMethod();
        protectedParentMethod();
        defaultParentMethod();
        privateParentMethod();

        //Implicitly, these calls are made on "this".
        //this.publicParentMethod();
    }


    public void checkAccessViaInstance() {

        //All the test methods can be called indirectly, via an instance.
        B_ParentClass instanceOfMyClass = new B_ParentClass();
        instanceOfMyClass.publicParentMethod();
        instanceOfMyClass.protectedParentMethod();
        instanceOfMyClass.defaultParentMethod();
        instanceOfMyClass.privateParentMethod();

    }


}
