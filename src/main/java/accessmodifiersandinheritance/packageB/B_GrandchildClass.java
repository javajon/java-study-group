package accessmodifiersandinheritance.packageB;

import accessmodifiersandinheritance.packageA.A_IndependentClass;
import org.apache.log4j.Logger;

/**
 *
 * B_GrandchildClass.
 *
 * This class is used with along with other classes in this package and a sibling package to
 * demonstrate inheritance and the use and rules of access modifiers.
 *
 * This class extends B_ChildClass, which extends B_ParentClass.
 * All of these classes are in the same package.
 *
 * In this example, the superclass or parent class, B_ChildClass, has fields and methods at all
 * four access levels. The subclass or child class, B_GrandchildClass, inherits and has access to the
 * public and protected ones. As it is in the same package as B_ChildClass, it also inherits
 * and has access to the default ones. This also extends ovr generations. The same can be said of
 * B_GrandchildClass with respect to its grandparent, B_ParentClass as shown here.
 * 
 * The same is true of B_ChildClass with respect to B_ParentClass.
 *
 * Created by jon on 15/06/2017.
 *
 */
public class B_GrandchildClass extends B_ChildClass {

    final static Logger logger = Logger.getLogger(B_GrandchildClass.class);

    //Instance method to check visibility of the above test methods and methods on the parent class.
    public void checkAccessDirectly() {

        //All the methods in the parent can be called through inheritance except the private one, which is not inherited.
        //privateMethod();
        protectedChildMethod();
        defaultChildMethod();
        publicChildMethod(null);

        logger.debug(publicChildInt);
        logger.debug(defaultChildInt);
        logger.debug(protectedChildInt);
        //logger.debug(privateChildInt);

        //All the methods in the grandparent can be called through inheritance except the private one, which is not inherited.
        publicParentMethod();
        protectedParentMethod();
        defaultParentMethod();
        //privateParentMethod();

        //logger.debug(classC.privateInt);
    }

    public void checkAccessViaInstance() {
        //All the parent methods and variables can be called via an instance, except the private ones,
        // which are not visible here.
        B_GrandchildClass instanceOfMyClass = new B_GrandchildClass();
        instanceOfMyClass.publicChildMethod(null);
        instanceOfMyClass.protectedChildMethod();
        instanceOfMyClass.defaultChildMethod();
        //instanceOfMyClass.privateChildMethod();

        logger.debug(instanceOfMyClass.publicChildInt);
        logger.debug(instanceOfMyClass.defaultChildInt);
        logger.debug(instanceOfMyClass.protectedChildInt);
        //logger.debug(instanceOfMyClass.privateChildInt);

        B_ChildClass instanceOfMyParentClass = new B_ChildClass();
        instanceOfMyParentClass.publicChildMethod(null);
        instanceOfMyParentClass.protectedChildMethod();
        instanceOfMyParentClass.defaultChildMethod();
        //instanceOfMyParentClass.privateChildMethod();

        logger.debug(instanceOfMyParentClass.publicChildInt);
        logger.debug(instanceOfMyParentClass.defaultChildInt);
        logger.debug(instanceOfMyParentClass.protectedChildInt);
        //logger.debug(instanceOfMyParentClass.privateChildInt);

        B_ParentClass instanceOfMyGrandparentClass = new B_ParentClass();
        instanceOfMyGrandparentClass.publicParentMethod();
        instanceOfMyGrandparentClass.protectedParentMethod();
        instanceOfMyGrandparentClass.defaultParentMethod();
        //instanceOfMyGrandparentClass.privateParentMethod();

        //None of these variables exist ont eh grandparent as they are defined in B_ChildClass.
        //logger.debug(instanceOfMyGrandparentClass.publicChildInt);
        //logger.debug(instanceOfMyGrandparentClass.defaultChildInt);
        //logger.debug(instanceOfMyGrandparentClass.protectedChildInt);
        //logger.debug(instanceOfMyGrandparentClass.privateChildInt);

    }
}
