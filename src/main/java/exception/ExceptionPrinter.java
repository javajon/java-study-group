package exception;

/**
 * Created by rc00028 on 12/07/2017.
 */
public class ExceptionPrinter {
    public static void main(String[] args) {
        try {
            hop();
        } catch (Exception e) {
            //There are three ways to print an exception
            e.printStackTrace();
            System.out.println(e);
            System.out.println(e.getMessage());
        }
    }

    private static void hop() {
        throw new RuntimeException("cannot hop");
    }
}
