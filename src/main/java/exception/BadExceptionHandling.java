package exception;

import exception.checked.CheckedExceptionTester;
import zoo.animal.Animal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created by rc00028 on 12/07/2017.
 */
public class BadExceptionHandling {

    static void fall() throws Exception {
        throw new Exception();
    }

    public static void main(String[] args) throws Exception {

        //Remember that brackets are required.
        /*
        try  // DOES NOT COMPILE
            fall();
        catch (Exception e)
            System.out.println("get up");//DOES NOT COMPILE. The problem is that the braces are missing.
        */

        //It needs to look like this:
        try {
            fall();
        } catch (Exception e) {
            System.out.println("get up");
        }

        //This does not compile because the code in the try clause never throws
        //the Exception in the catch clause.
        /*
        try {
            System.out.println("Hello");
        } catch (InvalidAnimalException e) {
            System.out.println("get up");
        }
        */

        //exceptionHandlingHorrorExampleOne("gorilla");
        //exceptionHandlingHorrorExampleTwo("shrew");
        //exceptionHandlingHorrorExampleThree("walrus");
        //exceptionHandlingHorrorExampleFour("walrus");
        //exceptionHandlingHorrorExampleFive("walrus");
    }

    //Catch cannot come after finally.
/*    private void readFileAndPrintOutContents(String filename) {
        try {

            File f = new File(filename);

            BufferedReader b = new BufferedReader(new FileReader(f));

            String readLine = "";

            System.out.println("Reading file using Buffered Reader");

            while ((readLine = b.readLine()) != null) {
                System.out.println(readLine);
            }

        } finally {
            System.out.println("In the finally clause of a try-catch-finally construct. This will execute whether the try block ran successfully or not");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    */

    //It is not allowed to have try only.
    /*
    private void tryOnlyMethod() throws InvalidAnimalException{
        try{
            System.out.println("In try clause of tryAndFinallyOnlyMethod()");
        }
    }
    */

    //This creates a NullPointerException - exactly what we are trying to avoid.
    // You CAN flow a RuntimeException, but it is rarely the best option.
    private static void exceptionHandlingHorrorExampleOne(String type) throws InvalidAnimalException {
        Animal animal = CheckedExceptionTester.getAnotherAnimal(type);

        if (animal != null) {
            System.out.println(animal.getName());
        } else {
            throw new NullPointerException();
        }


        //We might as well just do this!!!
        System.out.println(animal.getName());


        //Instead, we should throw a custom Exception: -

        if (animal != null) {
            System.out.println(animal.getName());
        } else {
            throw new InvalidAnimalException(type + " is an invalid animal. There are no animals of that type in this zoo.");
        }

    }

    //Exception swallowing - gets on ALL top ten lists of things NOT to do in JAVA.
    private static void exceptionHandlingHorrorExampleTwo(String type) {

        System.out.println("Execution started!");
        Animal animal = null;
        try {
            animal = CheckedExceptionTester.getAnimal(type);
        } catch (Exception e) {
            //Do nothing;
            //At the very least do this: -
            //e.printStackTrace();
        }

        System.out.println("Execution completed successfully!");
        //Instead, we should actually handle it.
    }

    //Catch useful, informative Exception, then throw generic, non-informative Exception - gets on MOST top ten lists of things NOT to do in JAVA.
    private static void exceptionHandlingHorrorExampleThree(String type) throws Exception {

        System.out.println("Execution started!");
        Animal animal = null;
        try {
            animal = CheckedExceptionTester.getAnimal(type);
        } catch (InvalidAnimalException e) {
            throw new Exception();
        }
    }

    //Catch Exception, print stack trace, then allow the thread to proceed!
    private static void exceptionHandlingHorrorExampleFour(String type) throws Exception {

        //Not much better than exceptionHandlingHorrorExampleTwo
        System.out.println("Execution started!");
        Animal animal = null;
        try {
            animal = CheckedExceptionTester.getAnimal(type);
        } catch (InvalidAnimalException e) {
            e.printStackTrace();
        }

        System.out.println("Execution completed successfully!");

    }

    //Catch Exception, use Exception handling to control flow!
    //The reluctant Exception thrower. Honestly, it's no shame to throw an Exception int he right circumstances.
    //You CAN control flow in Exception handling blocks, but please don't!
    private static void exceptionHandlingHorrorExampleFive(String filename) throws Exception {

        try {

            File f = new File(filename);

            BufferedReader b = new BufferedReader(new FileReader(f));

            String readLine = "";

            System.out.println("Reading file using Buffered Reader");

            while ((readLine = b.readLine()) != null) {
                System.out.println(readLine);
            }

        } catch (Exception e) {
            //Try again!!
            try {
                System.out.println("Didn't work...try again!!");

                File f = new File(filename);

                BufferedReader b = new BufferedReader(new FileReader(f));

                String readLine = "";

                System.out.println("Reading file using Buffered Reader");

                while ((readLine = b.readLine()) != null) {
                    System.out.println(readLine);
                }

            } catch (Exception e1) {

                System.out.println("Still didn't work...Try something else in desperation!!!");
                System.out.println("I'm determined not to throw an Exception, even though I should at this point!");
                System.out.println("Instead, I'll divert the thread to somewhere probably inappropriate - maybe we can find the file somewhere else, or using different code, or a different filepath.");
                System.out.println("If that doesn't work, maybe we can \"code round\" the problem by not even using the File.");
            }
        }
    }
}
