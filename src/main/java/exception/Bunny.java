package exception;

/**
 * Created by rc00028 on 12/07/2017.
 */
class NoMoreCarrotsException extends Exception {
}

//Why doesn’t the following code example compile?
public class Bunny {

    /*
    public static void main(String[] args) {
        eatCarrot();// DOES NOT COMPILE
    }

    private static void eatCarrot() throws NoMoreCarrotsException {
    }
*/


    //Why doesn’t the following code example compile?


  /*  public void bad() {
        try {
            eatCarrots();
        } catch (NoMoreCarrotsException e) {// DOES NOT COMPILE
            System.out.print("sad rabbit");
        }
    } */

    private static void eatCarrots() {
    }

    //Both these signatures are OK!
    //public void good() {
    public void good() throws NoMoreCarrotsException {
        eatCarrots();
    }

}
