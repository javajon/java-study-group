package exception;

/**
 * Created by rc00028 on 27/06/2017.
 */
public class InvalidAnimalDataException extends InvalidAnimalException {
    public InvalidAnimalDataException(String message) {
        super(message);
    }
}
