package exception;

/**
 * Created by rc00028 on 27/06/2017.
 */
public class InvalidAnimalException extends Exception {

    public InvalidAnimalException() {
    }

    public InvalidAnimalException(String message) {
        super(message);
    }
}
