package exception.checked;

/**
 * Created by rc00028 on 12/07/2017.
 */
class CanNotHopException extends Exception {
}

//Why doesn't this compile?

/*
public class Hopper {
    public void hop() {
    }
}

class Bunny extends Hopper {
    public void hop() throws CanNotHopException {
    } // DOES NOT COMPILE

    public static void main(String[] args) {
        Hopper bunny = new Bunny();
        bunny.hop();
        //bunny is handled as a Hopper at compile time
        //but a Bunny at runtime.
    }
}
*/

//Java knows hop() isn’t allowed to throw any checked exceptions because the superclass Hopper doesn’t declare any.
//Imagine what would happen if subclasses could add checked exceptions — you could write code that calls Hopper’s
//hop() method and not handle any exceptions. Then if Bunny was used in its place, the code wouldn’t know to handle
//or declare CanNotHopException.
//
// A subclass is allowed to declare fewer exceptions than the superclass or interface. This is legal
// because callers are already handling them.
class HopperTwo {
    public void hop() throws CanNotHopException {
    }
}

class BunnyTwo extends HopperTwo {
    public void hop() {
    }

    public static void main(String[] args) {
        HopperTwo bunny2 = new BunnyTwo();
        try {
            bunny2.hop();
        } catch (CanNotHopException e) {
            e.printStackTrace();
        }
        //bunny2 is handled as a HopperTwo at compile time
        //but a BunnyTwo at runtime.
    }
}

//An overridden method is allowed to declare a subclass of an exception type.
class HopperThree {
    public void hop() throws Exception {
    }
}

class BunnyThree extends HopperThree {
    public void hop() throws CanNotHopException {
    }
}

//As RuntimeExceptions are not checked, it's OK to throw them from overriden method even
//if they are not declared in the over-ridden method.
class HopperFour {
    public void hop() {
    }
}

class BunnyFour extends HopperFour {
    public void hop() throws IllegalStateException {
    }
}