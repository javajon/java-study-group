package exception.checked;

import java.io.*;

/**
 * Created by rc00028 on 13/07/2017.
 *
 * File interactions are effected using java.io code.
 * There are lots of steps in file connectivity that could potentially thrown an Exception.
 * Therefore it is a good candidate for "block" handling.
 *
 */
public class FileReaderAndPrinter {

    //Show how the Exceptions can be handled in a block or individually and parent classes can be
    //caught, which will catch any child classes. Show that the order of catch clauses is important.
    //
    //Show also the alternative throws declaration.
    //JH - do this by writing alternative methods.
    //Design 1 - a large try-catch block catching the superclass Exception.
    private void readFileAndPrintOutContents(String filename) {
        try {

            File f = new File(filename);

            BufferedReader b = new BufferedReader(new FileReader(f));

            String readLine = "";

            System.out.println("Reading file using Buffered Reader");

            while ((readLine = b.readLine()) != null) {
                System.out.println(readLine);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("In the finally clause of a try-catch-finally construct. This will execute whether the try block ran successfully or not");
        }
    }

    private void readFileAndPrintOutContentsV2(String filename) {
      //  try {

            File f = new File(filename);

        BufferedReader b = null;
        try {
            b = new BufferedReader(new FileReader(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String readLine = "";

            System.out.println("Reading file using Buffered Reader");

        try {
            while ((readLine = b.readLine()) != null) {
                System.out.println(readLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //  } catch (Exception e) {
      //      e.printStackTrace();
    //    } finally {
     //       System.out.println("In the finally clause of a try-catch-finally construct. This will execute whether the try block ran successfully or not");
      //  }
    }


    public static void main(String[] args) {
        FileReaderAndPrinter reader = new FileReaderAndPrinter();
        String fileToReadAndPrint = "./src/test/java/com/jon/javastudy/exception/checked/textfile.txt";
        reader.readFileAndPrintOutContents(fileToReadAndPrint);
    }

}
