package exception.checked;

import java.sql.*;

/**
 *
 * OracleDatabaseConnectionTester
 *
 * Created by rc00028 on 13/07/2017.
 *
 * Database connections are made using jdbc code.
 * There are lots of steps in database connectivity that could potentially thrown an Exception.
 * Therefore it is a good candidate for surrounding large blocks of code in try-catch constructs.
 *
 * It is OK to do this as long as at least one line of code within it throws that type of Exception.
 */
public class OracleDatabaseConnectionTester {

    static Connection con = null;

    public static void main(String args[]) {
        try {
            //step1 load the driver class
            Class.forName("oracle.jdbc.driver.OracleDriver");

            //step2 create  the connection object
            //Connect to DELTAENV4 database.
            con = DriverManager.getConnection(
                    "jdbc:oracle:thin:@10.104.142.27:1521/ppega25", "cfs1_data", "cfs1_data");

            //step3 create the statement object
            Statement stmt = con.createStatement();

            //step4 execute query
            ResultSet rs = stmt.executeQuery("SELECT count(*) AS total FROM pega_to_dwh_mis");
            while (rs.next()) {
                int count = rs.getInt(1);
                System.out.println("There are " + count + " records in the table pega_to_dwh_mis");
            }

            //step5 close the connection object
            con.close();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}