package exception.checked;


import zoo.animal.Animal;
import zoo.data.AnimalListDataManager;

/**
 * Created by rc00028 on 11/07/2017.
 * <p>
 * ExceptionTester
 * <p>
 * The Role of Exceptions An exception is Java’s way of saying, “I give up.
 * I don’t know what to do right now. You deal with it.” When you write a method,
 * you can either deal with the exception or make it the calling code’s problem.
 * <p>
 * Checked (non-runtime) Exceptions
 * A checked exception includes Exception and all subclasses that do not extend RuntimeException.
 * Checked exceptions tend to be more anticipated — for example, trying to read a file that doesn’t exist.
 * Checked exceptions? What are we checking? Java has a rule called the handle or declare rule.
 * For checked exceptions, Java requires the code to either handle them or declare them in the method signature.
 * <p>
 * Checked Exceptions can be re-thrown or handled in a try-catch block.
 * The code in the try block is run normally. If any of the statements throw an exception that can be caught
 * by the exception type listed in the catch block, the try block stops running and execution goes to the catch
 * statement. If none of the statements in the try block throw an exception that can be caught,
 * the catch clause is not run.
 * A finally clause can be added to try-catch - finally is typically used to close resources such as files or databases
 * both of which are topics on the OCP exam.
 * <p>
 * Learn the difference between the keywords "throw" (used to actually throw an Exception)
 * and "throws" (used to declare that a method MIGHT throw an Exception).
 * <p>
 * We should ideally deal with an Exception once. Don't keep handling and throwing.
 * <p>
 * Throwing the Exception super class Exception is considered lazy and not best practice.
 * The same goes for handling only Exception.
 * <p>
 * If unsure how to handle an Exception, throw it to the calling class. If each class in the calling
 * stack keeps throwing the Exception, it will end up at the main method and from there passed to the JVM.
 * The JVM will print the stack trace and stop execution of the program.
 * <p>
 * For the exam, you need to know that these are Checked Exceptions: -
 * FileNotFoundException - Thrown programmatically when code tries to reference a file that does not exist.
 * IOException - Thrown programmatically when there’s a problem reading or writing a file.
 *
 */
public class CheckedExceptionTester {

    void fall() throws Exception {
        throw new Exception();
    }

    //It is fine for a main method to throw an Exception.
    public static void main(String[] args){
        //Thread.sleep throws a checked Exception(java.lang.InterruptedException).
        // If we do not handle it or throw it, we get a compiler error.
        //JH - demo both handling and throwing.
        //Thread.sleep(1000);

        //Similarly, the fall method above throws a checked Exception(java.lang.Exception), which must be handled.
        CheckedExceptionTester tester = new CheckedExceptionTester();
        //tester.fall();

        //JH - uncomment and explain the following two calls.
        //This approach forces the developer to handle a possible Exception. The undelying getAnimal method
        // is defensive in that it will not return a null object, but it would throw an Exception instead.
        //This eliminates the possibility of NullPointerException
        Animal animal = null;
        try {
            animal = tester.getAnimal("squirrel");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(animal.getName());

        //This approach places emphasis on the developer to write defensive code
        // as the underlying getAnotherAnimal method may return a null object.
        //Animal possibleNullAnimal = tester.getAnotherAnimal("squirrel");
        //System.out.println(possibleNullAnimal);
        //System.out.println(possibleNullAnimal.getName()); //Will cause a NullPointerException.
        //So surround in defensive code.

        //tester.tryAndFinallyOnlyMethod("mule");
        //tester.tryAndFinallyOnlyMethod("tiger");

    }

    //How to explicitly throw an Exception.
    //
    //The developer has taken a design decision to throw a Checked Exception
    //if the getAnimal method returns a null Animal.
    //
    //This force client classes to deal with that possibility by handling or declaring the Exception.
    public static Animal getAnimal(String type) throws Exception {
    //protected Animal getAnimal(String type) throws Exception {// Alternative method structure - broader Exception.
        System.out.println("In getAnimal(String type) method");
        Animal animal = AnimalListDataManager.getAnimal(type);
        if (animal == null) {
            //throw new InvalidAnimalException(type + " is an invalid animal. There are no animals of that type in this zoo.");

            //Or this can be thrown with no argument.
            //throw new InvalidAnimalException();

            //Or this can be thrown with no argument.
            throw new Exception();
        }
        return animal;
    }

    //The alternative to the above is NOT to throw a checked Exception here. This is much less
    //defensive version of the previous method and means that the client code should be
    //written more defensively to prevent possible NullPointerExceptions.
    public static Animal getAnotherAnimal(String type) {
        System.out.println("In getAnotherAnimal(String type) method");
        Animal animal = AnimalListDataManager.getAnimal(type);
        return animal;
    }

    //It's fine to have try-finally, but if the body of try throws an Exception, it must be
    //handled in a catch or thrown.
    //It is not allowed to have try only.
    private void tryAndFinallyOnlyMethod(String animalType){
        try{
            System.out.println("In try clause of tryAndFinallyOnlyMethod()");
            getAnotherAnimal(animalType);
        } finally {
            System.out.println("In finally clause of tryAndFinallyOnlyMethod()");
        }
    }
}
