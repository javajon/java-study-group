package exception;

/**
 * Created by rc00028 on 12/07/2017.
 */
public class ErrorTester {

    //This causes an Error (java.lang.ExceptionInInitializerError).
    //The ExceptionInInitializerError is an error because Java failed to load the whole class.
    //This failure prevents Java from continuing.
    static {
        int[] countsOfMoose = new int[3];
        //int num = countsOfMoose[-1];
    }

    public static void main(String[] args) {
        //doNotCodeThis(2);
    }

    //This causes an Error (java.lang.StackOverflowError).
    //Since the method calls itself, it will never end. Eventually, Java runs out of room on
    //the stack and throws the error. This is called infinite recursion. It is better than
    //an infinite loop because at least Java will catch it and throw the error.
    // With an infinite loop, Java just uses all your CPU until you can kill it.
    public static void doNotCodeThis(int num) {
        doNotCodeThis(1);
    }

    //java.lang.NoClassDefFoundError is another Error.
    //This error won’t show up in code on the OCA exam -
    //you just need to know that it is an error.
    //NoClassDefFoundError occurs when Java can’t find the class at runtime.

    //The class may be deployed in a different env

}
