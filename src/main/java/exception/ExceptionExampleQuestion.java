package exception;

/**
 * Created by rc00028 on 12/07/2017.
 *
 * ExceptionExampleQuestion
 *
 * The study guide says:
 *
 * "(this is) the hardest example you can be asked related to exceptions."
 *
 * So if you follow this example, you should be ok!
 */
public class ExceptionExampleQuestion {

    public String exceptions() {
        String result = "";
        String v = null;
        try {
            try {
                result += "before";
                v.length();
                result += "after";
            } catch (NullPointerException e) {
                result += "catch";
                throw new RuntimeException();
            } finally {
                //This gets to finally before throwing the RuntimeException
                result += "finally";
                //This gets thrown before we have a chance to throw the RuntimeException
                throw new Exception();
            }
        } catch (Exception e) {
            result += "done";
        }
        return result;
    }

    public static void main(String[] args) {
        ExceptionExampleQuestion test = new ExceptionExampleQuestion();
        String exceptionResultString = test.exceptions();
        System.out.println(exceptionResultString);
    }

}