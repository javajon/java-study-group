package exception;

import encapsulation.Swan;
import org.apache.commons.lang3.StringUtils;
import zoo.animal.Animal;
import zoo.animal.Lion;
import zoo.animal.Tiger;

/**
 * Created by rc00028 on 11/07/2017.
 * <p>
 * RuntimeExceptionTester
 * <p>
 * The Role of Exceptions An exception is Java’s way of saying, “I give up.
 * I don’t know what to do right now. You deal with it.” When you write a method,
 * you can either deal with the exception or make it the calling code’s problem.
 *
 * Runtime (unchecked) Exceptions
 * A runtime exception is defined as the RuntimeException class and its subclasses.
 * Runtime exceptions tend to be unexpected but not necessarily fatal. They generally
 * indicate that code has not been written defensively enough.
 * For example, accessing an invalid array index is unexpected.
 * Runtime exceptions are also known as unchecked exceptions because you don't have to handle them.
 *
 * They can generally be anticipated and defensively coded against. They may occur in the early stages
 * of dev/test, but should be "weeded out" so that they don't occur in live.
 *
 * They do not have to be handled in the same way that Checked Exceptions do.
 * If every one had to be handled, the code would be full of Exception handling!
 * Instead they should be guarded against.
 * <p>
 */
public class RuntimeExceptionTester {

    public static void main(String[] args) {
        int[] intArray = new int[2];
        intArray[0] = 1;
        intArray[1] = 2;

        //This causes a RuntimeException (java.lang.ArrayIndexOutOfBoundsException).
        //This is an unchecked exception - i.e. we do not have to anticipate it with a try - catch block.
        //System.out.println(intArray[2]);

        //This would be better handled defensively: -
        if (intArray.length > 2) {
            System.out.println(intArray[2]);
        }

        //JH - return to this after demonstrating Checked Exceptions.
        //Note that you CAN handle RuntimeExceptions, but this is not encouraged.
        //It is far better to write defensive code.
        /*
        try {
            System.out.println(intArray[2]);
        }catch(ArrayIndexOutOfBoundsException aioobe){
            aioobe.printStackTrace();
        }
        */
        //This causes a RuntimeException (java.lang.NullPointerException).
        //This is an unchecked exception - i.e. we do not have to anticipate it with a try - catch block.
        Animal nullTiger = null;
                //System.out.println(nullTiger.getName());

        //This would be better handled defensively: -
        if (nullTiger != null) {
//            System.out.println(nullTiger.getName());
        }

        //This causes a RuntimeException (java.lang.ClassCastException).
        //This is an unchecked exception - i.e. we do not have to anticipate it with a try - catch block.
        Animal lion = new Lion("Leia");
        //Tiger wrongTiger = (Tiger)lion;

        //This would be better handled defensively: -
        if (lion instanceof Tiger) {
            Tiger tentativeTiger = (Tiger)lion;
        }

        //This causes a RuntimeException (java.lang.ArithmeticException).
        //This is an unchecked exception - i.e. we do not have to anticipate it with a try - catch block.
        //Trying to divide an int by zero gives an undefined result. When this occurs, the JVM will throw an ArithmeticException:
        int numerator = 11;
        int denominator = 0;
        int answer = 0;
        //answer = numerator/denominator;

        //This would be better handled defensively: -
        if (denominator > 0) {
            answer = numerator/denominator;
        }

        //This causes a RuntimeException (java.lang.IllegalArgumentException).
        //This is an unchecked exception - i.e. we do not have to anticipate it with a try - catch block.
        int desiredNoOfEggs = -1;
        Swan swan = new Swan();
        //swan.setNumberEggs(desiredNoOfEggs);

        //This would be better handled defensively: -
        if (desiredNoOfEggs >= 0) {
            swan.setNumberEggs(desiredNoOfEggs);
        }

        //This causes a RuntimeException (java.lang.NumberFormatException ).
        //This is an unchecked exception - i.e. we do not have to anticipate it with a try - catch block.
        String stringToConvertIntoAnInt = "abc";
        //Integer.parseInt(stringToConvertIntoAnInt);

        //This would be better handled defensively: -
        if (StringUtils.isNumeric(stringToConvertIntoAnInt)) {
            Integer.parseInt(stringToConvertIntoAnInt);
        }

    }

}
