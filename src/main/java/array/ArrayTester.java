package array;

import java.util.Arrays;

/**
 * ArrayTester
 *
 * Created by jon on 05/06/2017.
 *
 * Demonstrating the initialisation and usage of the following types of arrays: -
 *
 * 1. The String[] args array used in main methods.
 * 2. Arrays of primitive data types.
 * 3. Arrays of object references.
 * 4. Multi-dimensional arrays.
 *
 * It is important to remember that arrays are objects - even arrays of primitives.
 *
 * Arrays can be searched, but they should first be sorted to give meaningful search results.
 *
 *
 */
public class ArrayTester {
    public static void main(String[] args) {

        System.out.println("We are running ArrayTester.java");

        //An array can be of any Java type - primitive or object.
        //An array is an ordered list. It can contain duplicates.

        //Allowable constructs for the args array in.the main method: -
        //JH - demo all these and demo an invalid one to show that the program won't start.
        //public static void main(String[] args)
        //public static void main(String[] bananas)
        //public static void main(String args[])
        //public static void main(String... args) // varargs - Chapter 4
        //Invalid: -
        //public static void main(String args) //This compiles but won't run.

        //Accessing an element from the args array.
        //JH - demo this first by uncommenting below but with nothing passed in as args. Then correct by passing some args
        String arg1 = args[0];

        //Assigning the other direction. Need to make sure an arg is passed in first though
        args[0] = "Jon";

        //An array can be produced by calling the String.split() method. Another useful String method.
        String javaDevs = "Jon.Glenn.Guy.Giles.Andy.Peter";

        String[] javaDevsArray = javaDevs.split("\\.");
        for (String javaDev: javaDevsArray){
            System.out.println("javaDev: " + javaDev);
        }

        //1. Declaring array without explicitly initialising it.
        //Arrays declared this way must be sized.
        int[] defaultNumbers = new int[10];
        defaultNumbers[2] = 2;
        //int[] defaultNumbersInvalid = new int[]; //This will not compile - a restriction if arrays is that they must be sized up front.

        //With primitives, all values will have the default values - 0 in this case. Objects by dafault are null.
        System.out.println();
        for (int i = 0; i < defaultNumbers.length; i++) {
            System.out.println("default value of array element at index " + i + " = " + defaultNumbers[i]);
        }

        //2. Declaration and explicit initialisation of an array.
        int[] numbers2 = new int[] {42, 55, 99};

        //Above is fine, but contains redundant code. Shortcut way is shown below.
        int[] numbers3 = {42, 55, 0, 99};
        numbers3[2] = 4;

        //You can type the [] before or after the name, and adding a space is optional. This means that all four of these statements do the exact same thing:
        int[] numAnimals;

        int [] numAnimals2;
        int numAnimals3[];
        int numAnimals4 [];

        //The String and StringBuilder classes each maintain a char array under the hood.
        //JH - Demo this and show that the array only in String is final.
        //We could do our own String manipulation just using a char array if we wanted,
        //but then we wouldn't have access to all the handy String methods.
        //Direct instantiation of an array: -
        char[] charArray = {'h', 'e', 'l', 'l', 'o'};

        //Arrays are objects, so if we do this, we just get the object reference.
        System.out.println("\ncharArray: " + charArray);

        for(char c: charArray){
            System.out.print(c);
        }

        System.out.println("\n");

        //Array references can be pointed at other arrays.
        String [] bugs = { "cricket", "beetle", "ladybug" };
        String [] alias = bugs;
        System.out.println("bugs.equals(alias): " + bugs.equals(alias)); // true - arrays are objects and both references point to the saem object.

        String[] strings = { "stringValue" };

        //Arrays can be cast to arrays of other types.
        Object[] objects = strings; // Implicit casting is OK whent he cast is upwards in the inheritance tree - upcasting.
        //String[] againStringsInvalid = objects; // DOES NOT COMPILE - Implicit casting does not work when casting down the inheritance tree.
        String[] againStrings = (String[]) objects; // Explicit casting gets round this problem, but it is the developers responsibility to sanity check this.
        //againStrings[0] = new StringBuilder();   // DOES NOT COMPILE - wrong object type
        //objects[0] = new StringBuilder();        // Compiles but would throw a runtime exception, java.lang.ArrayStoreException, because we are trying to store the rong type.

        //Initialising and printing elements of a String array.
        //length is a property, not a method, that can be directly accessed to show how many elements in the array: -
        String[] mammals = {"monkey", "chimp", "donkey"};
        System.out.println("\nmammals.length: " + mammals.length);
        System.out.println("mammals[0]: " + mammals[0]);
        System.out.println("mammals[1]: " + mammals[1]);
        System.out.println("mammals[2]: " + mammals[2]);
 //       System.out.println("mammals[3]: " + mammals[3]); //Compiles but leads to a runtime exception, java.lang.ArrayIndexOutOfBoundsException.

        //Initialising an array with a set size. By default all object elements will be null.
        String[] birds = new String[6];
        //System.out.println("\nbirds.length: " + birds.length);
        for (int i = 0; i < birds.length; i++) {
            System.out.println("birds[i]: " + birds[i]);
        }

        //Change and print the values in an int array - by default they are all 0.
        int[] numbers = new int[10]; //At this point the value of all elements is 0.
        //System.out.println("\n");
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = i + 5;
            System.out.println("numbers[i]: " + numbers[i]);
        }

        //Arrays are sortable.
        //Examine the difference between sorting numerical array to String arrays.
        //Sorting a numerical array.
        int[] numbersToSort = { 6, 9, 1 };
        Arrays.sort(numbersToSort); //Expect the elements to now be numerically arranged: 1, 6, 9
        //System.out.println();
        for (int i = 0; i < numbersToSort.length; i++){
            System.out.println("numbersToSort[" + i + "]: " + numbersToSort[i]);
        }

        String[] stringsToSort = { "10", "9", "100" };
        Arrays.sort(stringsToSort); //Expect the elements to now be alphabetically arranged: 10, 100, 9
        System.out.println();
        for (String string : stringsToSort){
            System.out.println("stringsToSort String: " + string);
        }

        //Arrays are searchable.
        //But for an array search to give meaningful, predictable results, the array needs to be sorted first.
        numbers = new int[] {3,2,1};
        System.out.println("\nArrays.binarySearch(numbers, 1) - non-sorted: " + Arrays.binarySearch(numbers, 1));
        System.out.println("Arrays.binarySearch(numbers, 2) - non-sorted: " + Arrays.binarySearch(numbers, 2));
        System.out.println("Arrays.binarySearch(numbers, 3) - non-sorted: " + Arrays.binarySearch(numbers, 3));

        numbers = new int[] {3,2,1};
        Arrays.sort(numbers);
        System.out.println("\nArrays.binarySearch(numbers, 1) - sorted: " + Arrays.binarySearch(numbers, 1));
        System.out.println("Arrays.binarySearch(numbers, 2) - sorted: " + Arrays.binarySearch(numbers, 2));
        System.out.println("Arrays.binarySearch(numbers, 3) - sorted: " + Arrays.binarySearch(numbers, 3));
        System.out.println();

        //Arrays can be multi-dimensional.
        //A 2-dimensional array is an array of arrays.
        //A 3-dimensional array is an array of arrays of arrays.
        //A 4-dimensional array is an array of arrays of arrays of arrays! etc, etc.
        //Initialising a multi dimensional array.
        String[][] birdsMulti = new String[3][3];

        //In a 2-d array, both dimensions are required to access the individual elements.
        //Add garden birds to the first nested array.
        birdsMulti[0][0] = "Robin";
        birdsMulti[0][1] = "Blackbird";
        birdsMulti[0][2] = "Thrush";

        //Add birds of prey to the second nested array.
        birdsMulti[1][0] = "Hawk";
        birdsMulti[1][1] = "Eagle";
        birdsMulti[1][2] = "Falcon";

        //Add domestic birds to the third nested array.
        birdsMulti[2][0] = "Parrot";
        birdsMulti[2][1] = "Pidgeon";
        birdsMulti[2][2] = "Mynah bird";

        //Use a nested for to get to the different dimensions.
        for(int i = 0; i < birdsMulti.length; i++){

            System.out.println("Birds array " + (i + 1) + "\n===================");
            String [] innerArray = birdsMulti[i];
            for(int j = 0; j < innerArray.length; j++){
                System.out.println(innerArray[j]);
            }
            System.out.println();
        }

        //Same as above but using for shortcut.
        for(String[] innerArray : birdsMulti){

            System.out.println("Inner Birds array " + "\n===================");
            for(String bird : innerArray){
                System.out.println(bird);
            }
            System.out.println();
        }

        //Add birds of prey to the second nested array.
        //The inner dimension is handled in initialising a new String array.
        String[] newBirdsOfPrey = {"Kestrel", "Vulture", "Buzzard"};

        //We only need to access one dimension of the outer array to set this.
        birdsMulti[1] = newBirdsOfPrey;
        birdsMulti[1][1]="Sparrowhawk";

        //Multi-dimensional array with different sized nested arrays.
        int[][] differentSize = {{1, 4}, {3}, {9,8,7}};

        //Multi-dimensional array with different sized nested arrays - only the first dimension is initialised on declaration.
        int [][] multiArray = new int[4][];
        multiArray[0] = new int[5];
        multiArray[1] = new int[3];
        multiArray[2] = new int[4];
        multiArray[3] = new int[2];
        //A java.lang.ArrayIndexOutOfBoundsException occurs if we try to access elements outside the initialised bounds of the array.
        //multiArray[4] = new int[1];

        //Setting values on the inner array.
        multiArray[2][0] = 1;
        multiArray[2][1] = 2;
        multiArray[2][2] = 3;
        multiArray[2][3] = 4;
        //Again, a java.lang.ArrayIndexOutOfBoundsException occurs if we try to access elements outside the initialised bounds of the array.
        //multiArray[2][4] = 5;



        //Different ways of declaring multi-dimensional arrays.
        int[][] vars1; // 2D array
        int[][] vars1a, vars1b, vararray2; // 3 2D arrays
        int vars2 [][]; // 2D array
        int[] vars3[]; // 2D array
        int[] vars4 [], space [][]; // a 2D AND a 3D array
    }
}
