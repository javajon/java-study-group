package string;

/**
 *
 * StringImmutablityTester
 *
 * Created by jon on 16/05/2018.
 * <p>
 * Demonstrating immutability in the String class.
 * <p>
 */
public class StringImmutablityTester {
    public static void main(String[] args) {

        //Strings are immutable
        //As well as using the + operator, the method concat can also be used to concatenate Strings.
        String one = "1";
        String concatString = one.concat("2");
        System.out.println("1. Using concat on Strings: " + concatString);

        //The following line demonstrates that Strings are immutable. A new String is created in memory, but no reference is pointing at it. The original reference still points the the String from the line before.
        concatString = concatString.concat("3");
//        System.out.println(concatString.concat("3"));
        System.out.println("2. Using concat on Strings: " + concatString);

        //JH - show Sachin Tendulkar diagram example demonstrating immutability.
        //We need to eplictily assign it to make our variable reference point at it: -
        concatString = concatString.concat("3");
        //System.out.println("3. Using concat on Strings: " + concatString);

        //Futile concat call - resultant String is unreachable without a variable reference.
        String s = "Sachin";
        s.concat(" Tendulkar");
        System.out.println(s);

        //Correcting the above using an explicit assignment.
        String s1 = "Sachin";
        s1 = s1.concat(" Tendulkar");
        System.out.println(s1);

        //The compound operator += can also be used on Strings. As it includes an assignment, immutability is not an issue.
        String assignmentString = "1";
        assignmentString += "2";
        assignmentString += 3;
        System.out.println("Compound operator on Strings: " + assignmentString);

        //Immutability. In this example, the reference is pointed to first one, then another object.
        //This is in contrast to changing the value of an object, with a setter, for example.
        String stringsAreImmutable = new String("immutable");
        stringsAreImmutable = "I am a different Object!";

        //The following demonstrates immutability.
        String hello = "Hello";
        hello.concat(" World");

        System.out.println(hello);

    }
}
