package string;

/**
 * StringPoolTester
 * <p>
 * Created by jon on 16/05/2018.
 * <p>
 * Demonstrating the String Pool, which is a feature within the JVM.
 * <p>
 * <p>Text from the Study Guide:
 * Since strings are everywhere in Java, they use up a lot of memory.
 * In some production applications, they can use up 25–40 percent of the memory in the entire program.
 * Java solves this issue by reusing common ones. The string pool, also known as the intern pool,
 * is a location in the Java virtual machine (JVM) that collects all these strings.
 * The string pool contains literal values that appear in your program.
 * For example, “name” is a literal and therefore goes into the string pool.
 * myObject.toString() is a string but not a literal, so it does not go into the string pool.
 * Strings not in the string pool are garbage collected just like any other object.
 *
 * String name = "Fluffy"; //String pool
 * String name = new String("Fluffy"); //POJO (Plain old Java Object) - not in the String pool.
 * In the second example, use of 'new' forces the JVM not to use th String pool.
 *
 * In these examples, note the difference in behaviour when testing for Object equality using ==
 * and logical equality using equals()
 *
 */
public class StringPoolTester {
    public static void main(String[] args) {

        //Treating Java Strings as traditional Objects
        //NOTE: This would not normally be done as use of the String pool is more efficient.
        //But for the OCA exam, we need to know that we can treat Strings as normal objects.
        //JH - Show Object refernces diagram to support this.
        String s1 = new String("Hello World");
        String s2 = new String("Hello World");
        String s3 = s1;

        System.out.println("s1 == s2: " + (s1 == s2));
        System.out.println("s3 == s1: " + (s3 == s1));
        System.out.println("s3 == s2: " + (s3 == s2));

        //These Strings demonstrate the usage of the String pool.
        String s4 = "Hello World";
        String s5 = "Hello World";

        System.out.println("\ns4 == s5: " + (s4 == s5));

        //Checking for logical equality using equals method from Object.class
        System.out.println("\ns1.equals(s2): " + (s1.equals(s2)));
        System.out.println("s3.equals(s1): " + (s3.equals(s1)));
        System.out.println("s4.equals(s5): " + (s4.equals(s5)));

        System.out.println("\ns4 == s1: " + (s4 == s1));
        System.out.println("\ns4.equals(s1): " + (s4.equals(s1)));

    }
}
