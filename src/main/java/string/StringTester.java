package string;

/**
 * StringTester
 *
 * Created by jon on 01/06/2017.
 * <p>
 * Demonstrating methods on the java.lang.String class.
 * <p>
 * String is one of the most fundamental classes in the Java language.
 * <p>
 * A String represents a serious of characters - words, sentences, etc.
 * <p>
 * String is an Object not a primitive, but due to it's special behaviour it can sometimes be treated as a primitive
 */
public class StringTester {

    public static void main(String[] args) {

        int i = 1;

        //JH - demo that the String class cannot be subclassed.

        //Different way of instantiating Strings: -

        //1. Like a traditional Java object.
        String myName = new String("jon");

        //2. Shortcut using String pool. This is special to Strings.
        // Other Java objects cannot do this - they need 'new' to create a new object.
        String name = "jon";

        //Testing equality.
        System.out.println("name == myName?");
        System.out.println(name == myName);
        System.out.println("\nname.equals(myName)?");
        System.out.println(name.equals(myName));


        //The + operator, when used on Strings, serves to concatenate them.
        // This is often seem in System.out statements. This is another special
        // feature in Strings that doesn't apply to other Java Objects.
        String statement1 = "String = " + 1 + 2;
        String statement2 = "String " + (1 + 2);
        String statement3 = 1 + 2 + " String";

        //JH - explain the usage of \n - escaped newline character.
        System.out.println("\nstatement1: " + statement1);
        System.out.println("statement2: " + statement2);
        System.out.println("statement3: " + statement3);

        //As well as using the + operator, the method concat can also be used to concatenate Strings.
        //String methods will be numbered for reference
        //1. concat
        String one = "1";
        String concatString = one.concat("2");
        System.out.println("\nconcat: " + concatString);

        //Strings are immutable
        //JH - demonstrate immutability in the StringImmutablityTester clss.

        //The compound operator += can also be used on Strings.
        String s = "1";
        s += "2";
        s += 3;
        System.out.println("\nUsing compound += operator on Strings: " + s);

        //JH - demonstrate the String pool in the StringPoolTester class.

        //String utility methods: -
        String tenCharacters = "0123456789";

        //2. length
        int length = tenCharacters.length();
        System.out.println("\nlength: " + length);

        //3. charAt
        char myChar = tenCharacters.charAt(2);
        System.out.println("\ncharAt: " + myChar);

        //JH - change the value at pos 2 to a char to illustrate.

        //4. indexOf - demonstrate several overloaded methods - overloaded methods play a big part of chapter 5.
        //Note that like arrays, lists, etc Strings are indexed starting at 0.
        //Note that a non-match results in -1.
        String animals = "animals";

        int indexOf1 = animals.indexOf('n');
        System.out.println("\nindexOf1: " + indexOf1);

        int indexOf2 = animals.indexOf("al");
        System.out.println("indexOf2: " + indexOf2);

        int indexOf3 = animals.indexOf('a', 4);
        System.out.println("indexOf3: " + indexOf3);

        int indexOf4 = animals.indexOf("al", 5);
        System.out.println("indexOf4: " + indexOf4);

        //5. substring - contrast overloaded methods.
        String substring1 = animals.substring(3);
        System.out.println("\nsubstring1: " + substring1);

        String substringVariation2 = animals.substring(3, 4);
        System.out.println("substringVariation2: " + substringVariation2);

        //Chaining - the example below demonstrates chaining - result of String.indexOf is input to String.substring.
        String chainingStringMethods = animals.substring(animals.indexOf('n'));
        System.out.println("\nchainingStringMethods: " + chainingStringMethods);

        //Note: The above is effectively the same as doing this: -
        String chainingStringMethods2 = animals.substring(1);

        String animal2 = "aNiMaL";
        String animal3 = "aNIMaL";

        //6. toLowerCase
        String toLowerCase = animal2.toLowerCase();
        System.out.println("\ntoLowerCase: " + toLowerCase);

        //7. toUpperCase
        String toUpperCase = animal2.toUpperCase();
        System.out.println("\ntoUpperCase: " + toUpperCase);

        //8. equals (overridden from Object class)
        boolean equals = animal2.equals(animal3);
        System.out.println("\nequals: " + equals);
        System.out.println(animal2 == animal3);

        //9. equalsIgnoreCase
        boolean equalsIgnoreCase = animal2.equalsIgnoreCase(animal3);
        System.out.println("\nequalsIgnoreCase: " + equalsIgnoreCase);

        //10. startsWith
        boolean startsWithPositive = animal2.startsWith("aN");
        System.out.println("\nstartsWithPositive: " + startsWithPositive);
        boolean startsWithNegative = animal2.startsWith("annnnn");
        System.out.println("startsWithNegative: " + startsWithNegative);

        //11. endsWith
        boolean endsWithPositive = animal2.endsWith("aL");
        System.out.println("\nendsWithPositive: " + endsWithPositive);
        boolean endsWithNegative = animal2.endsWith("Lion");
        System.out.println("endsWithNegative: " + endsWithNegative);

        //12. contains
        boolean containsPositive = animal2.contains("NiM");
        System.out.println("\ncontainsPositive: " + containsPositive);
        boolean containsNegative = animal2.contains("NIM");
        System.out.println("containsNegative: " + containsNegative);

        //13. trim - very handy for example in processing record sets, processed data split into arrays, etc.
        String trailingSpace = "12345 ";
        System.out.println("\ntrailingSpace: " + trailingSpace + trailingSpace);

        String noTrailingSpace = trailingSpace.trim();
        System.out.println("trim (results in no trailing Space): " + noTrailingSpace + noTrailingSpace);

        //14. replace
        String replace = "ANIMal ".trim().toLowerCase().replace('a', 'A');
        System.out.println("\nreplace (withing another chained String): " + replace + ".");

        //15. toString() - not needed for exam.
        System.out.println("\ntoString() : " + replace.toString());

        //Using toString() compiles but is redundant here. It has the same effect as just doing this line.
        System.out.println(replace);

        //16. split() - This is covered and demonstrated in the ArrayTester class.

    }
}
