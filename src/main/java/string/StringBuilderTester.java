package string;

/**
 * StringBuilderTester
 * <p>
 * Created by jon on 05/06/2017.
 * <p>
 * Demonstrating the java.lang.StringBuilder class.
 * <p>
 * This class maintains an internal String representation as a char array.
 * This minimises the object creation involved in preforming manipulations
 * on a traditional String, due to the java.lang.String class being immutable.
 * <p>
 * A key point to remember is that String instances are immutable, but StringBuilder instances are mutable.
 *
 * NOTE: StringBuffer (not on OCA) exam, is almost identical to StringBuilder but is less performant due to being threadsafe.
 *
 */
public class StringBuilderTester {

    public static void main(String[] args) {

        //Creating an alphabet String incrementally.
        //Approach 1 - using Strings and the + operator. String.concat() would have the same effect.
        String string = "";

        //This involves creating 27 String objects.
        System.out.println("Building the alphabet using String objects (creates 27 objects): -");
        //NOTE: This also demonstrates usage of char instead of normal int i = 0 in a for loop.
        for (char current = 'a'; current <= 'z'; current++) {
            string += current;
            System.out.println(string);
        }

        //StringBuilder methods are numbered here for reference.
        //1. append()
        //Approach 2 - using StringBuilder and the append() method.
        //Be aware of the three possible constructors for SB: -
        //StringBuilder stringBuilder = new StringBuilder(); //Using no args constructor

        //StringBuilder stringBuilder = new StringBuilder(10); //Using an initial size for the underlying char array.
        //NOTE: The StringBuilder class will dynamically resize its char array as needed.

        StringBuilder stringBuilder = new StringBuilder("a"); //Using a String argument to prime the StringBuilder.
        // NOTE: When using the String arg constructor, ensure that current starts at 'b' in the for loop.

        //This involves creating 1 StringBuilder object.
        //As StringBuilder maintains a char array and not String objects,
        //the object proliferation observed in approach 1 is avoided.
        System.out.println("\nBuilding the alphabet using a StringBuilder (creates 1 object): -");
        for (char current = 'b'; current <= 'z'; current++) {
            stringBuilder.append(current);
            System.out.println(stringBuilder);
        }

        //String is immutable. As demonstrated below. A new String is created but as it is not assigned, it is unreachable.
        //string.concat("0123456789");
        //An explicit assignment (below) would result in the concat not being lost.
        string = string.concat("0123456789");
        System.out.println("\nString.concat() result:\n" + string);

        //StringBuilder is not immutable. Even without assignment, the change is not lost here
        stringBuilder.append("0123456789");
        //An explicit assignment (below) compiles fine, but is redundant.
        //stringBuilder = stringBuilder.append("0123456789");
        System.out.println("\nStringBuilder.append() result:\n" + stringBuilder);

        //2. charAt(), 3. indexOf(), 4. length(), and 5. substring() - These four methods work exactly the same as in the String class.
        StringBuilder sb = new StringBuilder("animals");
        String sub = sb.substring(sb.indexOf("a"), sb.indexOf("al")); //Expecting "anim" - the indices evaluate to 0 and 4.
        System.out.println("\nsubstring() and indexOf():\n" + sub);

        int len = sb.length(); //length of "animals" is 7

        char ch = sb.charAt(6); //the char at position 6 of "animals" is s

        String newString = sub + " " + len + " " + ch;

        System.out.println("\ncharAt() and length():\n" + newString); //Expecting "anim 7 s"
        //anim 7 s

        //6. insert()
        sb = new StringBuilder("animals");
        sb.insert(7, "-"); // sb String value is now "animals-"
        System.out.println("\ninsert():\n" + sb);

        sb.insert(0, "-"); // sb String value is now "-animals-"
        System.out.println(sb);

        sb.insert(4, "-"); // sb String value is now "-ani-mals-"
        System.out.println(sb);

        //7. delete.
        //NOTE: By using new, we are creating a new object on the heap. The old object has no pointers and is eligible for gc.
        sb = new StringBuilder("abcdef");
        sb.delete(1, 3); // sb String value is now "adef". The substring "bc" is removed.
        System.out.println("\ndelete():\n" + sb);

        //8. deleteCharAt.
        //This would throw an Exception of type java.lang.StringIndexOutOfBoundsException.
        //sb.deleteCharAt(5);
        //This is Ok though.
        sb.deleteCharAt(3); // sb String value is now "ade". The char 'f' is removed.
        System.out.println("\ndeleteCharAt():\n" + sb);

        //9. reverse();
        sb = new StringBuilder("ABC");
        sb.reverse();
        System.out.println("\nreverse():\n " + sb);

        //10. toString()
        //NOTE: When we print an object we are, by default calling the toString() method.
        //So in the initial example of creating the alphabet, by virtue of the print statements,
        //we are creating more object than we claim - as many as the String example!
        //JH - show that StringBuilder has overidden the toString() method from Object.
        //JH - show that is passes it's char array to a conctructor of String.
        String s = sb.toString();
        System.out.println("\ntoString():\n" + s);

        //Testing equality using == and equals().
        //This is a good example of what happens when the equals() method is not overidden as is the case here.
        //JH - show that when equals() is clicked on, we go straight to the Object class.
        //JH - show that, if equals is not overidden, it behaves exactly as ==
        //i.e. it returns true only if both references point ot the same physical object.
        //Return to StringTester and show that equals is overidden in String.
        StringBuilder one = new StringBuilder("Hello");
        StringBuilder two = new StringBuilder("Hello");

        String oneS = new String("Hello");
        String twoS = new String("Hello");

        System.out.println("\n== and equals()\none == two?" + (one == two));
        System.out.println("\n== and equals()\noneS == twoS?" + (oneS == twoS));

        System.out.println("one.equals(two)?" + (one.equals(two)));
        System.out.println("oneS.equals(twoS)?" + (oneS.equals(twoS)));

        StringBuilder three = one.append("a");

        System.out.println("one == two?" + (one == two));
        System.out.println("one.equals(two)?" + (one.equals(two)));
        System.out.println("one == three?" + (one == three));
        System.out.println("one.equals(three)?" + (one.equals(three)));

        //JH - contrast this with the behaviour in StringTester
    }
}
