package primitivevariabletypes;

import sun.plugin2.main.server.CookieSupport;

/**
 * Created by u.7829872 on 06/12/2017.
 */
public class VariablesTypesExamGotchas {
    //Objects can be initialised as null;
    CookieSupport cookieSupport = null;

    //Primitives cannot be initialised as null;
    //int nullInt = null;    //DOES NOT COMPILE

    byte maxByte = 127;

    //Too big - does not compile.
    //byte biggerThanMaxByte = 128;

    int maxInt = 2147483647;
    //Too big - does not compile.
    //int biggerThanMaxInt = 2147483648;

    //Numeric non floating point literals are ints by default
    long maxIntAsLong = 2147483647;
    long biggerThanMaxIntAsLong = 2147483648L;

    long anotherLong = 6L;


    //Numeric floating point literals are doubles by default
    double myDouble = 0.7373663;
    float myFloat = 0.32F;
    //JH show F suffix and casting solutions.


    public static void main(String[] args) {

        System.out.println(2147483647);
    }
}
