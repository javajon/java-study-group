package primitivevariabletypes;

/**
 * Created by rc00028 on 05/05/2017.
 */
public class TestClassAndInstancePrimitives {

    //Instance variables.
    //1 bit.
    boolean myBoolean;

    //8 bits
    //A byte can hold 2^8 (2 raised to the power of 8) possible values.
    // 2^8 = 256 possible values.
    //As it can be negative, the range of possible values a byte can hold is -128 to 127.
    byte myByte;

    //16 bits
    short myShort;

    //32 bit
    //int is the default numeric type.
    int myInt;

    //64 bit
    long myLong;

    //32 bit
    float myFloat;

    //64 bit
    double myDouble;

    //32 bit
    //Unsigned int - holds characters
    //Default value 'u0000' - null character
    char myChar;

    //Objects
    String myString;
    Object myObject;

    //Class or static variables
    static int myStaticInt;

    public static void main(String[] args) {

        //Can't access this directly.
        //System.out.println(myChar);

        //Can access this directly.
        System.out.println(myStaticInt);

        TestClassAndInstancePrimitives test = new TestClassAndInstancePrimitives();

        System.out.println("myBoolean value: " + test.myBoolean);
        System.out.println("myByte value: " + test.myByte);
        System.out.println("myShort value: " + test.myShort);
        System.out.println("myInt value: " + test.myInt);
        System.out.println("myStaticInt value: " + TestClassAndInstancePrimitives.myStaticInt);
        System.out.println("myLong value: " + test.myLong);
        System.out.println("myFloat value: " + test.myFloat);
        System.out.println("myDouble value: " + test.myDouble);
        System.out.println("myChar value: " + test.myChar);
        System.out.println("myString value: " + test.myString);

//        System.out.println(test.myString.concat(" add this to my String"));
        System.out.println("myObject value: " + test.myObject);
        int myLocalInt;

        myLocalInt = 200;
        System.out.println("myLocalInt value: " + myLocalInt);

        test.performArithmetic();
    }

    public void performArithmetic(){

        //Increment operator
        System.out.println(myInt++);
        System.out.println(++myInt);

        int maxInt;

        maxInt = Integer.MAX_VALUE;
        System.out.println(maxInt);

        //Same as maxInt = maxInt - 1;
        maxInt -= 1;

        System.out.println(maxInt);
        System.out.println(++maxInt);
        System.out.println(++maxInt);

        //Operators are covered in chapter 2 - e.g. +, -, /, *, %, ++.




    }
}
