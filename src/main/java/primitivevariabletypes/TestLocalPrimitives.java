package primitivevariabletypes;

/**
 * Created by rc00028 on 05/05/2017.
 */
public class TestLocalPrimitives {

    public static void main(String[] args) {
        boolean myBoolean = false;

        byte myByte;
        myByte=100;

        short myShort;
        int myInt;
        long myLong;
        float myFloat;
        double myDouble;
        char myChar;
        String myString;
        Object myObject;

        TestLocalPrimitives test = new TestLocalPrimitives();

        System.out.println("This compiles and runs - but only because we are not attempting to reference the uninitialised variables");

//        if (true){
  //          int anotherInt=5;
    //    }

        myFloat=0.0F;
      //  anotherInt=6;
        /*System.out.println("myBoolean value: " + myBoolean);
        System.out.println("myByte value: " + myByte);
        System.out.println("myShort value: " + myShort);
        System.out.println("myInt value: " + myInt);
        System.out.println("myLong value: " + myLong);
        System.out.println("myFloat value: " + myFloat);
        System.out.println("myDouble value: " + myDouble);
        System.out.println("myChar value: " + myChar);
        System.out.println("myString value: " + myString);
        System.out.println("myObject value: " + myObject);
        */
    }

    public static void access(){
        //Method local variable invisible outside method in which it is declared.
        //System.out.println(myInt);
    }
}
