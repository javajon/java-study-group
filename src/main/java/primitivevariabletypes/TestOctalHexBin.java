package primitivevariabletypes;

/**
 * Created by rc00028 on 05/05/2017.
 */
public class TestOctalHexBin {

    int myDecimalInt = 11;
    // 1 1 1 1 1 1 1 1 1 1                      10
    // 1                                        1

    int myOctalInt = 011;
    // 1 1 1 1 1 1 1 1                          8
    // 1                                        1

    int myHexInt = 0x11;
    // 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1          16
    // 1                                        1

    //int myBinaryInt = 0b11;
    // 1 1                                      2
    // 1                                        1

    public static void main(String[] args) {
        TestOctalHexBin test = new TestOctalHexBin();
        System.out.println("myDecimalInt value: " + test.myDecimalInt);
        System.out.println("myOctalInt value: " + test.myOctalInt);
        System.out.println("myHexInt value: " + test.myHexInt);
 //       System.out.println("myBinaryInt value: " + test.myBinaryInt);
    }
}
