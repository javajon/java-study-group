package primitivevariabletypes;

import zoo.animal.Animal;
import zoo.ZooFirstEffort;
import zoo.animal.Tiger;

/**
 * Created by rc00028 on 05/05/2017.
 */
public class TestPrimitivesInitialisation {

    //Primitives
    boolean myBoolean = true, yourBoolean;
    byte myByte = 1, d, e,r,t,y,u,i,o;
    short myShort = 232;
    int myInt = 28837;
    long myLong;
    float myFloat = 0.32F;
    double myDouble = 0.7373663;
    char myChar = 'c';

    //Objects
    ZooFirstEffort myZoo = new ZooFirstEffort();
    Object myObject = new Tiger("Jerry");

    public static void main(String[] args) {
        TestPrimitivesInitialisation test = new TestPrimitivesInitialisation();
        test.myLong = 2147483648L;
        System.out.println("myBoolean value: " + test.myBoolean);
        System.out.println("yourBoolean value: " + test.yourBoolean);
        System.out.println("myByte value: " + test.myByte);
        System.out.println("myShort value: " + test.myShort);
        System.out.println("myInt value: " + test.myInt);
        System.out.println("myLong value: " + test.myLong);
        System.out.println("myFloat value: " + test.myFloat);
        System.out.println("myDouble value: " + test.myDouble);
        System.out.println("myChar value: " + test.myChar);
        System.out.println("myZoo value: " + test.myZoo);
        System.out.println("myObject value: " + test.myObject);
    }
}
