package conditionals;

import java.time.DayOfWeek;
import java.util.Calendar;

/**
 * Created by rc00028 on 31/05/2017.
 */
public class SwitchStatementWithEnum {

    public static void main(String[] args) {

        //Demonstrate use of Enums;
        DayOfWeek monday = DayOfWeek.MONDAY;
        System.out.println(monday);
        DayOfWeek sunday = DayOfWeek.SUNDAY;
        System.out.println(sunday);

        System.out.println("Calendar.SUNDAY: " + Calendar.SUNDAY);
        System.out.println("DayOfWeek.SUNDAY.getValue(): " + DayOfWeek.SUNDAY.getValue());

        System.out.println("Calendar.MONDAY: " + Calendar.MONDAY);
        System.out.println("DayOfWeek.MONDAY.getValue(): " + DayOfWeek.MONDAY.getValue());
        System.out.println("Calendar.MONDAY - 1: " + (Calendar.MONDAY - 1));


        Calendar cal = Calendar.getInstance();










        int dayOfWeekAsInt = cal.get(Calendar.DAY_OF_WEEK);

        int dows = cal.get(Calendar.DAY_OF_WEEK) - 1;

        DayOfWeek dow = DayOfWeek.of(dows);


        switch (dow) {
            case MONDAY:
                System.out.println("It's Monday, nothing needs more to be said!");
                break;
            case TUESDAY:
                System.out.println("It's Tuesday, enough said!");
            case WEDNESDAY:
                System.out.println(dows + " " + dow);
            case THURSDAY:
                System.out.println("  It's midweek, so what!");
                break;
            case FRIDAY:
                System.out.println("Cooking on Gas! It's Friday!");
                break;
            case SATURDAY:
                System.out.println("Great. It's Saturday!!");
                break;
            case SUNDAY:
                System.out.println("Well it is the Sabbath, a day of rest");
                break;
            default:
                System.out.println("Wow! What Calendar are you using?");
                break;
        }

    }
}
