package conditionals;

import java.time.DayOfWeek;
import java.util.Calendar;

/**
 * Solution to Chapter 2 homework issued 26/05/2017.
 * Submitted by Richard.
 */
public class SwitchWithEnum {
    public static void main(String[] args) {

        Calendar cal = Calendar.getInstance();
        int dows = cal.get(Calendar.DAY_OF_WEEK) - 1;

      //  Calendar.SUNDAY

        //allow for the special case of Sunday;
        dows = dows == 0 ? 7 : dows;

        DayOfWeek dow = DayOfWeek.of(dows);

        switch (dow) {
            case MONDAY:
                System.out.println("It's Monday, nothing needs more to be said!");
                break;
            case TUESDAY:
                System.out.println("It's Tuesday, enough said!");
            case WEDNESDAY:
                System.out.println(dows + " " + dow);
            case THURSDAY:
                System.out.println("  It's midweek, so what!");
                break;
            case FRIDAY:
                System.out.println("Cooking on Gas! It's Friday!");
                break;
            case SATURDAY:
                System.out.println("Great. It's Saturday!!");
                break;
            case SUNDAY:
                System.out.println("Well it is the Sabbath, a day of rest");
                break;
            default:
                System.out.println("Wow! What Calendar are you using?");
                break;
        }
    }
}
