/**
 * Created by jhead on 07/02/2018.
 * <p>
 * If Statements
 * =============
 * <p>
 * Tf is comprised of the following clauses: -
 * <p>
 * if
 * if else
 * else
 * <p>
 * if else and else are not required.
 * <p>
 * For if and if else, the condition must evaluate to a boolean.
 */
package conditionals;

import zoo.animal.Animal;

public class IfStatement {

    static String statement;
    static int value = 3;

    public static void main(String[] args) {

        //Standard if statement
        //System.out.println("With brackets (please do this - for me!): -\n");

        if (value >= 3) {
            statement = "equal to or greater than 3";
        } else if (value > 4) {
            statement = "this should be unreachable!";
        } else {
            statement = "less than 3";
            int x = 3;
            System.out.println(x);
        }

        //System.out.println(statement);


        //If there is only one statement for any clause within the if, brackets are not required.
//        System.out.println("\nWithout brackets (I don't like it!): -\n");

        if (value >= 3)
            statement = "equal to or greater than 3";
        else if (value > 4)
            statement = "this should be unreachable!";
            //This looks like the above example, but due to lack of bracketing, it is processed differently.
        else
            statement = "less than 3";

        int x = 3;
        //System.out.println(x);

        //System.out.println(statement);

//If statements must have an if clause, but if else and else are optional
        //All the following constructions are legal:
        if (value >= 3) {
            statement = "equal to or greater than 3";
        }


        if (value >= 3) {
            statement = "equal to or greater than 3";
        } else if (value > 4) {
            statement = "this should be unreachable!";
        }



        int another = 5;
        int four = 4;



        if (value >= 3  || another == 6 && four > 3) {
            statement = "equal to or greater than 3";
        } else {
            statement = "less than 3";
        }


        //If statements are often used as guards.
        //Note the risk of null pointers here:
        Animal myAnimal = null;
        //System.out.println(myAnimal.getName());

        //Defensive guarding helps avoid null pointers.

        if(myAnimal != null && myAnimal.getName() != null){
            System.out.println(myAnimal.getName());
        } else {
            System.out.println("The animal object myAnimal is null in this case!");

        }




        if (value >= 3) {
            statement = "equal to or greater than 3";
        } else if (value > 4) {
            statement = "this should be unreachable!";
        } else {
            statement = "less than 3";
        }

        if (value >= 3) {
            statement = "equal to or greater than 3";
        } else if (value == 1) {
            statement = "1";
        } else if (value == 2) {
            statement = "2";
        } else if (value == 3) {
            statement = "3";
        } else {
            statement = "less than 3";
        }


        //Nested branching is allowed in if statements.
        //But overly complex, hard to read, multiple branched
        //if statements probably indicate the need for
        //a more complex structure like a switch statement.
        int i = 1;
        if (value >= 3) {
            statement = "equal to or greater than 3";
        } else if (value == 1) {
            statement = "1";
        } else if (value == 2) {
            if (i >= 3) {
                //This also demonstrates that + acts to concatenate Strings including use of compound operator.
                statement += " i is equal to or greater than 3";
            } else if (i > 4) {
                statement += " this should be unreachable for i too!";
            } else {
                statement += " i is less than 3";
            }
        } else if (value == 3) {
            statement = "3";
        } else {
            statement = "less than 3";
        }


    }
}
