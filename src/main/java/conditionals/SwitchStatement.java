/**
 * Created by jhead on 07/02/2018.
 * <p>
 * Switch Statements
 * =============
 * <p>
 * Switch statements are more complex decision making structures.
 * <p>They are commonly used when if would become unnecessarily complex and overly-branched
 *
 * Switch is based on a variable or literal to test. This can be of type:
 *
 * int (and smaller primitives)
 * enum
 * String
 * Wrapper classes
 *
 */
package conditionals;

import java.util.Calendar;

public class SwitchStatement {

    static int size = 30;
    static int cost = 10;

    public static void main(String[] args) {

        final int testInt = 5;

        final int testAnotherInt;
        testAnotherInt = 6;
//        testAnotherInt = 7;
        //Simple switch using literals in case

        //testInt++;

        switch (size) {
//        switch (1000) { //Alternative: A literal can also be used in the switch, although there wouldn't be much point!
            //If there is only one statement for any clause within the switch, brackets are not required.
            case testInt:
                cost += 4;
                break;
            case 40:
                cost += 4;
                break;
            case 30:
                cost++;
                cost++;
                //Demonstrate that removing this break cause the default to be evaluated too.
                break;
            default:
                cost++;

        }

        System.out.println(cost);

        //cases must be constants (final);
        //Cases cannot be larger than an int
        //They can also be a String or enum type, but cases must be compatible with what's in the switch.
        final int large = 40;

        //value must be assigned at the same time it is declared.
        //Alternative 1 - FINE.
        final int medium = 30;

        //Alternative 2 - The following causes the switch statement not to compile.
        //int medium = 30;

        //Alternative 3 -The following also causes the switch statement not to compile.
        //final int medium;
        //medium = 30;

        size = 50;
        cost = 40;
        int sizeB = 300;
        //Simple switch using literals
        switch (sizeB) {
            //Default does not have to be the final clause.
            default:
                cost++;
                break;
            case large:
                cost += 4;
            case medium:
                cost++;
                cost++;
        }
       // System.out.println(cost);

        //Switch statement using Strings.
        //A similar example using Enums is shown in another example class.
        Calendar today = Calendar.getInstance();
        int dayOfWeek = today.get(Calendar.DAY_OF_WEEK);
        String dayString;

        switch(dayOfWeek){
            case Calendar.MONDAY:
                dayString = "It's Monday!";
                break;
            case Calendar.TUESDAY:
                dayString = "It's Tuesday!";
                break;
            case Calendar.WEDNESDAY:
                dayString = "It's Wednesday!";
                break;
            case Calendar.THURSDAY:
                dayString = "It's Thursday!";
                break;
            case Calendar.FRIDAY:
                dayString = "Hurray! It's Friday!";
                break;
            case Calendar.SATURDAY:
                dayString = "Great. It's Saturday!!";
                break;
            case Calendar.SUNDAY:
                dayString = "It's Sunday!";
                break;
            default:
                dayString = "Wow! What Calendar are you using?";
                break;
        }

        //System.out.println(dayString);

        //The switch statement can also be used with Enums
        //Homework, re-create the above switch statement, using the Enum java.time.DayOfWeek.
        //Hint, the case statement for Monday should start like:
        // case MONDAY:
        //Where MONDAY is from the Enum java.time.DayOfWeek.
        //This will require you to look at the API documentation of java.time.DayOfWeek and java.util.Calendar and combine logic from both of these.

    }
}
