package staticmethods;

/**
 * Koala - used to demonstrate static members
 * <p>
 * Created by rc00028 on 16/06/2017.
 * <p>
 * Uses of static methods: -
 * <p>
 * 1. To provide an entry point into a program without requiring an instance of the class (main method).
 * <p>
 * 2. For utility or helper methods that don’t require any object state.
 * Since there is no need to access instance variables,
 * having static methods eliminates the need for the caller
 * to instantiate the object just to call the method.
 * See also com.jon.javastudy.zoo.data.AnimalListDataManager
 * <p>
 * 3. For state that is shared by all instances of a class, like a counter on websites.
 * All instances must share the same state.
 * Methods that merely use that state should be static as well.
 * See also com.jon.javastudy.zoo.Animal
 * The variable refNoSequence does this.
 */
public class StaticTester {

    //This variable can be changed as it is not final.
    public static int count = 12121212; // static variable

    public static void main(String[] args) {      // static method
        System.out.println(count);
    }
}
