package staticmethods;


import zoo.data.AnimalListDataManager;

/**
 * Created by rc00028 on 16/06/2017.
 */
public class StaticTesterClient {

    //Static methods can be called without an instance. Even the main method can be called like this.
    public static void main(String[] args) {
        StaticTester.main(new String[0]);

        //JH also do with passing args
        StaticTester.main(args);

        //static members can be called through instances, although this is rarely best practice.
        StaticTester tester = new StaticTester();
        System.out.println(tester.count);

        //Best practice is to call it statically.
        System.out.println(StaticTester.count);

        tester.count = 8;
        tester = null;

        //What is the result of uncommenting this?
        System.out.println(tester.count);
        tester.count=1;

        //JH - AnimalListDataManager manager;
        AnimalListDataManager.getAnimalList();
        //JH - demo singleton

        //This demonstrates that the class and all instances point to the same variable.
        StaticTester.count = 4;

        tester.count = 1982773673;

        StaticTester tester1 = new StaticTester();
        tester1.count = 6;

        StaticTester tester2 = new StaticTester();
        tester2.count = 5;

//        System.out.println(StaticTester.count);
  //      System.out.println(tester.count);
    //    System.out.println(tester1.count);
      //  System.out.println(tester2.count);

    }
}
