package staticmethods;

/**
 *
 * Gorilla
 *
 * Created by rc00028 on 16/06/2017.
 */
public class Gorilla {


    public static int count;

    public static void addGorilla() {
        count++;
    }

    public void babyGorilla() {
        count++;
    }

    public void announceBabies() {
        addGorilla();
        babyGorilla();
    }

    public static void announceBabiesToEveryone() {
        addGorilla();
        //babyGorilla(); // DOES NOT COMPILE

        Gorilla gorilla = new Gorilla();
        gorilla.babyGorilla();

        new Gorilla().babyGorilla();
    }

    public int total;

    //There is a misprint in the book, the word int is omitted.
    //public static int average = total /count;

}
