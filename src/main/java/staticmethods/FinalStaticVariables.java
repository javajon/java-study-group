package staticmethods;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by rc00028 on 16/06/2017.
 */
public class FinalStaticVariables {


    public static final String ONE_AS_STRING = "one";
    public static final int ONE_AS_INT = 1;
    public static final LocalDate START_DATE_OF_FRENCH_REVOLUTION = LocalDate.of(1789, 5, 5);
    private static final ArrayList<String> STRING_ARRAY_LIST = new ArrayList<>();

    public static void main(String[] args) {

        //Referencing constants in the same class.
        System.out.println(ONE_AS_STRING);
        System.out.println(ONE_AS_INT);
        System.out.println(START_DATE_OF_FRENCH_REVOLUTION);

        //Referencing constants in another class.
        System.out.println(Calendar.MONDAY);

        //Changing the value is not allowed in either constants in the same or another class.
        //ONE_AS_STRING = "ONE";
        //TestConstants.TFC = "ABC";
        //START_DATE_OF_FRENCH_REVOLUTION = LocalDate.of(2017, 6, 18);

        //For constant objects, methods can be called on them to change their values.
        STRING_ARRAY_LIST.add("changed");

        //But the reference can't be changed to point to another object.
        //Does not compile.
        //STRING_ARRAY_LIST = new ArrayList<String>();

    }
}
