package staticmethods;

//import static java.util.Arrays; // DOES NOT COMPILE. Remember that static imports are only for importing static members.

import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;
import static staticmethods.B.TYPE;

//import static java.util.Arrays.asList;
//static import java.util.Arrays.*;  // DOES NOT COMPILE. Wrong order of keywords.
//import java.util.Arrays;
//import static com.jon.javastudy.staticmethods.B.TYPE;  //To get round this, use the full package path in the body of the method.

/**
 * Created by rc00028 on 22/06/2017.
 */
public class StaticImportTester {


    // public class BadStaticImports { 5:
    //
    public static void main(String[] args) {
        asList("one", "two");  // WORKS FINE BECAUSE OF LINE 5.
//        sort



        List myList = Arrays.asList("one");  // DOES NOT COMPILE UNLESS WE IMPORT java.util.Arrays

        //The two lines below get round the static import clash commented above.
        System.out.println(TYPE);
        System.out.println(staticmethods.B.TYPE);
    }
}
