package staticmethods;

/**
 *
 * StaticAndInstanceTester
 *
 * Created by rc00028 on 16/06/2017.
 *
 * Rules: -
 *
 * 1. Instance variables and methods cannot be referenced directly from a static context. To do this, one must first create an instance with which to reference them.
 * 2. Instance variables and methods can be referenced directly from instance methods.
 * 3. Static variables and methods can be referenced directly from a static context.
 * 4. Static variables and methods can be referenced directly from instance methods.
 *
 */
public class StaticAndInstanceTester {

    private static String staticString = "Static String";
    private String instanceString = "Instance String";

    public static void staticMethod() {
        staticString = "";
 //       instanceString = "";

        staticMethod();
   //     instanceMethod();

        StaticAndInstanceTester tester = new StaticAndInstanceTester();
        tester.instanceMethod();
        tester.instanceString="Helloworld";
    }

    public void instanceMethod() {
        staticMethod();
        System.out.println(staticString);

        instanceMethod();
        System.out.println(instanceString);
    }

    public static void main(String args[]) {
        //JH - discuss the two ways to get round instance access within a static method and the secondary problem with one of the solutions.
    }
}
