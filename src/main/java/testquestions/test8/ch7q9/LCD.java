package testquestions.test8.ch7q9;

/**
 * Created by rc00028 on 11/07/2017.
 *
 *  ch7, q9, answer C
 *
 */
class Television {
    protected final void watch(){}
}

//How many changes are required to make the following class compile.
/*
public class LCD extends Television{
    Object watch(){}
}
*/
