package testquestions.test8.q43;


import zoo.animal.Animal;

/**
 * Created by rc00028 on 11/07/2017.
 *
 *     //ch6, q43, answer A
 *
 */
public class ConstructorTest {

    public ConstructorTest() {
        //implicit call to super().
        //OK to explicitly put in if we want: -
        //super();
    }
}

class ConstructorAnimalTest extends Animal {

    String javaSkills = "expert";

    public ConstructorAnimalTest() {
        //implicit call to super() fails because superclass does not have a no args constructor.
        //We can pass arguments to a parameterised superconstructor instead.
        //Therefore the first line in a constructor is not always super(). But it has to be some kind of call to super or this.
        super("Vijaya", "vixen");
    }

    //What happens if we uncomment this?
    /*
    public ConstructorAnimalTest(String javaSkills) {
        //this();
        this.javaSkills = javaSkills;

    }
    */

    @Override
    public void eat() {
        System.out.println("I eat very healthy foods (and pineapple cake!)");
    }
}

class ImplicitConstructor {
    //You can't see it, but I have a no args constructor.......and a pet horse named Dave - OK, I lied about the horse! ;)
}
