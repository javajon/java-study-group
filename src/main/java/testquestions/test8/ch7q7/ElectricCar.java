package testquestions.test8.ch7q7;

/**
 * Created by rc00028 on 11/07/2017.
 *
 *  ch7, q7, answer B
 */
class Automobile {
    private final String drive(){
        return "Driving vehicle";
    }
}
class Car extends Automobile {
    protected String drive(){
        return "Driving car";
    }
}
public class ElectricCar extends Car{
    public final String drive(){
        return "Driving electric car";
    }

    public static void main(String[] args) {
        final Car car = new ElectricCar();
        System.out.println(car.drive());
    }
}
