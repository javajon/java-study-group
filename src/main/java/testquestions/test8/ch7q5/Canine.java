package testquestions.test8.ch7q5;

/**
 * Created by rc00028 on 11/07/2017.
 *
 *  ch7, q5, answer A
 */
interface Pet {

}

public class Canine implements Pet {

    //Which of these is illegal?
    /*
    public Class getDoggy() {
        return this;
    }
    */
    /*
    public Pet getDoggy() {
        return this;
    }
    */
    /*
    public Canine getDoggy() {
        return this;
    }
    */
    /*
    public Object getDoggy() {
        return this;
    }
    */

}

