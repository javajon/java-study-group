package testquestions.test8.ch7q12;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by rc00028 on 11/07/2017.
 */
class School {
    public int getNumberOfStudentsPerClassroom(String... students) throws IOException {
        return 3;
    }
    public int getNumberOfStudentsPerClassroom() throws IOException {
        return 9;
    }
}
public class HighSchool extends School{

    @Override
    public int getNumberOfStudentsPerClassroom() throws FileNotFoundException {
        return 2;
    }

    public static void main(String[] students) throws IOException {
        School school = new HighSchool();
        System.out.println(school.getNumberOfStudentsPerClassroom());
        //Extra - what about this one?
        System.out.println(school.getNumberOfStudentsPerClassroom("three"));

        //Extra - what about these?
        School school2 = new School();
        System.out.println(school2.getNumberOfStudentsPerClassroom());
        System.out.println(school2.getNumberOfStudentsPerClassroom("three"));
    }

    //Extra - What happens if we uncomment this?
    /*
    public int getNumberOfStudentsPerClassroom() throws Exception {
        return 2;
    }
    */


}
