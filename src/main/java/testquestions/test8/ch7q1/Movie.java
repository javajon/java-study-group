package testquestions.test8.ch7q1;

/**
 * Created by rc00028 on 11/07/2017.
 *
 *  ch7, q1, answer C
 */
class Cinema {
    private String name;

    public Cinema(String name) {
        this.name = name;
    }
}

/*
public class Movie extends Cinema{
    //How many compilation errors are here?
    public Movie(String name) {}

    public static void main(String[] args) {
        System.out.println(new Movie("Another Trilogy").name);
    }
}
*/
