package testquestions.test8.ch7q11;

/**
 * Created by rc00028 on 11/07/2017.
 *
 *  ch7, q11, answer C
 *
 */
class Computer {
    protected final int process(){
        return 5;
    }
}

//What happens when we uncomment this?
//What if we remove the extends clause?
/*
public class Laptop extends Computer {
    public final int process(){
        return 3;
    }

    public static void main(String[] args) {
        System.out.println(new Laptop().process());
    }
}
*/
