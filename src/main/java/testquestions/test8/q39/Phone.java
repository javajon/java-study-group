package testquestions.test8.q39;

/**
 * Created by rc00028 on 04/07/2017.
 *     //ch6, q39, answer A
 *
 */
public class Phone {

    private int size;

    public Phone(int size) {
        this.size = size;
    }

    public static void sendHome(Phone p, int newSize) {

        //Creating a new object points p to a different object than phone in main method.
        p = new Phone(newSize);
        p.size = 4;
    }

    public static void main(String[] args) {

        final Phone phone = new Phone(3);
        sendHome(phone, 7);
        System.out.println(phone.size);
    }
}
