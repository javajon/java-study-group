package testquestions.test8.q42;

/**
 * Created by rc00028 on 04/07/2017.
 *     //ch6, q42, answer D
 *
 */
public class StaticVariables {

    //OK not to set - will get default value.
    public static final int ONE_THOUSAND = 1000;
    public static int age;
    private static int privateAge = 21;

}
