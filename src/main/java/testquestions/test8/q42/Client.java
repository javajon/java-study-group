package testquestions.test8.q42;

import static testquestions.test8.q42.StaticVariables.ONE_THOUSAND;
import static testquestions.test8.q42.StaticVariables.age;

/**
 * Created by rc00028 on 11/07/2017.
 */
public class Client {
    public static void main(String[] args) {

        //Accessing static variables outside the class
        int localAge = age;
        int anotherTime = age;
        int aGrand = ONE_THOUSAND;

        StaticVariables s1 = new StaticVariables();

        localAge = age;

        //Cannot access private static variables from instances of the class declared in another class.
        //int myAge = s1.privateAge;
    }
}
