package testquestions.test8.q44;

/**
 * Created by rc00028 on 11/07/2017.
 *
 * ch6, q44, answer D
 *
 */
public class Tree {
    public static long numberOfTrees;
    public final double height;

    static{}

    {
        final int initHeight = 2;

        //Extra - what would happen if we commented this?
        height = initHeight;
    }

    static{
        numberOfTrees = 100;

        //Uncomment to start question
        //height = 4;
    }
}
