package testquestions.test8.q40;

/**
 * Created by rc00028 on 04/07/2017.
 *     //ch6, q40, answer B
 *
 */
public class Drink {
    public static void water() {}

    public void get() {
        //fine
        water();

        //Below is invalid syntax combining this with the classname is meaningless to the compiler.
        //this.Drink.water();

        //works fine, but it is considered bad practice to call a static method in a non-static way.
        this.water();

        //fine
        Drink.water();

    }
}
