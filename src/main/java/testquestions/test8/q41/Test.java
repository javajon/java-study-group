package testquestions.test8.q41;

/**
 * Created by rc00028 on 04/07/2017.
 *     //ch6, q41, answer C
 *
 */
public class Test {
    public void call(int count, String me, String... data) {}

    public void get() {
        //call(9, "me", 10, "Al"); //DOES NOT COMPILE
        //call(5); //DOES NOT COMPILE
        call(29, "home", "sweet"); //fine
        //call("answering", "service"); //DOES NOT COMPILE

        //Extra options to test knowledge: -
        //call(null, null, null);
        //call(1, null);
        //call(1, null, null);
        //call(1, null, null, "one", "two", null, null, "null", "three");
        //call(1, null, 1);

    }
}
