package testquestions.test8.ch7q2;

/**
 * Created by rc00028 on 11/07/2017.
 *
 *  ch7, q2, answer D
 */
public interface InterfaceMethodTester {
    //protected void protectedChildMethod(); // protected not allowed
    //public static void publicStaticMethod(); // static methods must have a body. JH to fix.
    //public final void publicFinalMethod(); // final not allowed
    public void publicMethod(); // public allowed

    //Extra
    //private void privateMethod(); // private not allowed

}
