package testquestions;

import java.util.Arrays;

/**
 * Created by rc00028 on 04/07/2017.
 */
public class Test7 {
    static int[][] game;

    public static void main(String[] args) {

        // ch4, q40, answer B
        Arrays.sort(args);
        // This will print the contents of the array starting with [ and ending with ].
        //System.out.println(Arrays.toString(args));

        // ch4, q41, answer D
        String[] os = new String[]{"Mac", "Linux", "Windows"};
        // When using Arrays.binarySearch, the array has to be sorted
        // to give meaningful output. Cannot be certain of output here.
        //System.out.println(Arrays.binarySearch(os, "Linux"));

        // ch4, q42, answer B
        /*
        game[3][3] = 6;
        Object[] obj = game;
        // The following line does not compile. Array is of type int, we are attempting to add a String.
        game[3][3]="X";
        System.out.println(game[3][3]);
        */

        // ch4, q43, answer A
        String[][] listing = new String[][]{{"Book"}, {"Game", "29.99"}};
        //System.out.println(listing.length + " " + listing[0].length);

        // ch4, q44, answer C
        //System.out.println(args[0]);

        // ch4, q45, answer A
        String[] days = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        for (int i = 1; i < days.length; i++) {
        //    System.out.println(days[i]);
        }

        // ch4, q46, answer B
        //Set command line params to "1 2"
        //System.out.println(args.length);

        // ch4, q47, answer A
        String[] os1 = new String[]{"Linux", "Mac", "Windows"};
        // When using Arrays.binarySearch, the array has to be sorted
        // to give meaningful output.
        // But here it is sorted on initialisation, so works predicatably.
        //System.out.println(Arrays.binarySearch(os1, "Linux"));

        // ch4, q48, answer A
        String[] testArray = {"test", "array", "example"};

        callArray(testArray);

        //The example below does not compile. In a method signature, an array param is more restrictive than a varargs parameter.
        //callArray("test", "varargs", "example");

        callVarargs(testArray);
        callVarargs("test", "varargs", "example");

        //ch4, q49, answer B
        int[][][][] nums1a, nums1b;
        int[][][] nums2a[], nums2b;
        int[][] nums3a[][], nums3b[][];
        int[] nums4a[][][], nums4b[][][];

        //ch4, q50, answer C
        Arrays.sort(args);
        //The line blow fails compilation because Arrays.binarySearch returns a representation of index as int. Here we attempt to assign to a String. 
        //String result = Arrays.binarySearch(args, args[0]);
        //System.out.println(result);

        //ch5, q39, answer C
        //Fails compilation because the while condition is not a boolean.
        /*
        StringBuilder builder = new StringBuilder();
        String str = new String("Leaves growing");
        do{
            System.out.println(str);
        } while(builder);
        System.out.println(builder);
        */

        //ch5, q40, answer A
/*
        int count = 0;
        do {
            do {
                count++;
            } while (count < 2);
            break;
        } while (true);
     //   System.out.println(count);
*/

        //ch5, q41, answer C
/*
        int count = 0;
        t: while(true){
            System.out.println("t" + ++count);
            f: while(true){
                System.out.println("f" + ++count);
//                break;
//               break f;//
 break t;
            }
        }
        */


        //ch5, q42, answer B
/*
        String[] nycTourLoops = new String[] {"Downtown", "Uptown", "Brooklyn"};
        String[] times = new String[] {"Day", "Night"};
        for(int i = 0, j = 0; i < nycTourLoops.length && j < times.length; i++, j++){
            System.out.print(nycTourLoops[i] + " " + times[j] + "-");
        }
        String checkpoint = "checkPoint";
*/

        //ch5, q43, answer B
/*
        List<String> exams = Arrays.asList("OCA", "OCP");
        for(String el : exams){
            for(String e2 : exams){
                System.out.println(el + " " + e2);
            }
        }
*/

        //ch5, q44, answer B

  //      for(alpha; beta; gamma){
    //        delta;
      //  }

        //If the condition is false immediately, the loop is never incremented
        /*
        for(int i = 0; i < 0; i++){
            System.out.println("This won't be printed!");;
        }
        String checkpoint = "checkPoint";
        */
    }

    public static void callArray(String[] arg) {

    }

    public static void callVarargs(String... arg) {

    }
}
