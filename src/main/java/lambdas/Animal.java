package lambdas;

/**
 *
 * Java is an object-oriented language at heart. You’ve seen plenty of objects by now.
 * In Java 8, the language added the ability to write code using another style.
 * Functional programming is a way of writing code more declaratively.
 * You specify what you want to do rather than dealing with the state of objects.
 * You focus more on expressions than loops. Functional programming uses lambda expressions to write code.
 * A lambda expression is a block of code that gets passed around. You can think of a lambda expression as an
 * anonymous method. It has parameters and a body just like full-fledged methods do,
 * but it doesn’t have a name like a real method.
 *
 * Lambda expressions are often referred to as lambdas for short.
 * You might also know them as closures if Java isn’t your first language.
 *
 * In other words, a lambda expression is like a method that you can pass as if it were a variable.
 * For example, there are different ways to calculate age. One human year is equivalent to seven dog years.
 * You want to write a method that that takes an age() method as input. To do this in an object-oriented program,
 * you’d need to define a Human subclass and a Dog subclass.
 *
 * With lambdas, you can just pass in the relevant expression to calculate age.
 * Lambdas allow you to write powerful code in Java. Only the simplest lambda expressions are on the OCA exam.
 * The goal is to get you comfortable with the syntax and the concepts.
 * This means you aren’t truly doing functional programming yet.
 * You’ll see lambdas again on the OCP exam.
 *
 * Created by rc00028 on 26/06/2017.

 */
public class Animal {
    private int weight;
    private String species;
    private boolean canHop;
    private boolean canSwim;
    private boolean canWalk;

    public Animal(String speciesName, boolean hopper, boolean swimmer, boolean walker) {
        species = speciesName;
        canHop = hopper;
        canSwim = swimmer;
        canWalk = walker;
    }

    public int getWeight() {
        return weight;
    }

    public String getSpecies() {
        return species;
    }

    public boolean canHop() {
        return canHop;
    }

    public boolean canSwim() {
        return canSwim;
    }

    public boolean canWalk() {
        return canWalk;
    }

    public String toString() {
        return species;
    }
}