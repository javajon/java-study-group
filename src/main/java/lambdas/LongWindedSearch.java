package lambdas;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rc00028 on 26/06/2017.
 */
public class LongWindedSearch {
    public static void main(String[] args) {
        List<Animal> animalList = new ArrayList<Animal>();  // list of animals
        animalList.add(new Animal("fish", false, true, false));
        animalList.add(new Animal("kangaroo", true, false, true));
        animalList.add(new Animal("wallaby", true, false, true));
        animalList.add(new Animal("rabbit", true, false, true));
        animalList.add(new Animal("shark", false, true, false));
        animalList.add(new Animal("frog", true, true, true));
        animalList.add(new Animal("turtle", false, true, true));
        animalList.add(new Animal("croc", false, true, true));
        //printUnorderedInvetory(animalList);
        printOrderedInvetory(animalList);
    }

    private static void printUnorderedInvetory(List<Animal> animalList) {
        for (Animal animal : animalList) {
            if (animal.canHop()) {
                System.out.println(animal.getSpecies() + " can hop");
            }
            if (animal.canSwim()) {
                System.out.println(animal.getSpecies() + " can swim");
            }
            if (animal.canWalk()) {
                System.out.println(animal.getSpecies() + " can walk");
            }
        }
    }

    private static void printOrderedInvetory(List<Animal> animalList) {
        for (Animal animal : animalList) {
            if (animal.canHop()) {
                System.out.println(animal.getSpecies() + " can hop");
            }
        }
        System.out.println();
        for (Animal animal : animalList) {
            if (animal.canSwim()) {
                System.out.println(animal.getSpecies() + " can swim");
            }
        }
        System.out.println();
        for (Animal animal : animalList) {
            if (animal.canWalk()) {
                System.out.println(animal.getSpecies() + " can walk");
            }
        }
        System.out.println();
    }
}