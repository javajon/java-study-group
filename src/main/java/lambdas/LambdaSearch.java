package lambdas;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rc00028 on 26/06/2017.
 *
 * Lambda Expressions
 *
 * The syntax of lambdas is tricky because many parts are optional.
 *
 * These two lines do the exact same thing:
 *
 * a -> a.canHop()
 *
 * (Animal a) -> { return a.canHop(); }
 *
 * The first example has three parts:
 * 1. Specify a single parameter with the name a
 * 2. The arrow operator to separate the parameter and body
 * 3. A body that calls a single method and returns the result of that method
 *
 * The second example also has three parts; it’s just more verbose
 * 1. Specify a single parameter with the name a and stating the type is Animal
 * 2. The arrow operator to separate the parameter and body
 * 3. A body that has one or more lines of code, including a semicolon and a return statement
 *
 * Rules
 * The parameter parentheses can only be omitted if there is a single parameter and its type is not explicitly stated.
 *
 * In the method body, we can omit braces when we only have a single statement. We did this with if statements and loops already.
 * What is different here is that the rules change when you omit the braces.
 * Java doesn’t require you to type return or use a semicolon when no braces are used.
 * This special shortcut doesn’t work when we have two or more statements.
 * At least this is consistent with using {} to create blocks of code elsewhere.
 *
 * Some examples of valid lambdas. Assume that there are valid interfaces that can consume a lambda with zero, one, or two String parameters.
 *
 * print(() -> true); // 0 parameters
 * print(a -> a.startsWith("test")); // 1 parameter
 * print((String a) -> a.startsWith("test")); 1 parameter
 * print((a, b) -> a.startsWith("test")); 2 parameters
 * print((String a, String b) -> a.startsWith("test")); // 2 parameters
 *
 * Lambda expressions with invalid syntax. Do you see what’s wrong with each of these?
 * print(a, b -> a.startsWith("test")); // DOES NOT COMPILE - needs parentheses around the parameter list
 * print(a -> { a.startsWith("test"); }); // DOES NOT COMPILE - missing the return keyword
 * print(a -> { return a.startsWith("test") }); // DOES NOT COMPILE - missing the semicolon
 *
 */
public class LambdaSearch {

    public static void main(String[] args) {
        List<Animal> animalList = new ArrayList<Animal>();  // list of animals
        animalList.add(new Animal("fish", false, true, false));
        animalList.add(new Animal("kangaroo", true, false, true));
        animalList.add(new Animal("wallaby", true, false, true));
        animalList.add(new Animal("rabbit", true, false, true));
        animalList.add(new Animal("shark", false, true, false));
        animalList.add(new Animal("frog", true, true, true));
        animalList.add(new Animal("turtle", false, true, true));
        animalList.add(new Animal("croc", false, true, true));

        // This is deferred execution. The canHop() method will be called when the print method is run.
        //The second parameter i.e.  a -> a.canHop() must be a method that comforms to the CheckTrait interface - as it is mapped at runtime.
        print(animalList, a -> a.canHop(), "hop");
//        print(animalList, new CheckIfHopper(), "hop");

        //This is a longer way of achieving the above.
        print(animalList, (Animal a) -> { return a.canHop(); }, "hop");

        //The lambda expression passed in must conform to the CheckTrait Interface
        //This doesn't compile because getWeight returns an int not a boolean.
        //print(animalList, a -> a.getWeight(), "hop");// DOES NOT COMPILE

        //This doesn't compile because print requires a List of Animals, not Strings.
        List<String> stringList = new ArrayList<>();
        //print(stringList, a -> a.canHop(), "hop");// DOES NOT COMPILE

        //and for animals that can swim: -
        print(animalList, a -> a.canSwim(), "swim");

        //and for animals that cannot swim: -
        print(animalList, a -> ! a.canSwim(), "not swim");

        //and for animals that can walk: -
        print(animalList, a -> a.canWalk(), "swim");

    }

    private static void print(List<Animal> animalList, CheckTrait checker, String trait) {
        for (Animal animal : animalList) {
            if (checker.checkProperty(animal)) {
                System.out.println(animal.getSpecies() + " can " + trait);
            }
        }
        System.out.println();
    }

}