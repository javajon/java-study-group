package lambdas;

/**
 * Created by rc00028 on 26/06/2017.
 */
public class CheckIfWalks implements CheckTrait {
    public boolean checkProperty(Animal a) {
        return a.canHop();
    }
}