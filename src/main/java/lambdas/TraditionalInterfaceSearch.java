package lambdas;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rc00028 on 26/06/2017.
 */
public class TraditionalInterfaceSearch {

    public static void main(String[] args) {
        List<Animal> animalList = new ArrayList<Animal>();  // list of animals
        animalList.add(new Animal("fish", false, true, false));
        animalList.add(new Animal("kangaroo", true, false, true));
        animalList.add(new Animal("wallaby", true, false, true));
        animalList.add(new Animal("rabbit", true, false, true));
        animalList.add(new Animal("shark", false, true, false));
        animalList.add(new Animal("frog", true, true, true));
        animalList.add(new Animal("turtle", false, true, true));
        animalList.add(new Animal("croc", false, true, true));
        print(animalList, new CheckIfHopper(), "hop");     // pass class that does check
        print(animalList, new CheckIfSwims(), "swim");      // pass class that does check
        print(animalList, new CheckIfWalks(), "walk");      // pass class that does check
    }

    private static void print(List<Animal> animalList, CheckTrait checker, String trait) {
        for (Animal animal : animalList) {
            if (checker.checkProperty(animal)) {
                System.out.println(animal.getSpecies() + " can " + trait);
            }
        }
        System.out.println();
    }
}