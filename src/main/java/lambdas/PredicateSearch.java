package lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by rc00028 on 11/07/2017.
 */
public class PredicateSearch {

    public static void main(String[] args) {
        List<Animal> animalList = new ArrayList<Animal>();  // list of animals
        animalList.add(new Animal("fish", false, true, false));
        animalList.add(new Animal("kangaroo", true, false, true));
        animalList.add(new Animal("wallaby", true, false, true));
        animalList.add(new Animal("rabbit", true, false, true));
        animalList.add(new Animal("shark", false, true, false));
        animalList.add(new Animal("frog", true, true, true));
        animalList.add(new Animal("turtle", false, true, true));
        animalList.add(new Animal("croc", false, true, true));
        print(animalList, a -> a.canHop(), "hop");        // pass impl for Predicate Interface
        print(animalList, (Animal a) -> {return ! a.canSwim();}, "not swim");      // pass impl for Predicate Interface
        print(animalList, a -> a.canSwim(), "swim");      // pass impl for Predicate Interface
        print(animalList, a -> a.canWalk(), "walk");      // pass impl for Predicate Interface

    }

    private static void print(List<Animal> animalList, Predicate<Animal> checker, String trait) {
        for (Animal animal : animalList) {
            if (checker.test(animal))
                System.out.println(animal.getSpecies() + " can " + trait);
        }
        System.out.println();
    }
}
