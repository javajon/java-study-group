package zoo;

/**
 * Created by rc00028 on 30/06/2017.
 */
public interface Dangerous {
    String howToHandle();
}
