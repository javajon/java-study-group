package zoo;

import zoo.animal.*;

import java.util.List;

/**
 * Created by u.7829872 on 16/11/2017.
 */
public class ZooFirstEffort {

    //static String staticString = "static String";
    String name;
    boolean hasAnimals;
    List<Animal> animals;

    public void hello(){

    }

    public static  void main(String[] args) {

        Animal laura = new Chick("Laura");
        Animal Laura = new Panther("Laura");

        //Not allowed - abstract
//        Animal lauraAsAnimal = new Animal("Laura");

        Animal giles = new Lion("Giles");

        //Here we are using the constructor to set the comment.
        Animal tiger = new Tiger(args[1]);

        //Try to set name directly. This can only be done if properties are not private.
        //tiger.name = "Bill";

        //Otherwise, properties can only be changed by publicly available methods, such as a setter.
        tiger.setName("Bill");

        Animal elephant = new Elephant("Elly");
        Animal ferret = new Zebra("Freddy");



        //Not allowed - abstract class
      //  Animal mouse = new Animal();
/*
        System.out.println("Number of animals in the zoo " + Animal.animalCounter);
        System.out.println("Number of animals in the zoo " + laura.animalCounter);

        Animal moose = new Animal();
        System.out.println("Number of animals in the zoo " + Animal.animalCounter);
        System.out.println("Number of animals in the zoo " + laura.animalCounter);
*/

        //Here we are using the setter to chsange the comment.
        elephant.setName("Evan");


        System.out.println("Welcome to " + args[0]);
     //   System.out.println("This zoo has a " + tiger.getType() + " called " + tiger.getName());
        System.out.println("This zoo has an " + elephant.getType() + " called " + elephant.getName());
        System.out.println("This zoo has a " + laura.getType() + " called " + laura.getName());
        System.out.println("This zoo has a " + tiger.getType() + " called " + tiger.getName());
        System.out.println("This zoo has a " + giles.getType() + " called " + giles.getName());

        System.out.println(laura.getName());

        //JH - demo polymorphism

        Lion lionAsLion = new Lion("Leo");
        lionAsLion.getName();
        lionAsLion.roar();

        Animal lionAsAnimal = new Lion("Lenny");
        lionAsAnimal.getName();
        ///lionAsAnimal.roar();

        //Ultimately, all Java classes extend from java.lang.Object.
        //JH Show lion can be an Object as object
        Object lionAsObject = new Lion("Luther");
       // lionAsObject.


//JH - show how changing the value of a static variable, changes it in every instance of the class.
/*
        elephant.setName("Ethel");
        System.out.println("This zoo has an " + elephant.getType() + " called " + elephant.getName());
        System.out.println("This zoo has a " + laura.getType() + " called " + laura.getName());
        System.out.println("This zoo has a " + tiger.getType() + " called " + tiger.getName());
        System.out.println("This zoo has a " + giles.getType() + " called " + giles.getName());
*/



    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public boolean hasAnimals() {
        return hasAnimals;
    }

    public void setHasAnimals(boolean hasAnimals) {
        this.hasAnimals = hasAnimals;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(List<Animal> animals) {
        this.animals = animals;
    }
}
