package zoo;

import zoo.animal.Animal;

import java.util.List;

/**
 * Created by u.7829872 on 16/11/2017.
 */
public class Zoo {

    //Fields
    private String name;
    private List<Animal> animals;

    //Constructors
    public Zoo(String name) {
        this.name = name;
    }

    public Zoo(String name, List<Animal> animals) {
        this.name = name;
        this.animals = animals;
    }

    //Getter/Setter methods
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(List<Animal> animals) {
        this.animals = animals;
    }

    //Other methods
    public void printInventory() {
        System.out.println("\nWelcome to " + name + "\n");

        //Here we can either use isEmpty() or size() in our guard condition.
        if (animals != null && !animals.isEmpty()) {
        //if (animals != null && animals.size() > 0) {
            System.out.println("These are our animals:\n");
            for (Animal animal : animals) {
                System.out.println(animal.getName() + " " + animal.getType());
            }
            System.out.println();
        } else {
            System.out.println("We don't yet have any animals. Please try again soon!");
        }

    }
}
