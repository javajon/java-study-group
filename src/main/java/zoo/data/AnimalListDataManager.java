package zoo.data;

import zoo.animal.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * AnimalListDataManager
 *
 * Created by jon on 05/06/2017.
 *
 * This class maintains Animal test data.
 *
 * It holds a List of Animals in an ArrayList and a Map of Animals in a HashMap.
 *
 * The aim of this class is to decouple test data dependency from the classes that use that data.
 *
 */
public class AnimalListDataManager {

    static List<Animal> animalList = new ArrayList<>();
    //static List<Animal> animalList = new JonsList<>();

    static Map<String, Animal> animalMap = new HashMap<>();

    static {
        //Create Animals
        Animal tiger = new Tiger("Terry");
        Animal tiger2 = new Tiger("Jemima");
        Animal elephant = new Elephant("Erica");
        Animal chimpanzee = new Chimpanzee("Charlie");
        Animal lion = new Lion("Larry");
        Animal antelope = new Antelope("Amit");
        Animal panther = new Panther("Priya");
        Animal rabbit = new Rabbit("Ramesh");

        //Add Animals to List
        animalList.add(tiger);
        animalList.add(tiger2);
        animalList.add(elephant);
        animalList.add(chimpanzee);
        animalList.add(lion);

        //This entry shows the other overloaded add method.
        animalList.add(3, antelope);
        animalList.add(panther);
        animalList.add(rabbit);

        //Showing how to populate a Map - not on OCA exam, but for reference.
        animalMap.put("terry", tiger);
        animalMap.put("jemima", tiger2);
        animalMap.put("charlie", chimpanzee);
        animalMap.put("larry", lion);
        animalMap.put("amit", antelope);
        animalMap.put("priya", panther);
        animalMap.put("ramesh", rabbit);
    }

    private AnimalListDataManager() {
    }

    public static List<Animal> getAnimalList() {
        return animalList;
    }

    public static Map<String, Animal> getAnimalMap() {
        return animalMap;
    }

    public static Animal getAnimal(String type) {
        Animal returnAnimal = null;
        searchType:
        for (Animal animal : animalList) {
            if (animal.getType().equalsIgnoreCase(type)) {
                returnAnimal = animal;
                break searchType;
            }
        }
        return returnAnimal;
    }

    public static Animal getAnimalFromMap(String name) {
        Animal animal = null;
        if (animalMap != null && animalMap.containsKey(name)) {
            animal = animalMap.get(name);
        }

        return animal;
    }
}