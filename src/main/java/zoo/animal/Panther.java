package zoo.animal;

/**
 * Created by u.7829872 on 05/12/2017.
 */
public class Panther extends Animal {

    public Panther(String name) {
        super(name, "Panther");
    }
    @Override
    public void eat() {}

}
