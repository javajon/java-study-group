
package zoo.animal;

import org.apache.log4j.Logger;
import staticFields.StaticAndInstanceVariableTester;

/**
 *
 * Zebra
 *
 * Created by jon on 05/12/2017.
 *
 * This is an animal in the zoo.animal package.
 *
 * NOTE: This differs from other animals in th same package in that it has extra methods.
 * These methods are designed to be used with StaticAndInstanceVariableTester.java.
 * They aim to demonstrate the difference between static and instance members.
 *
 *
 */
public class Zebra extends Animal {
    final static Logger logger = Logger.getLogger(Zebra.class);
    private static int count;

    public Zebra(String name) {
        super(name, "Zebra");
        count++;
    }

    public static void main(String[] args) {
        logger.info("This is the public static main (String[] args) method being called from the Zebra class.");
    }

    public static int getCount() {
        return count;
    }

    public static void printCount() {
        logger.info(String.format("%s zebra instances have been created", count));
    }

    public static void setCount(int count) {
        Zebra.count = count;
    }

    public void printMyName(){
        //System.out.println(String.format("My name is %s", name)); //DOES NOT COMPILE - name is private in animal.
        //JH - also demonstrate String.format method.
        logger.info(String.format("My name is %s", this.getName()));
    }

    //Doesn't compile - we cannot access an instance variable from a static context.
    /*
    public static void printMyName(){
        logger.info(String.format("My name is %s", name));
    }
    */

    @Override
    public void eat() {}

}
