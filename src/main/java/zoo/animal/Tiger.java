package zoo.animal;

import zoo.Dangerous;

/**
 * Created by u.7829872 on 05/12/2017.
 */
public class Tiger extends Animal implements Dangerous {

    public Tiger(String name) {
        super(name, "Tiger");
    }

    public void purr() {
        System.out.println("PPPPPUUUUUUUUURRRR!");
    }

    @Override
    public String howToHandle() {
        return null;
    }

    @Override
    public void eat() {

    }

    public void tigerMethod(){};

}
