package zoo.animal;

/**
 * Created by u.7829872 on 05/12/2017.
 */
public class Chimpanzee extends Animal {

    public Chimpanzee(String name) {
        super(name, "Chimpanzee");
    }
    @Override
    public void eat() {}

}
