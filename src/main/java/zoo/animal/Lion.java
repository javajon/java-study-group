package zoo.animal;

/**
 * Created by u.7829872 on 05/12/2017.
 */
public class Lion extends Animal {
    public Lion(String name) {
        super(name, "Lion");
    }

    @Override
    public void eat() {}

    public void roar() {
        System.out.println("RROOOAARRRR!");
    }
}
