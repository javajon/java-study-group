package zoo.animal;

/**
 * Created by u.7829872 on 05/12/2017.
 */
public class Rabbit extends Animal {

    public Rabbit(String name) {
        super(name, "Rabbit");
    }
    @Override
    public void eat() {}

}
