package zoo.animal;

/**
 * Created by u.7829872 on 05/12/2017.
 */
public class Chick extends Animal {
    public Chick(String name) {
        super(name, "Chick");
    }

    @Override
    public void eat() {

    }
}
