package zoo.animal;

/**
 * Created by u.7829872 on 05/12/2017.
 */
public class Antelope extends Animal {

    public Antelope(String name) {
        super(name, "Antelope");
    }

    @Override
    public void eat() {}

}
