package zoo.animal;

/**
 * Created by u.7829872 on 05/12/2017.
 */
public class Elephant extends Animal {

    public Elephant(String name) {
        super(name, "Elephant");
    }

    @Override
    public void eat() {}

}
