package zoo.animal;

import org.apache.log4j.Logger;

/**
 *
 * Duck
 *
 * Created by jon on 27/06/2018.
 *
 * This is an animal in the zoo.animal package.
 *
 * This is a special case with extra methods that are used to demonstrate
 * inheritance and access to member.
 *
 *
 */
public class Duck extends Animal {

    final static Logger logger = Logger.getLogger(Duck.class);

    public Duck(String name) {
        super(name, "Duck");
    }

    @Override
    public void eat() {
        logger.debug(getName() + " is eating");
    }

    public void feed(Duck... ducklings){

        //Mother feeds herself first
        eat();
        //this has an implicit "this" reference
        //this.eat();

        //Mother then feeds her ducklings by calling the method against an object reference.
        for(Duck duckling: ducklings){
            duckling.eat();
        }

        //In the same way, methods inherted fromt he parent can be called.


    }

    //Calling a methods inherited from the parent.
    public void logName(Duck... ducklings){

        //Mother logs her own name first
        logger.debug(getName());
        //again, this has an implicit "this" reference
        //logger.debug(this.getName());

        //Mother then logs her ducklings names.
        for(Duck duckling: ducklings){
            logger.debug(duckling.getName());
        }



    }

    //Calling a method inherited from java.lang.Object.
    public void checkForDuck(Duck mysteryDuck, Duck... ducklings){

        //Mother first checks if she is the mystery duck.
        if(equals(mysteryDuck)) {
            logger.debug(String.format("%s is the mystery duck", getName()));
        } else {
            logger.debug(String.format("%s is NOT the mystery duck", getName()));
        }
        //again, this has an implicit "this" reference
        /*
        if(equals(mysteryDuck)) {
            logger.debug(String.format("%s is the mystery duck", this.getName()));
        }
        */

        //Mother first checks if any of the ducklings is the mystery duck.
        for(Duck duckling: ducklings){
            if(duckling.equals(mysteryDuck)) {
                logger.debug(String.format("%s is the mystery duck", duckling.getName()));
            } else {
                logger.debug(String.format("%s is NOT the mystery duck", duckling.getName()));
            }
        }

    }

    public static void main(String[] args) {
        Duck mother = new Duck("Daisy");

        Duck duckling1 = new Duck("Simon");
        Duck duckling2 = new Duck("Shirley");
        Duck duckling3 = new Duck("Sue");

        logger.debug("Calling feed() method:");
        mother.feed(duckling1, duckling2, duckling3);

        logger.debug("\nCalling logName() method:");
        mother.logName(duckling1, duckling2, duckling3);

        //Duck mysteryDuck = new Duck("Dave");

        //In the line below, mysteryDuck is set to point to
        //the same object that duckling2 is pointing at.
        Duck mysteryDuck = duckling2;

        logger.debug("\nCalling logName() method:");
        mother.checkForDuck(mysteryDuck, duckling1, duckling2, duckling3);
    }

}
