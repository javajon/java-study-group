package zoo.animal;

/**
 * Animal class
 *
 * Created by Jon on 16/11/2017.
 */
public abstract class Animal {

    //Class (or static) properties.
    static int animalCounter = 0;

    //Instance Properties
    //public static String name;
    private String name;
    private String type;

    //Constructors
    public Animal() {
        animalCounter++;
    }

    public Animal(String name, String type) {
        this.name = name;
        this.type = type;
        animalCounter++;
    }


    //Methods
    public String getName() {
        //System.out.println("Returning name");
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    //JH - override this
    public abstract void eat();

    public void animalMethod(){};
}
