package zoo.animal;

/**
 * Created by u.7829872 on 05/12/2017.
 */
public class Snake extends Animal {

    public Snake(String name) {
        super(name, "Snake");
    }
    @Override
    public void eat() {}

}
