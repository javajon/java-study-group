package zoo;

import org.apache.log4j.Logger;
import zoo.animal.Animal;
import zoo.animal.Lion;
import zoo.data.AnimalListDataManager;

import java.util.List;


public class ZooTester {

    final static Logger logger = Logger.getLogger(ZooTester.class);

    public static  void main(String[] args) {

        Zoo zoo = new Zoo("Jon's Zoo");

        logger.debug("Testing our newly refactored zoo");
        logger.info("Before getting animals");
        zoo.printInventory();

        List<Animal> testAnimals = AnimalListDataManager.getAnimalList();
        zoo.setAnimals(testAnimals);

        //or use the other constructor: -
        zoo = new Zoo("Jon's Zoo", AnimalListDataManager.getAnimalList());

        logger.info("After adding animals");
        zoo.printInventory();

        //A new Lion can be added to the data manager.
        Lion newLion = new Lion("Rex");
        AnimalListDataManager.getAnimalList().add(newLion);
        zoo.setAnimals(AnimalListDataManager.getAnimalList());

        logger.info("After another animal");
        zoo.printInventory();
    }

}
