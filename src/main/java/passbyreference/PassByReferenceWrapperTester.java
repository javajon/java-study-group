package passbyreference;

/**
 * Created by rc00028 on 23/06/2017.
 */
public class PassByReferenceWrapperTester {

    public static void main(String[] args) {
        Integer num = 4;
        System.out.println("main, value of num before: " + num);
        changeWrapper(num);
        System.out.println("main, value of num after changeWrapper(): " + num);
        newWrapper(num);
        System.out.println("main, value of num after newWrapper(): " + num);
    }

    public static void changeWrapper(Integer num) {
        System.out.println("changeWrapper, value of num before: " + num);
        System.out.println(num+= 8);
        System.out.println("changeWrapper, value of num after: " + num);
    }

    public static void newWrapper(Integer num) {
        System.out.println("newWrapper, value of num before: " + num);
        num = 8;
        System.out.println("newWrapper, value of num after: " + num);
    }
}
