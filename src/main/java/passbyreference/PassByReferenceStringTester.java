package passbyreference;

/**
 * Created by rc00028 on 23/06/2017.
 */
public class PassByReferenceStringTester {

    public static void main(String[] args) {
        String num = "4";
        System.out.println("main, value of num before: " + num);
        changeString(num);
        System.out.println("main, value of num after changeString(): " + num);
        newString(num);
        System.out.println("main, value of num after newString(): " + num);
    }

    public static String changeString(String num) {
        System.out.println("changeString, value of num before: " + num);
        System.out.println(num.concat("8"));
        System.out.println("changeString, value of num after: " + num);
        return num;
    }

    public static String newString(String num) {
        System.out.println("newString, value of num before: " + num);
        num = "8";
        System.out.println("newString, value of num after: " + num);
        return num;
    }
}
