package passbyreference;


import zoo.animal.Animal;
import zoo.animal.Elephant;
import zoo.animal.Tiger;

/**
 * Created by rc00028 on 23/06/2017.
 */
public class PassByReferenceTester {

    public static void main(String[] args) {
        Animal animal1 = new Tiger("Terry");
        System.out.println("main, value of animal.getName() before: " + animal1.getName());
        changeAnimal(animal1);
        System.out.println("main, value of animal.getName() after changeAnimal(): " + animal1.getName());
        newAnimal(animal1);
        System.out.println("main, value of animal.getName() after newAnimal(): " + animal1.getName());
    }

    public static void changeAnimal(Animal animal) {
        System.out.println("changeAnimal, value of animal.getName() before: " + animal.getName());
        animal.setName("Charlie");
        System.out.println("changeAnimal, value of animal.getName() after: " + animal.getName());
    }

    public static void newAnimal(Animal animal) {
        System.out.println("newAnimal, value of animal.getName() before: " + animal.getName());
        animal = new Elephant("Elrica");
        //animal = new Rabbit("Roger", Gender.MALE);
        System.out.println("newAnimal, value of animal.getName() after: " + animal.getName());
    }
}
