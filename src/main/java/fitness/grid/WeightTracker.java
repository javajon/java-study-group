package fitness.grid;

import org.jdesktop.swingx.JXDatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class WeightTracker extends JFrame implements ActionListener {

    private final int hGap = 10;
    private final int vGap = 10;
    double amount = 0.0;

    JPanel p = new JPanel(new GridLayout(5,1,hGap,vGap));
    JPanel prow = new JPanel(new GridLayout(1,2));

    JLabel message = new JLabel();
    Button addButton = new Button("Add");
    JXDatePicker datePicker = new JXDatePicker();
    NumberFormat amountFormat;
    JFormattedTextField weightTextField = new JFormattedTextField(amountFormat);

    //Formats to format and parse numbers



    public WeightTracker() {

        super("WeightTracker");
        this.setTitle("WeightTracker");

        p.setBorder(
                BorderFactory.createTitledBorder("Record Weight"));

        p.add(new JLabel());
        prow = new JPanel(new GridLayout(1,2));
        prow.add(new JLabel("Date: "));
        datePicker.setDate(Calendar.getInstance().getTime());
        datePicker.setFormats(new SimpleDateFormat("dd.MM.yyyy"));
        prow.add(datePicker);
        p.add(prow);

        prow = new JPanel(new GridLayout(1,2));
        prow.add(new JLabel("Weight (Kg): "));
        weightTextField.setValue(new Double(amount));
        weightTextField.setColumns(10);
        prow.add(weightTextField);
        p.add(prow);

        prow = new JPanel(new GridLayout(1,1));
        prow.add(addButton);
        p.add(prow);

        prow = new JPanel(new GridLayout(1,1));
        prow.add(message);
        p.add(prow);

        addButton.addActionListener(this);
        getContentPane().add(p);
        addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                }
        );
    }

    public void actionPerformed(ActionEvent AE) {
        if (AE.getSource() == addButton) {
            String content = new SimpleDateFormat("dd.MM.yyyy").format(datePicker.getDate()) + "|" + weightTextField.getText() + "\n";
            try {
                Files.write(Paths.get("/home/jon/fitness/weight.txt"), content.getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }

            message.setText("Entry added. Date: " + new SimpleDateFormat("dd.MM.yyyy").format(datePicker.getDate()) + ". Weight: " + weightTextField.getText());
            datePicker.setDate(Calendar.getInstance().getTime());
            weightTextField.setValue(new Double(amount));
        }
    }

    public static void main(String args[]) {
        WeightTracker tracker = new WeightTracker();
        tracker.setVisible(true);
        tracker.setSize(350, 160);
    }
}