package fitness.gridbag;

import org.jdesktop.swingx.JXDatePicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class WeightTracker extends JFrame implements ActionListener {

    private final int hGap = 10;
    private final int vGap = 10;

    JPanel p = new JPanel();
    Button addButton = new Button("Add");
    JXDatePicker datePicker = new JXDatePicker();
    TextField weightTextField = new TextField(40);
    GridBagLayout experimentLayout = new GridBagLayout();
    GridBagConstraints gbc = new GridBagConstraints();

    public WeightTracker() {

        super("WeightTracker");
        this.setTitle("WeightTracker");
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(hGap, vGap, hGap, vGap);

        p.setLayout(experimentLayout);

        p.add(new JLabel("Date: "), gbc);
        gbc.gridy++;
        p.add(new JLabel("Weight: "), gbc);

        gbc.gridx++;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;

        //datePicker.setSize(80,25);
        datePicker.setDate(Calendar.getInstance().getTime());
        datePicker.setFormats(new SimpleDateFormat("dd.MM.yyyy"));
        p.add(datePicker, gbc);
        gbc.gridy++;
        p.add(weightTextField, gbc);
        gbc.gridy++;
        p.add(addButton, gbc);
        addButton.addActionListener(this);
        getContentPane().add(p);
        addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                }
        );
    }

    public void actionPerformed(ActionEvent AE) {
        if (AE.getSource() == addButton) {
            String content = new SimpleDateFormat("dd.MM.yyyy").format(datePicker.getDate()) + "|" + weightTextField.getText() + "\n";
            try {
                Files.write(Paths.get("/home/jon/fitness/weight.txt"), content.getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public static void main(String args[]) {
        WeightTracker tracker = new WeightTracker();
        tracker.setVisible(true);
        tracker.setSize(300, 200);

//        tracker.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //      tracker.setBounds(400, 400, 250, 100);


    }
}