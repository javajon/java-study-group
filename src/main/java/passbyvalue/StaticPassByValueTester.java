package passbyvalue;

/**
 * Created by rc00028 on 23/06/2017.
 */
public class StaticPassByValueTester {

    static int num = 4;
    public static void main(String[] args) {

        System.out.println("main, value of num before: " + num);
        newNumber(num);
        System.out.println("main, value of num after: " + num);
    }

    public static void newNumber(int number) {
        System.out.println("newNumber, value of num before: " + num);
        num = 8;
        System.out.println("newNumber, value of num after: " + num);
    }
}
