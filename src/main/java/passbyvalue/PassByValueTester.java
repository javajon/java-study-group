package passbyvalue;

/**
 * Created by rc00028 on 23/06/2017.
 */
public class PassByValueTester {

    public static void main(String[] args) {
        int num = 4;

        System.out.println("main, value of num before: " + num);
        newNumber(num);
        System.out.println("main, value of num after: " + num);
    }

    public static void newNumber(int num) {
        System.out.println("newNumber, value of num before: " + num);
        num = 8;
        System.out.println("newNumber, value of num after: " + num);
    }
}
