package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */
public class Bear implements Herbivore, Omnivore {
    @Override
    public void eatMeat() {
        System.out.println("Eating meat");
    }

    //This is defined in  Herbivore, Omnivore. One implementation satisfies both of them.
    @Override
    public void eatPlants() {
        System.out.println("Eating plants");
    }
}