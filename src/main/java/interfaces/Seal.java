package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 *
 * Like an abstract class, an interface may be extended using the extend keyword.
 * In this manner, the new child interface inherits all the abstract methods of the parent interface.
 *
 * This is like multiple inheritance - not allowed in classes, but oK in interfaces.
 *
 */
public interface Seal extends HasTail, HasWhiskers {
}