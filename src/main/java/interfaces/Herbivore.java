package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */
public interface Herbivore {
    public void eatPlants();
}

//This shows how, like classes, multiple interfaces can be defined in one file - but only one can be public.
//Neither have to be public.
interface Omnivore {

    //This has the same signature as another interface - Herbivore. So a class that
    //implements both can satisfy this with a single implementation.
    public void eatPlants();

    //If there was no change in Herbivore, if the method signature above was changed to this,
    //the implementing class would fail compilation as this contravenes overloading rules.
    //In this case, teh implementing class could not implement both interfaces.
    //This would also cause an interface that extended both to not compile.
    //public int eatPlants();

    //This has a different as another interface - Herbivore. It is essentially an overloaded method.
    // So a class that implements both would have to implement both.
    //public void eatPlants(int animalSize);

    public void eatMeat();
}