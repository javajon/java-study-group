package interfaces;

/**
 *
 * CanBurrow - example of an Interface
 *
 * An interface is an abstract data type that defines a list of abstract public methods that any class implementing the interface must provide.
 * An interface can also include a list of constant variables and default methods.
 *
 * Created by rc00028 on 28/06/2017.
 */
public abstract interface CanBurrow {

    //Maximum depth in metres this can burrow.
    public static final int MINIMUM_DEPTH = 5;

    public abstract int getMaximumDepth();
    public abstract int getMinimumDepth();

}