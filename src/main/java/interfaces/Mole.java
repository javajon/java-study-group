package interfaces;


import zoo.animal.Animal;

/**
 * Created by rc00028 on 28/06/2017.
 */
public class Mole extends Animal implements CanBurrow {

    public Mole(String name) {
        //int i = 2;
        super(name, "mole");
    }

    @Override
    public void eat() {
        System.out.println("Although moles like to eat grubs," +
                "using a chemical control against grubs won't" +
                " work for mole control because the pest's diet" +
                "also includes earthworms (which are good for your soil)" +
                "and other insects. Avoid using insecticides on lawns," +
                "as it may kill beneficial insects.");

    }

    @Override
    public int getMaximumDepth() {
        return 3;
    }

    @Override
    public int getMinimumDepth() {
        return 0;
    }
}
