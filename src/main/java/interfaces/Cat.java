package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */
public class Cat implements Walk, Run { // DOES NOT COMPILE

    //Without the below method, this won't compile due to a conflict in default interface methods.
    public int getSpeed() {
        return 1;
    }

    public static void main(String[] args) {
        System.out.println(new Cat().getSpeed());
    }
}
