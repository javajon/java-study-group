package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */
public interface Carnivore {
    //public default void eatMeat();  // DOES NOT COMPILE -cannot be abstract
    /*
    public int getRequiredFoodAmount() {  // DOES NOT COMPILE - not default but has body
        return 13;
    }
    */
}