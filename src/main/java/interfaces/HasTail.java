package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */
public interface HasTail {
    public int getTailLength();
}
