package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 *
 * Default interface methods introduce potential multiple inheritance issues. These rules address these.
 *
 * <p>
 * 1. When an interface extends another interface that contains a default method,
 * it may choose to ignore the default method, in which case the default implementation
 * for the method will be used.
 * <p>
 * 2. Alternatively, the interface may override the definition of the default method
 * using the standard rules for method overriding, such as not limiting the
 * accessibility of the method and using covariant returns.
 * <p>
 * 3. Finally, the interface may redeclare the method as abstract, requiring classes that
 * implement the new interface to explicitly provide a method body.
 */
public interface IsWarmBloodedChild extends IsWarmBlooded {


    //For option 1, comment everything in the interface body.
    //Option 2.

    /*
    @Override
    public default double getTemperature() {
        return 38.0;
    }
    */


    //Option 3: -
    //public double getTemperature();

    //Does not compile.
    /*
    public double getTemperature(){
        return 35.6;
    }
    */



}
