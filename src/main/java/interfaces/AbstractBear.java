package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */
public abstract class AbstractBear implements Herbivore, Omnivore {} // DOES NOT COMPILE if the two interfaces have methods which contravene overloading rules.