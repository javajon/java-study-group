package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */
public class Bunny implements Hop {
    public void printDetails() {
        //System.out.println(getJumpHeight()); // DOES NOT COMPILE
        System.out.println(Hop.getJumpHeight()); // COMPILES FINE
    }
}