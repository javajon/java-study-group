package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */
public interface HasWhiskers {
    public int getNumberOfWhiskers();

    //The exam creators are fond of questions that mix class and interface terminology.
    //Watch out for this.
    //public int getAverageLengthOfWhiskers(){ return 2; } //DOES NOT COMPILE

}
