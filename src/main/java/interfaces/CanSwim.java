package interfaces;

/**
 *
 * Interface variables
 *
 * Created by rc00028 on 03/07/2017.
 *
 * 1. Interface variables are assumed to be public, static, and final.
 * Therefore, marking a variable as private or protected will trigger a compiler error,
 * as will marking any variable as abstract.
 *
 * 2. The value of an interface variable must be set when it is declared since it is marked as final.
 */
public interface CanSwim {
    //public static and final keywords are all implicit, but it's OK to include them explicitly if desired.
    public static final int MAXIMUM_DEPTH = 100;
    public static final boolean UNDERWATER = true;
    //public static final String TYPE = "Submersible";

    //private int MAXIMUM_DEPTH = 100;  // DOES NOT COMPILE
    //protected abstract boolean UNDERWATER = false;  // DOES NOT COMPILE
    //public static String TYPE;  // DOES NOT COMPILE
}
