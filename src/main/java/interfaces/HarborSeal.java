package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 *
 * Any child class of HarborSeal will need to implement all of HarborSeal's
 * abstract methods and those in both interfaces to become a non-abstract class
 */
//public abstract class HarborSeal implements HasTail, HasWhiskers {
public abstract class HarborSeal implements Seal {
    abstract void abstractMethod();
}
