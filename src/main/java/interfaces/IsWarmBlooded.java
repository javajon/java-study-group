package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 *
 * This demonstrates default methods in interfaces.
 *
 * Introduced in Java 8.
 *
 * A default method within an interface defines an abstract method with a default implementation.
 * In this manner, classes have the option to override the default method if they need to, but they are not required to do so.
 * If the class doesn’t override the method, the default implementation will be used.
 * In this manner, the method definition is concrete, not abstract.
 *
 * The purpose of adding default methods to the Java language was in part to help with code development and backward compatibility.
 * Imagine you have an interface that is shared among dozens or even hundreds of users that you would like to add a new method to.
 * If you just update the interface with the new method, the implementation would break among all of your subscribers,
 * who would then be forced to update their code. In practice, this might even discourage you from making the change altogether.
 * By providing a default implementation of the method, though, the interface becomes backward compatible with the existing codebase,
 * while still providing those individuals who do want to use the new method with the option to override it.
 *
 * Rules for default interface methods
 * 1. A default method may only be declared within an interface and not within a class or abstract class.
 * 2. A default method must be marked with the default keyword. If a method is marked as default, it must provide a method body.
 * 3. A default method is not assumed to be static, final, or abstract, as it may be used or overridden by a class that implements the interface.
 * 4. Like all methods in an interface, a default method is assumed to be public and will not compile if marked as private or protected.
 *
 */
public interface IsWarmBlooded {
    boolean hasScales();

    //An implementer can either implement this, or it will inherit the default implementation.
    //A default method cannot be static, abstract or final.
    public default double getTemperature() {
        return 37.0;
    }
}