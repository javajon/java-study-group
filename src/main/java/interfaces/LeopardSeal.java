package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */
//public class LeopardSeal implements HasTail, HasWhiskers {  // DOES NOT COMPILE
//}

//JH - uncomment above and make compile.


public class LeopardSeal extends HarborSeal {
    @Override
    public int getTailLength() {
        return 0;
    }

    @Override
    public int getNumberOfWhiskers() {
        return 0;
    }

    @Override
    void abstractMethod() {

    }
}
