package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */

public class Stoat implements IsWarmBlooded {

    @Override
    public boolean hasScales() {
        return false;
    }

    @Override
    public double getTemperature() {
        return 0;
    }
}
