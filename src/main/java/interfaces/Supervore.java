package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 *
 * With interfaces, we can have multiple inheritance.
 *
 */
public interface Supervore extends Herbivore, Omnivore {} // DOES NOT COMPILE if the two interfaces have methods which contravene overloading rules.
