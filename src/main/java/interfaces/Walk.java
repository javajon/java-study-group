package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */
public interface Walk {
    public default int getSpeed() {
        return 5;
    }
}
