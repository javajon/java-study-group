package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 * <p>
 * Static methods in interfaces: -
 * <p>
 * Introduced in Java 8.
 * <p>
 * There is really only one distinction between a static method in a class and an interface.
 * A static method defined in an interface is not inherited in any classes that implement the interface.
 * <p>
 * It follows, then, that a class that implements two interfaces containing static methods with the same
 * signature will still compile at runtime, because the static methods are not inherited by the subclass
 * and must be accessed with a reference to the interface name. Contrast this with the behavior you saw
 * for default interface methods in the previous section: the code would compile if the subclass overrode
 * the default methods and would fail to compile otherwise.
 * <p>
 * Here are the static interface method rules you need to be familiar with:
 * <p>
 * 1. Like all methods in an interface, a static method is assumed to be public and will
 * not compile if marked as private or protected.
 * <p>
 * 2. To reference the static method, a reference to the name of the interface must be used.
 */
public interface Hop {
    static int getJumpHeight() {
        return 8;
    }

    static void printGreeting() {
        System.out.println("Hellow Interface!");
    }
}
