package interfaces;

/**
 * Created by rc00028 on 03/07/2017.
 */

//Does not compile - a class cannot extend an interface
/*
public class Mouse extends HasTail {
    @Override
    public int getTailLength() {
        return 0;
    }
}
*/

