/**
 * Created by jhead on 07/02/2018.
 *
 * Compound Operators
 * ==================
 *
 * *=
 * /=
 * +=
 * -=
 *
 *
 *
 *C's. F
 */
package operators;

public class CompoundOperatorTester {
    public static void main(String[] args) {
        //Compound operators: *=, /=, +=, -=
        int x = 3;
        int y = 5;

        //y *= x;
        //When x and y are both int, this is the same as: -
        y = y * x;

        System.out.println("y: " + y);


        //However, when one is a short, the second form does not require casting: -
        int a = 3;
        short b = 5;

        //b = b * a;  // DOES NOT COMPILE
        b *= a;  //COMPILES FINE WITH COMPOUND ASSIGNMENT

        //System.out.println("b: " + y);

        //The same principle applies to long: -
        int c = 3;
        long d = 5;

        //c = c * d;  // DOES NOT COMPILE
        c *= d;  //COMPILES FINE WITH COMPOUND ASSIGNMENT

        //System.out.println("c: " + y);


    }
}
