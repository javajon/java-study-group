package operators;

/**
 * Created by jhead on 17/01/2018.
 *
 * Binary Operators
 * ================
 *
 * Assignment operator is a binary operator.
 *
 * =
 *
 * Arithmetic binary operators:
 *
 * +
 * -
 * /
 * *
 * %
 *
 * By convention we use a space to delimit the operators from the operands in binary expressions:
 * e.g. int i = 1 + 5;
 *
 * But it also works fine without any spacing or with multiple spacing.
 *
 * Binary arithmetic operators follow the BODMAS system, as used in schools.
 * *, / and % take precedence over + and - and are evaluated first.
 * Brackets can override the natural precedence -
 * When priority is equal, evaluate left to right.
 *
 * Rules of arithmetic promotion
 *
 * 1. If two values have different data types, Java will automatically promote one of the values to the larger of the two data types.
 * 2. If one of the values is integral and the other is floating-point, Java will automatically promote the integral value to the floating-point value’s data type.
 * 3. Smaller data types, namely byte, short, and char, are first promoted to int any time they’re used with a Java binary arithmetic operator, even if neither of the operands is int.
 * 4. After all promotion has occurred and the operands have the same data type, the resulting value will have the same data type as its promoted operands.
 *
 */
public class BinaryOperatorTester {

    public static void main(String[] args) {

        //By convention we use a space to delimit binary expressions:
        int i = 1 + 5;

        //But it works fine without any:
        int j = 1+6;

        //It also works fine with extra spacing:
        int k = 1         +         6;

        int binaryOperatorPrecedence = 2 * 5 + 3 * 4 - 8;

        /*

        1. 2 * 5 is evaluated, so expression becomes:

        10 + 3 * 4 - 8

        2. 3 * 4 is evaluated, so expression becomes:

        10 + 12 - 8

        3. The expression is now evaluated left to right. This evaluates to 14:

         */
        System.out.println("Result of evaluating 2 * 5 + 3 * 4 - 8:    " + binaryOperatorPrecedence);

        //Bracketing can be used to override precedence.
        binaryOperatorPrecedence = 2 * ((5 + 3) * 4 - 8);

        System.out.println("Result of evaluating 2 * ((5 + 3) * 4 - 8):    " + binaryOperatorPrecedence);



        //Modulus = remainder
        System.out.println(9 / 3);  // Outputs 3
        System.out.println(9 % 3);  // Outputs 0
        System.out.println(9 % 6);  // Outputs 3
        System.out.println(9 / 6);  // Outputs 1 (loses precision)

        //Due to precedence, in the below example 4/2 and 8*3 will be evaluated first
        //So the equation effectively becomes: 1 + 2 + 24 - 2
        //Bracketing shows this more clearly: 1 + (4 / 2) + (8 * 3) - 2
        double doubleResultOne = 1 + 4 / 2 + 8 * 3 - 2;
        int intResultOne = 1 + 4 / 2 + 8 * 3 - 2;

        //Use of double for the results is advisable because decimals may result. Otherwise we lose precision.
        System.out.println("doubleResultOne: " + doubleResultOne);
        System.out.println("intResultOne: " + intResultOne);

        //Demonstrate loss of precision when using an int with decimals.
        //Note that 4/3 produces a decimal
        //JH - show how to get decimal precision.
        //This is as above, but 3 instead of 2.
        //double doubleResultTwo = 1 + 4 / 3 + 8 * 3 - 2;

        //To avoid loss of precision, change above to below.
        double doubleResultTwo = 1 + (double)4 / 3 + 8 * 3 - 2;

        int intResultTwo = 1 + 4 / 3 + 8 * 3 - 2;

        System.out.println("doubleResultTwo: " + doubleResultTwo);
        System.out.println("intResultTwo: " + intResultTwo);

        /*
        double myDouble = 39.21;
        float myFloat = 2.1F;

        System.out.println(myDouble + myFloat);
*/
        //Review the 4 promotion rules to explain the above and before explaining the following examples.


        //= is a binary operator (assignment): -
/*

        int a = 1.0;  // DOES NOT COMPILE
        short b = 1921222;  // DOES NOT COMPILE
        int c = 9f;  // DOES NOT COMPILE
        long d = 192301398193810323;  // DOES NOT COMPILE
*/

        //The below examples address the above issues.


        int a = (int) 1.0;
        short b = (short) 1921222;
        int c = (int) 9f;
        long d = 192301398193810323L;


        //This demonstrates all the binary promotion rules: -
        short myShortTwo = 14;
        float myFloatTwo = 13;
        double myDoubleTwo = 30;

        double result = myShortTwo * myFloatTwo / myDoubleTwo;
        //float floatResult = myShortTwo * myFloatTwo / myDoubleTwo; //DOESN'T COMPILE - rule 4
        //System.out.println(result);

        //Demonstration of casting and overflow, which can lead to loss of precision.
        //short s = 1921222; //DOES NOT COMPILE
        //This can be fixed by casting, but precision is lost by overflow.
        short s = (short) 1921222;
        System.out.println(s);

        //Similarly
        short e = 10;
        short f = 3;

        //short g = e * f;  // DOES NOT COMPILE
        //Requires a cast
        short g = (short)(e * f);

        //Or preferable to use an int instead due to possible loss of precision
        int h = e * f;

        //Assignment operator can be chained: -

        int myInt = 3;
        int myInt2 = (myInt = 2);

        //But can't do assignment of the second operand on the same line: -
        //int c = (int d = 2);  // DOES NOT COMPILE

        //Look at Unary operators and then return here.
        //Combining unary and binary operators.
        short x = 3;
        //short z = ++x * 5 / x-- + --x;   //DOES NOT COMPILE.

        //Must cast to a short to make this work. The whole expression must be put in parentheses.
        short z = (short) (++x * 5 / x-- + --x);
        int myInt222 = ++x * 5 / x-- + --x;

        //1. 4 * 5 / x-- + --x

        //2. 4 * 5 / 4   + --x      (value of x is 3)

        //3. 4 * 5 / 4   +   2

        //4. 20/4        +   2

        //5. 5           +   2

        //6. 7

        System.out.println("++x * 5 / x-- + --x, where x starts with a value of 3: " + z);

        //Using + operator on Strings serves to concatenate them, as we often see in System.out statements.

        String partOne = "part one";
        String partTwo = "part two";
        String space = " ";
        String comma = ",";

        System.out.println(partOne + comma + space + partTwo);

    }
}
