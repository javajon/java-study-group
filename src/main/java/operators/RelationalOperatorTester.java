/**
 * Created by jhead on 07/02/2018.
 *
 * Relational Operators
 * ====================
 *
 * Relational operators evaluate to a boolean.
 *
 * >
 * <
 * >=
 * <=
 * instanceof   NOTE: This is a rare example of not using camel case.
 *
 */
package operators;

import zoo.animal.Animal;
import zoo.animal.Lion;
import zoo.animal.Tiger;

public class RelationalOperatorTester {

    /* Relational operators evaluate to a boolean

     >
     <
     >=
     <=
     ==
     instanceof

      */

    public static void main(String[] args) {

        boolean a = 3 < 1;
        boolean b = 3 >= 1;
        boolean c = 3 == 1;
        boolean d = 3 == 3;

        System.out.println("3 < 1: " + a);
        System.out.println("3 >= 1: " + b);
        System.out.println("3 == 1: " + c);
        System.out.println("3 == 3: " + d);

        if (3 < 1) {
            System.out.println("3 is less than 1");
        } else if (3 == 1) {
            System.out.println("3 is equal to 1");
        } else if (3 > 1) {
            System.out.println("3 is greater than 1");
        } else {
            System.out.println("I'm baffled!");
        }


        //Demonstrating instanceof
        Animal animal = new Lion("Leopold");
        animal.getName();
        //animal.roar();
        boolean isAnimalLion = animal instanceof Lion;
        boolean isAnimalTiger = animal instanceof Tiger;

        System.out.println("Is animal a lion?: " + isAnimalLion);
        System.out.println("Is animal a tiger?: " + isAnimalTiger);

        //animal can therefore be cast as a Lion.
        Lion myLion = (Lion) animal;
        myLion.roar();

        //But animal cannot be cast as a Tiger. The below line would throw a ClassCastException
        //System.out.println("Attempting Tiger myTiger = (Tiger)animal;");
        //Tiger myTiger = (Tiger)animal;

        //To protect against this, check first using instanceof.
        //This is covered in more detail in the Exception Handling section.
        if (isAnimalTiger){
            System.out.println("Tiger yourTiger = (Tiger)animal;");
            Tiger yourTiger = (Tiger)animal;
        }

        Elephant elephant = new Elephant();
        //Above chapter 2, revisit the below in later chapters
        //The below lines will not compile as it is not on same final inheritance branch

        //boolean isElephantAnimal = elephant instanceof Animal;
        //boolean isElephantString = elephant instanceof String;
        boolean isLionString = myLion instanceof Object;

        //NOTE: instanceof is out of scope for the exam.

    }

}

class Elephant{

}