/**
 * Created by jhead on 07/02/2018.
 *
 * Equality Operators
 * ==================
 *
 * Equality operators evaluate to a boolean.
 *
 *
 *
 * ==     (returns true if two primitives or objects are equal)
 * !=     (returns true if two primitives or objects are not equal)
 *
 * Be careful not to confuse assignment operator (=) with equality operator(==)
 *
 */
package operators;

import java.io.File;

public class EqualityOperatorTester {
    public static void main(String[] args) {


        //Be careful not to confuse assignment operator (=) with equality operator(==)
        int five = 5;
        int fiveMore = 5;

        System.out.println("five == fiveMore: " + (five == fiveMore));

        int anotherFive = five = fiveMore;
        System.out.println("five = fiveMore: " + (five = fiveMore));
//        System.out.println("anotherFive: " + anotherFive);

        //Must use == here, = does not compile. An if requires a boolean condition.
        int x = 1;
        if (x == 5){
            System.out.println("x is equal to 5.");
        } else {
            System.out.println("x is NOT equal to 5. In fact x is equal to " + x);
        }

        //Testing equality of objects using File.
        File f1 = new File("myFile.txt");
        File f2 = new File("myFile.txt");
        File f3 = f1;

        //f1 and f2 are different objects.
        System.out.println("f1 == f2: " + (f1 == f2));

        //f3 and f1 point to the same object.
        System.out.println("f3 == f1: " + (f3 == f1));

        //It is important to recognise the difference between == and equals();
        System.out.println("f1.equals(f2): " + (f1.equals(f2)));
        System.out.println("f3.equals(f1): " + (f3.equals(f1)));


        //Testing equality of objects using String.
        String s1 = new String("Hello World");
        String s2 = new String("Hello World");
        String s3 = s1;

        System.out.println("s1 == s2: " + (s1 == s2));
        System.out.println("s3 == s1: " + (s3 == s1));

        //This demonstrates the uniqueness of Strings
        //1. We can instantiate them without a "new".
        //2. When we do this, we use the String pool.
        String s4 = "Hello World";
        String s5 = "Hello World";

        System.out.println("s4 == s5: " + (s4 == s5));
        System.out.println("s1.equals(s2): " + (s1.equals(s2)));
        System.out.println("s3.equals(s1): " + (s3.equals(s1)));
        System.out.println("s4.equals(s5): " + (s4.equals(s5)));

    }
}
