package operators;

/**
 * Created by jhead on 17/01/2018.
 *
 * Unary Operators
 * ================
 *
 * +   (makes sign positive)
 * -   (reverses sign)
 * ++  (increment - can be pre or post)
 * --  (decrement - can be pre or post)
 * !   (reverses a boolean value)
 *
 * In pre increment expressions, the value is returned to the expression AFTER being incremented:
 *
 * ++y
 * --x

 * In post increment expressions, the value is returned to the expression BEFORE being incremented:
 *
 * y++
 * x--
 *
 * By convention spaces are not used between operator and operand in unary expressions:
 * e.g. i++;
 *
 * But it also works fine with a single or even multiple spacing.
 *
 */
public class UnaryOperatorTester {
    public static void main(String[] args) {

        int i = 0;

        //By convention spaces are not used between operator and operand in unary expressions:
        int j = i++;

        //But it works fine with a single space:
        int k = ++ i;

        //It also works fine with mulitple spacing:
        int l = -                         i;

        //+ operator
        int positiveInt = +1;

        //- operator
        int negativeInt = -positiveInt;

        //! operator
        boolean negatedBoolean = !false;

        //+ makes the value positive.
        System.out.println("Postive int: " + positiveInt);

        //- reverses the sign.
        System.out.println("Negative int: " + negativeInt);

        //Post increment, the value is returned to the expression BEFORE being incremented.
        System.out.println("Post-incremented postive int: " + positiveInt++); //value becomes 2 after expression is resolved

        //Pre increment, the value is returned to the expression AFTER being incremented.
        System.out.println("Pre-incremented postive int: " + ++positiveInt);

        //Post decrement, the value is returned to the expression BEFORE being decremented.
        System.out.println("Post-decremented negative int: " + negativeInt--);

        //Pre decrement, the value is returned to the expression AFTER being decremented.
        System.out.println("Pre-decremented negative int: " + --negativeInt);

        //Boolean negation - whatever the vlue of boolean starts is, the negation operator reverses it.
        System.out.println("Negated boolean value: " + negatedBoolean);

        //Unary operators and binary operators in the same expression.
        //unary operators have precedence over binary, but only when "pre" will be evaluated before the expression.
        //Post-increment/decrement example
        short eight = 8;
        byte four = 4;
        double precedenceEg = eight++ / four--;
        System.out.println("Post increment unary precedence example: " + precedenceEg);

        //Reset the values and this time pre-increment/decrement
        eight = 8;
        four = 4;
        precedenceEg = ++eight / --four;
        System.out.println("Pre increment unary precedence example: " + precedenceEg);

        //Return to BinaryOperatorTester.java to see more expressions that combine unary operators with binary operators.

    }
}
