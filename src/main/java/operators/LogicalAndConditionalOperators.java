/**
 * Created by jhead on 07/02/2018.
 *
 * Logical And Conditional (Short-Circuit) Operators
 * =================================================
 *
 *     Logical And Conditional operators evaluate to a boolean.
 *
 *     Logical Operators: -
 *
 *     &   (AND)
 *     |   (OR)
 *     ^   (EXCLUSIVE OR)
 *
 *     Conditional Operators: -
 *
 *     &&
 *     ||
 *
 *     Conditional (Short-Circuit) Operators are like Logical operators,
 *     but the right hand may not be evaluated. These are commonly used in guards.
 *
 *
 */
package operators;

import zoo.animal.Lion;

public class LogicalAndConditionalOperators {

    public static void main(String[] args) {
        boolean a = 5 < 3;
        boolean b = 6 >= 2;

        //Logical Operator examples

        if (a & b) {
            System.out.println("Both a and b are true");
        }

        if (a | b) {
            System.out.println("Either a and b or both are true");
        }

        if (a ^ b) {
            System.out.println("Either a and b but not both are true");
        }

        if (!a & !b) {
            System.out.println("Both a and b are false");
        }

        //Conditional Operator examples
        //For reference: a is false, b is true.
        if (a && b) {
            System.out.println("Both a and b are true");
        }

        if (a || b) {
            System.out.println("Either a and b or both are true");
        }

        //The above shows identical behaviour in Conditional as Logical operators.
        //The key difference can be seen in guard conditions.
        Lion lion = null;
        //lion = new Lion();
        //lion.setName("Layla");


        /*
        if (lion != null & lion.getName() != null){
            System.out.println("(Logical) I am a Lion called " + lion.getName());
        }
*/


        if (lion != null && lion.getName() != null){
            System.out.println("(Conditional) I am a Lion called " + lion.getName());
        } else {
            System.out.println("By using a conditional and not a logical operator, we avoided a NullPointerException (smug smile!)");
        }



    }
}
