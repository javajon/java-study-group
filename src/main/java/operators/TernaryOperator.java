/**
 * Created by jhead on 07/02/2018.
 *
 * Ternary Operator
 * ====================
 *
 * Relational operators evaluate to a boolean.
 *
 * Ternary operations include three parts. They can be an alternative to an if statement.
 *
 * The first operand must be a boolean expression.
 *
 * The second and third can be any expression that returns a value.
 *
 *
 */
package operators;

public class TernaryOperator {

    //If referenced directly, these must be marked static.
    //Alternatively, they could be declared as local variables.
    static String statement;
    static int value = 1;

    public static void main(String[] args) {

        //Standard if statement
        if (value >= 3 ) {
            statement = "equal to or greater than 3";
        } else {
            statement = "less than 3";
        }

        System.out.println(statement + " (if statement)");

        //Alternative using ternary operator
        statement = (value >= 3) ? ("equal to or greater than 3") : ("less than 3");

        System.out.println(statement + " (ternary operator)");

        //Both possibilities must be of the correct data type.
        //statement = (value >= 3) ? "equal to or greater than 3" : 10; //DOES NOT COMPILE.

        //Ternary operator can be used with all data types.
        //Example showing int.
        //NOTE: Underscore allowed in numerics. Not as first or last character or either side of a decimal point.
        long ternaryLong = (value > 3) ? 1 : 10_000_000_000_000_000L;

        //These sysout statements also demonstrate that + acts to concatenate Strings.
        System.out.println("ternaryLong: " + ternaryLong);

        //Demonstrate usage of underscore.
        double myDouble = 10.0;

        //Re-emphasise rules of arithmetic promotion.
        long ternaryLongInt = (value > 3) ? 1 : 1_000_000L; //DOES NOT COMPILE

        //Compiles if we assign to a long variable.
        //long ternaryLong = (value > 3) ? 1 : 1_000_000L;

        //This example shows the second and third operands as expressions.
        //Bracketing is not necessayr here, but vastly improves readablity.
        int y = 10;
        int answer = (y > 12) ? (2 * y) : (3 * y);
//        int answer = y > 5 ? 2 * y : 3 * y;
        System.out.println("answer: " + answer);
    }
}
