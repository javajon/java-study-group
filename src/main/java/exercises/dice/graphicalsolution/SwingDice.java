package exercises.dice.graphicalsolution; /**
 * Created by U.8005072 on 07/12/2017.
 */


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;


public class SwingDice extends JFrame implements ActionListener {

    JButton diceButton = new JButton("Roll Dice");
    JLabel resultLabel = new JLabel("Result");

    public SwingDice() {
        // set flow layout for the frame
        this.getContentPane().setLayout(new FlowLayout());
        //set action listener for button
        diceButton.addActionListener(this);
        //add components to the frame
        this.add(diceButton);
        this.add(resultLabel);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        if (action.equals("Roll Dice")) {
            // Generate number, convert, and set resutLabel with converted result
            Random random = new Random();
            int n = random.nextInt(6) + 1;
            String str = String.valueOf(n); // Convert int(n) to String(str) for JLabel compatibility
            resultLabel.setText("You rolled: " + str);
        } else {
        }
    }


    public static void createAndShowGUI() {
        // Init Frame
        JFrame frame = new SwingDice();
        frame.setTitle("Dice Program");

        // Display the Frame
        frame.setAlwaysOnTop(true);
        frame.setSize(300,200);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
