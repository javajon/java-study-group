package exercises.dice.graphicalsolution;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Random;

/**
 * AWTDiceRoller simulates a dice roller using the java.awt package.
 *
 * @author Jon
 * @date 09/01/2018
 */
public class AWTDiceRoller extends Frame implements ActionListener {
    Button rollDiceButton = new Button("Roll Dice");
    Label dice1Label = new Label("Dice 1");
    Label dice2Label = new Label("Dice 2");
    Label resultLabel = new Label("Total");
    TextField dice1TextField = new TextField(20);
    TextField dice2TextField = new TextField(20);
    TextField resultTextField = new TextField(20);

    public AWTDiceRoller() {
        super("Dice Roller");
        setLayout(null);
        dice1Label.setBounds(20, 50, 55, 25);
        add(dice1Label);
        dice2Label.setBounds(20, 100, 55, 25);
        add(dice2Label);
        resultLabel.setBounds(20, 150, 55, 25);
        add(resultLabel);
        dice1TextField.setBounds(100, 50, 100, 25);
        add(dice1TextField);
        dice2TextField.setBounds(100, 100, 100, 25);
        add(dice2TextField);
        resultTextField.setBounds(100, 150, 100, 25);
        add(resultTextField);
        rollDiceButton.setBounds(60, 200, 80, 25);
        add(rollDiceButton);
        rollDiceButton.addActionListener(this);
        addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }
                }
        );
    }

    public void actionPerformed(ActionEvent AE) {
        if (AE.getSource() == rollDiceButton) {
            Random r = new Random();
            int dice1 = r.nextInt(6) + 1;
            dice1TextField.setText("" + dice1);

            int dice2 = r.nextInt(6) + 1;
            dice2TextField.setText("" + dice2);

            int result = dice1 + dice2;
            resultTextField.setText("" + result);
        }
    }

    public static void main(String args[]) {
        AWTDiceRoller MC = new AWTDiceRoller();
        MC.setVisible(true);
        MC.setSize(600, 500);
    }
}