package exercises.dice.oneclassstaticsolution;

import java.util.Random;

/**
 * Created by u.7829872 on 14/12/2017.
 */
public class Dice {

    public static int rollDice() {
        Random r = new Random();
        int result = r.nextInt(6) + 1;
        return result;
    }

    public static void main(String[] args) {
        int score1 = Dice.rollDice();
        int score2 = Dice.rollDice();

        int result = score1 + score2;

        System.out.println("One class static solution");
        System.out.println("Dice 1: " + score1);
        System.out.println("Dice 2: " + score2);
        System.out.println("Total: " + result);
    }
}
