package exercises.dice.twoclasssolution;

/**
 * Created by u.7829872 on 14/12/2017.
 */
public class DiceRoller {
    public static void main(String[] args) {



        Dice dice = new Dice();
        int score1 = dice.rollDice();
        int score2 = dice.rollDice();

        int result = score1 + score2;

        System.out.println("Two class solution");
        System.out.println("Dice 1: " + score1);
        System.out.println("Dice 2: " + score2);
        System.out.println("Total: " + result);
    }
}
