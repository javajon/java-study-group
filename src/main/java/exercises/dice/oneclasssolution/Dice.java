package exercises.dice.oneclasssolution;

import java.util.Random;

/**
 * Created by u.7829872 on 14/12/2017.
 */
public class Dice {

    public int rollDice() {
        Random r = new Random();
        int result = r.nextInt(6) + 1;
        return result;
    }

    public static void main(String[] args) {
        System.out.println("One class solution");
        Dice dice = new Dice();
        int score1 = dice.rollDice();
        int score2 = dice.rollDice();

        int result = score1 + score2;

        System.out.println("Dice 1: " + score1);
        System.out.println("Dice 2: " + score2);
        System.out.println("Total: " + result);
    }
}
