package inheritanceandconstructors;

public class Airplane {

    static int start =2;
    final int end;

    public Airplane() {
        System.out.println("Airplane default constructor");
        int x = 4;
        end = x;
    }

    public Airplane (int x) {
        x = 4;
        end = x;
    }

    public void fly(int distance) {
        System.out.print(end-start+" ");
        System.out.println(distance);
    }

    public static void main(String ... start) {
        // TODO Auto-generated method stub

        new Airplane();

        new Airplane(10).fly(5);

        new String("Useless!");

        Airplane rs = new Airplane();
        rs.fly(5);

        Airplane rs2 = new Airplane(10);
        rs.fly(5);
    }

}
