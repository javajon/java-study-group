package inheritanceandconstructors;

/**
 * Created by rc00028 on 11/05/2017.
 */
public class Tiger extends Animal {

    public Tiger(String name, String type){
        super(name, type);
        System.out.println("Tiger constructor");
        System.out.println("Tiger super class: " + Tiger.class.getSuperclass().getName());
    }
}
