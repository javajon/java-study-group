package inheritanceandconstructors;

/**
 * Created by rc00028 on 26/06/2017.
 * <p>
 * final variables must be assigned once and once only.
 * <p>
 * This can happen at declaration,
 * in an initialiser block,
 * or in a constructor as is the case here.
 */
public class MouseHouse {
    private final int volume;
    private final String name = "The Mouse House";

    public MouseHouse(int length, int width, int height) {
        volume = length * width * height;
    }
}
