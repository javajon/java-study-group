package inheritanceandconstructors;

/**
 * Created by rc00028 on 26/06/2017.
 */
public class Bunny {
    private String colour;
    private int height;
    private int length;

    public Bunny() {
        System.out.println("constructor");
    }

    public Bunny(int length, int theHeight) {
        length = this.length; // backwards but legal, so could cause a bug.
        height = theHeight; // fine because a different name
        this.colour = "white"; // fine, but redundant - could be done at declaration of the variable
    }

    public Bunny(int length, int theHeight, String colour) {
        length = this.length; // backwards but legal, so could cause a bug.
        height = theHeight; // fine because a different name
        colour = "white"; // fine, but redundant - could be done at declaration of the variable
    }

    //this can be used to call one constructor with another.
    public Bunny(String colour) {
        //The following line would cause a compilation line on the line after that.
        //System.out.println("Not allowed before call to this");
        //Calls to super or this must be the first line ina  constructor.
        this(2, 3, colour);

        //In this way constructors can be chained (see p.201).
    }



    //What is wrong with the following with regard to constructors.
    //public bunny() { }     // DOES NOT COMPILE
    //public void Bunny() { }

    public static void main(String[] args) {

    }
}
