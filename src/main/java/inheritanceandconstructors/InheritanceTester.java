package inheritanceandconstructors;

/**
 * Created by rc00028 on 26/06/2017.
 *
 *  The first thing a constructor must do is call out to the super constructor.
 *  If the superclass has a default no arguments constructor, this is done implictly.
 *  Otherwise, a super constructor must be called explicitly as in BengalTiger.
 *
 *  Remember that a constructor cannot have a return type - that would make it a method.
 *
 */
public class InheritanceTester {

    private String testMessage;
    public InheritanceTester() {
        System.out.println("Default constructor. You get me implicitly unless you define another constructor.");
        System.out.println("If you still want me after that, as is the case here, you must define me explicitly.");
    }

    public InheritanceTester(String message) {
        System.out.println("Overloaded constructor that takes a String argument and prints it: " + message);

        //The this keyword can be used.
        this.testMessage = message;

        //Or if omitted, it default to the variable with that name with the most limited scope.
        testMessage = message;

        //In this case, both the above have the same effect.

    }

    //On the exam, beware of methods that look like constructors.
    public void InheritanceTester() {
        System.out.println("I'm pretending to be the Default constructor. But I'm actually a method.");
    }

    public void InheritanceTester(String greeting) {
        System.out.println("I'm pretending to be an overloaded constructor. But I'm actually an overloaded  method.");
    }

    public void constructorTester(){

        //This only works because we have a method masquerading as a constructor.
        //If the method is commented out, compilation fails - the constructor cannot be called like that.
        InheritanceTester("Hello");

        //This is how we call the constructor.
        InheritanceTester tester = new InheritanceTester("Hello");

    }

    //JH - Demo the Chick class before returning here.

    public static void main(String[] args) {
        BengalTiger tiger = new BengalTiger("Anil", "Tiger");
        System.out.println("I'm a Bengal Tiger. My name is " + tiger.getName());

    }
}
