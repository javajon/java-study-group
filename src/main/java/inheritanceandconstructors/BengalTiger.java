package inheritanceandconstructors;

/**
 * Created by rc00028 on 11/05/2017.
 */
public class BengalTiger extends Tiger {
    int one = 1;
    int two = 2;
    String bengal = "Bengal";
    String tiger = "Tiger";
    String space = " ";

    public BengalTiger(String name, String type) {

        //The first thing a constructor must do is call out to the super constructor.
        //If the superclass has a default no arguments constructor, this is done implictly.
        //Otherwise, a super constructor must be called explicitly as in this case.
        super(name, type);

        System.out.println(one + two + space + bengal + space + tiger + space + one + two);
        //We can't call a constructor twice
        //super(name, gender); //DOES NOT COMPILE.
        System.out.println("Bengal Tiger Constructor");
        System.out.println("Bengal Tiger super class: " + BengalTiger.class.getSuperclass().getName());
    }
}
