package inheritanceandconstructors;

/**
 * Created by rc00028 on 08/05/2017.
 */
public class Animal {

    private static int refNoSequence;
    private String name;
    private String type;
    private String uniqueReference;

    //Constructors
    public Animal(String name, String type) {
        System.out.println("Animal constructor");
        this.name = name;
        this.type = type;
        uniqueReference = "000" + ++refNoSequence;
        System.out.println("Animal super class: " + Animal.class.getSuperclass().getName());
    }

    //Methods
    public String getUniqueReference() {
        return uniqueReference;
    }

    public String getName() {
        return name;
    }

    //Overloaded method example
    public String getName(String prefix) {
        return prefix+name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        Animal otherAnimal = (Animal)obj;

        if (this.getType().equals(otherAnimal.getType())){
            return true;
        }
        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Goodbye World!");
    }
}

//Below demonstrates that multiple classes can be defined in a single file. But only one public class can be.
class Tester{

}

