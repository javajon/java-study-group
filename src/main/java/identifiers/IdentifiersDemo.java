package identifiers;

import zoo.animal.Animal;
import zoo.animal.Panther;
import zoo.animal.Zebra;

public class IdentifiersDemo {

    //Identifiers must start with a letter, $ or _  (£ can also be used)

    String my4String;
    String $dollar;
    String _underscore;
    String __doubleUnderscore;
    String £pound;

    //String 76DoesNotCompile;
    //String *DoesNotCompile;
    //String does@NotCompile;

    //Camel case is the accepted standard
    //Two animals called Laura.
    Animal laura = new Panther("laura");
    Animal lauraTheLeopard = new Zebra("laura");

    //Reserved words cannot be used as variable names
    //Do not compile
    //String case;
    //String class;

    //Allowed but not recommended.
    //String String;

    //Chapter 4 - the same rules are applied to method names.
    //public void walk1() { }
    //public void 2walk() { } // DOES NOT COMPILE
    //public walk3 void() { } // DOES NOT COMPILE
    //public void Walk_$() { }
    //public void() { } // DOES NOT COMPILE


}
