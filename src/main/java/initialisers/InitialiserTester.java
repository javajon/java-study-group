package initialisers;

/**
 * Created by rc00028 on 06/06/2017.
 */
public class InitialiserTester {

    private static final int NUM_SECONDS_PER_HOUR;
    private static int one;
    private static final int two;
    private static final int three = 3;
    private static final int four;     // DOES NOT COMPILE UNLESS INITIALISED HERE OR IN THE INITIALISER BLOCK

    static {
        System.out.println("This is a static initialiser!");
        int numSecondsPerMinute = 60;
        int numMinutesPerHour = 60;
        NUM_SECONDS_PER_HOUR = numSecondsPerMinute * numMinutesPerHour;
        //NUM_SECONDS_PER_HOUR = 5;
        one = 1;
        one = 2;
        one = 1;
        two = one;
        one = two;
        one = three;
        //three = 3; // DOES NOT COMPILE
        //two = 2; // DOES NOT COMPILE
        //Uncomment the line below to make above declaration compile
        four = 4;
    }

    //Fields
    String name;

    //Constructors
    public InitialiserTester() {
        System.out.println("Constructor!");
    }

    {
        System.out.println("This is instance initialiser 1!");
    }

    //Methods
    public String getName() {
        System.out.println("In getName() method!");
        return name;
    }

    {
        System.out.println("This is instance initialiser 2!");
    }

    public static void main(String[] args) {
        InitialiserTester initialiserTester = new InitialiserTester();
        initialiserTester.getName();

        InitialiserTester initialiserTester2 = new InitialiserTester();
        initialiserTester2.getName();
    }

}
