package initialisers;

/**
 * Created by rc00028 on 06/06/2017.
 */
public class SimpleInitialiserTester {


    static{
        System.out.println("In static Initialiser!");
    }
    static{
        System.out.println("In static Initialiser 2!");
    }
    //Fields
    String name;
    //static String presidentName = "Obama";

    //Constructors
    public SimpleInitialiserTester(String name) {
        System.out.println("Constructor!");
        this.name = name;
    }

    //Initialiser 1 - this will not compile if placed above name declaration and initialisation.

    {
        System.out.println("This is instance initialiser 1!. Name: " + name);
    }

    //Methods
    public String getName() {
        System.out.println("In getName() method!");
        return name;
    }

    //Initialiser 2 - this will not compile if placed above name declaration and initialisation.

    {
        System.out.println("This is instance initialiser 2!");
    }


    public static void main(String[] args) {
        System.out.println("In the main method!");
        //System.out.println("The name is: " + name);



        SimpleInitialiserTester initialiserTester = new SimpleInitialiserTester("Dave");
        initialiserTester.getName();

        SimpleInitialiserTester initialiserTester2 = new SimpleInitialiserTester("Sally");
        initialiserTester2.getName();
    }
    static{
        System.out.println("In static Initialiser3!");
    }

}
