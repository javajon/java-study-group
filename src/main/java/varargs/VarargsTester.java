package varargs;

import org.apache.log4j.Logger;
import zoo.animal.Zebra;

/**
 * Created by rc00028 on 16/06/2017.
 */
public class VarargsTester {

    final static Logger logger = Logger.getLogger(VarargsTester.class);

    public static void printArray(String[] stringArray) {
        for (String arg : stringArray) {
            logger.info(arg);
        }

        //JH - uncomment the lower section after initial explanation.
        //Interacting with elements in the array.

        if(stringArray.length > 0){
            logger.debug("stringVarargs[0]: " + stringArray[0]);
            stringArray[0] = "HelloWorld";
            logger.debug("stringVarargs[0]: " + stringArray[0]);
        }


    }

    public static void printVarargs(String... stringVarargs) {
        for (String arg : stringVarargs) {
            logger.info(arg);
        }

        //JH - uncomment the lower section after initial explanation.
        //varargs can be interacted with in the same way as an array. Varargs use array indexing.

        if(stringVarargs.length > 0){
            logger.debug("stringVarargs[0]: " + stringVarargs[0]);
            stringVarargs[0] = "HelloWorld";
            logger.debug("stringVarargs[0]: " + stringVarargs[0]);
        }

    }

    //It is valid to have varargs in the main method sig.
    //public static void main(String... args) {
    public static void main(String[] args) {
        String[] stringArray = new String[]{"Glenn", "Peter", "Richard"};

        //A standard Array can be sent to both methods.
        printArray(stringArray);
        printVarargs(stringArray);

        //You can't declare varargs as a type in a method. It is not allowed and does not make sense.
        //String... stringVarargs = "Nick, Andy, Adam"; //Does not compile.
        //Instead, they are passed in directly: -
        printVarargs();
        printVarargs(new String[] {"Nick"});
        printVarargs(new String[] {"Nick", "Andy", "Adam"});
        printVarargs(new String[] {"As many or as few arguments as you like", "Andy", "Rao", "Adam", "Persi", "Jyo", "Priya", "Giles", "Jon", "Vijaya", "Karolina", "Venu", "Stuart", "Jatinder"});

        printVarargs("Nick");
        printVarargs("Nick", "Andy", "Adam");
        printVarargs("As many or as few arguments as you like", "Andy", "Rao", "Adam", "Persi", "Jyo", "Priya", "Giles", "Jon", "Vijaya", "Karolina", "Venu", "Stuart", "Jatinder");

        printArray(new String[] {"Nick"});
        printArray(new String[] {"Nick", "Andy", "Adam"});
        printArray(new String[] {"As many or as few arguments as you like", "Andy", "Rao", "Adam", "Persi", "Jyo", "Priya", "Giles", "Jon", "Vijaya", "Karolina", "Venu", "Stuart", "Jatinder"});

      //A method with an array int he params is less flexible - you can't send just varargs
        //printArray("Nick");
        //printArray("Nick", "Andy", "Adam");
        //printArray("As many or as few arguments as you like", "Andy", "Rao", "Adam", "Persi", "Jyo", "Priya", "Giles", "Jon", "Vijaya", "Karolina", "Venu", "Stuart", "Jatinder");

        //These arguments cannot be sent to a method that requires a traditional array argument.
        //The below line will not compile
        //printArray("As many or as few arguments as you like", "Andy", "Adam", "Persi", "Jyo", "Priya", "Giles", "Jon", "Vijaya", "Karolina", "Venu", "Stuart", "Jatinder");

        walk(1); // 0
        walk(1, 2); // 1
        walk(1, 2, 3); // 2
        walk(1, new int[] {4, 5}); // 2

        //What will this do?
        walk(1, null); //JH

    }

    public static void walk(int start, int... nums) {
        logger.info(nums.length);
    }

    //If you have a varargs param, you can only have one per method and it must be the last argument in the list - even if other arguments are of a different type.
    //This is to avoid ambiguity as, by definition, varargs are a variable number of arguments. Even different types can be converted, so the law is universal.
    //Which of the following don't compile?
    public void walk1(int... nums) { }
    public void walk2(int start, int... nums) { }
    //public void walk3(int... nums, int start) { }
    //public void walk4(int... start, int... nums) { }
    //public void walk5(int... nums, String start) { }


}
