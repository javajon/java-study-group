package comments;

/**
 * This is a Javadoc comment and will appear in the official documentation of the class.
 *
 * Created by u.7829872 on 16/11/2017.
 */
public class Comments {

    //Properties - this is a single line comment. Usually a not or help text for other developers/testers
    String comment;


    //Constructors - this is another single line comment.
    /**
     * This is another  Javadoc comment
     *
     * No argument constructor for the class comments.Comments
     */
    public Comments(){
    }



    //Methods - this is another single line comment.
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    /*
    This is a block comment. Commonly used to "comment out" a section of code which may be required again later in the project.
    Code should ideally not go live with large block comments like this. It makes the code less readable and look less professional.
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    */
}
