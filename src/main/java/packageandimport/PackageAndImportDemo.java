package packageandimport;

import java.util.Date;
import java.util.Random;

/**
 * Created by u.7829872 on 16/11/2017.
 */
public class PackageAndImportDemo {

    public static void main(String[] args) {

        Random r = new Random();

        //JH - import java.util.Random
        System.out.println("Random   number between 1 and 10:");
        System.out.println(r.nextInt(10));

        //JH - zoo.Zoo class - demo implicit imports.

        //JH - import java.util.Date, then show wildcard.

        Date myDate = new Date();

        //JH - show clash with java.sql.Date.

        java.sql.Date sqlDate = new java.sql.Date(199191919);

        //JH - show redundant imports.
        //import java.lang.System;
//import java.lang.String;


    }

}
