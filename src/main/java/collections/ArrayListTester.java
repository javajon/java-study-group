package collections;

import org.apache.log4j.Logger;
import zoo.*;
import zoo.animal.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * ArrayListTester
 *
 * Created by jon on 07/06/2017.
 *
 * This class demonstrates java.util.ArrayList - one of Java's key Collections classes.
 *
 * An ArrayList may be typed using angled brackets: ArrayList<String>
 *
 * Methods included in the OCA exam. The numbering is used int he body of the class: -
 *
 * 1. add(Object o);
 * 2. add(int i, Object o);
 * 3. remove(Object o)
 * 4. remove(int i)
 * 5. toArray()
 * 6. set(int i, Object o)
 * 7. isEmpty()
 * 8. clear()
 * 9. contains()
 *10. equals()
 *11. size()
 *
 * The classes java.util.Arrays and java.util.Collections are useful utility classes for Arrays and Collections.
 * The following methods from these two classes are demonstrated here: -
 * Arrays.asList
 * Collections.sort(numbers);
 *
 */
public class ArrayListTester {

    final static Logger logger = Logger.getLogger(ArrayListTester.class);

    public static void main(String[] args) {

        //Creating a new ArrayList. Three constructors to show
        ArrayList list1 = new ArrayList();
        ArrayList list2 = new ArrayList(10);
        ArrayList list3 = new ArrayList(list2);

        //Creating a new ArrayList with a type using diamond
        //These diamond brackets were introduced for Java 5
        ArrayList<String> list4 = new ArrayList<String>();
        ArrayList<String> list5 = new ArrayList();

        //List is an interface - covered in a later chapter
        List<String> list6 = new ArrayList();
        //ArrayList<String> list7 = new List<>(); // DOES NOT COMPILE

        Animal tiger = new Tiger("Timmy");
        Animal tiger2 = new Tiger("Tammy");
        Animal lion = new Lion("Leonard");
        Animal lion2 = new Lion("Lax");

        //ArrayList typed as Animal.
        List<Animal> animalList = new ArrayList();
        //Usage of methods will be numbered.
        //1. add(Object o)
        animalList.add(tiger);
        animalList.add(tiger2);
        //2. add(int i, Object o)
        animalList.add(1, lion);

        logger.info("animalList contents after using add() methods");
        for (Animal animal : animalList) {
            logger.info(animal.getName() + ", " + animal.getType());
        }

        //3. remove(Object o)
        animalList.remove(tiger);
        logger.info("\nanimalList after executing remove(tiger)");
        for (Animal animal : animalList) {
            logger.info(animal.getName() + ", " + animal.getType());
        }

        animalList.add(tiger);
        logger.info("\nanimalList after adding tiger back to animalList");
        for (Animal animal : animalList) {
            logger.info(animal.getName() + ", " + animal.getType());
        }

        //4. remove(int i)
        animalList.remove(1);
        logger.info("\nanimalList after executing remove(1)");
        for (Animal animal : animalList) {
            logger.info(animal.getName() + ", " + animal.getType());
        }

        //It is not necessary to type the List, but if this is not done, it has implications as shown below.
        List anotherAnimalList = new ArrayList();
        anotherAnimalList.add(tiger);
        anotherAnimalList.add(tiger2);

        //As the type defaults to Object, the below is quite possible.
        //Uncomment to demonstrate this then re-comment
        //anotherAnimalList.add(new String("Hello"));

        Zoo zoo = new Zoo("First zoo", animalList);
        Zoo zoo2 = new Zoo("Second zoo", anotherAnimalList); // this is allowed, but is susceptible to casting problems because of the String.

        //Can't do this as the List is not typed to Animal and so defaults to Object.
        //for(Animal animal : anotherAnimalList){
        logger.debug("\nanotherAnimalList contents after using add() methods");
        for (Object object : anotherAnimalList) {
            //Instead we have to explicitly cast, which has risks
            Animal animal = (Animal) object;
            logger.info(animal.getName() + " " + animal.getType());
        }

        //Add some extra animals to the list for further testing.
        animalList.add(lion);
        animalList.add(lion2);
        animalList.add(new Antelope("Dave"));

        //Converting from List to Array: -
        //5. toArray()
        Animal[] animalListArray = animalList.toArray(new Animal[5]);
        logger.info("\nanimalListArray.length: " + animalListArray.length);

        //Converting from Array to List (the above in reverse): -
        List<Animal> animalListFromArray = Arrays.asList(animalListArray);
        logger.info("\nanimalListFromArray contents having converted from List to Array (toArray()) and back to List (asList())");
        for (Animal animal : animalListFromArray) {
            logger.info(animal.getName() + " " + animal.getType());
        }

        //6. set(int i, Object o)
        //The following examples swap the positions of Timmy Tiger with Lax Lion
        animalListFromArray.set(1, lion2);
        logger.info("\nanimalListFromArray after executing set(1, lion2)");
        for (Animal animal : animalListFromArray) {
            logger.info(animal.getName() + " " + animal.getType());
        }

        animalListFromArray.set(3, tiger);
        logger.info("\nanimalListFromArray after executing set(3, tiger)");
        for (Animal animal : animalListFromArray) {
            logger.info(animal.getName() + " " + animal.getType());
        }

        //7. isEmpty()
        logger.info("\nisEmpty() method. Is animalListFromArray empty?");
        logger.info(animalListFromArray.isEmpty());
        //NOTE: isEmpty() can also be used in Zoo.java in the guard condition. Show this after demonstrating size()

        //8. clear()
       // animalList.clear();
        logger.info("\nanimalListFromArray after executing clear()");
        for (Animal animal : animalList) {
            logger.info(animal.getName() + " " + animal.getType());
        }

        //9. contains()
        Chimpanzee chimpanzee = new Chimpanzee("Charlie");
        logger.info("\ncontains(tiger): " + animalList.contains(tiger));
        logger.info("contains(chimpanzee) before: " + animalList.contains(chimpanzee));
        animalList.add(chimpanzee);
        logger.info("contains(chimpanzee) after: " + animalList.contains(chimpanzee));

        //10. equals() method. Also compare with equality operator ==
        List animalListOne = new ArrayList();
        animalListOne.add(tiger);
        animalListOne.add(tiger2);

        List animalListTwo = new ArrayList();
        animalListTwo.add(tiger);
        animalListTwo.add(tiger2);

        logger.info("\nequals() method where both Lists contain the same elements");
        logger.info(animalListOne.equals(animalListTwo));

        //also test object equality - again, know the difference between equals and ==.
        logger.info("\nanimalListOne == animalListTwo where both Lists contain the same elements");
        logger.info(animalListOne == animalListTwo);

        //add an element so the lists now do not contain exacly the same elements.
        animalListTwo.add(lion2);
        logger.info("\nequals() method where the Lists do not contain the same elements");
        logger.info(animalListOne.equals(animalListTwo));

        //also test object equality - again, know the difference between equals and ==.
        logger.info("\nanimalListOne == animalListTwo where the Lists do not contain the same elements");
        logger.info(animalListOne == animalListTwo);

        //11. size()
        //Lists have a size() method. NOTE: This is different from Arrays - they have a length property.
        logger.info("\nanimalListFromArray.size(): " + animalListFromArray.size());
        //NOTE: size() can also be used in Zoo.java in the guard condition. Show this at this point

        //Lists cannot be typed as primitives: -
        //List<int> numbers1 = new ArrayList<int>(); //Does not compile

        //Lists cannot hold primitives, even when typed generically: -
        List numbers2 = new ArrayList();
        int twoAsInt = 2;
        //Does the following compile?
        numbers2.add(3);

        Integer three = new Integer(3);
        numbers2.add(three);
        numbers2.add(new Integer(4));

        //But primitive types can be introduced in wrappers. In this case it is implicitly converted.
        //Wrapper classes are dealt with in detail later.
        //Sorting an ArrayList is very similar to sorting an array. You just use a different helper class:
        List<Integer> numbers = new ArrayList();
        numbers.add(99);
        numbers.add(5);
        numbers.add(81);

        Collections.sort(numbers);
        logger.info("\nSorted numbers using Collections.sort(): " + numbers); //[5, 81, 99]

        //Now look at the new refactored zoo to see ArrayList in action. Start in ZooTester.java
    }
}
