package polymorhphismandcasting;


import zoo.Dangerous;
import zoo.animal.Animal;
import zoo.animal.Chick;
import zoo.animal.Lion;
import zoo.animal.Tiger;

/**
 * Created by rc00028 on 03/07/2017.
 * <p>
 * Java objects can be dealt with as different object types depending on their inheritance tree and implemented interfaces.
 * Methods can only be called if they are relevant to that type, as demonstrated below.
 * <p>
 * Once the object has been assigned a new reference type, only the methods and variables available to that reference
 * type are callable on the object without an explicit cast.
 * <p>
 * Object vs. Reference
 * animal, tiger and dangerous each point to a Tiger object created on the heap. But their ability to access methods depends on
 * their individual types.
 * <p>
 * We can summarize this principle with the following two rules:
 * 1. The type of the object determines which properties exist within the object in memory.
 * 2. The type of the reference to the object determines which methods and variables are accessible to the Java program.
 * <p>
 * While interfaces don't themselves extend Object, as it is known that any implementation will, the the Object methods are available through an interface reference.
 * <p>
 * Explicit casting can change the reference types and hence the possible methods accessed.
 *
 * Virtual Methods
 * All non-final, nonstatic, and non-private Java methods are considered virtual methods, since any of them can be overridden at runtime.
 *
 */
public class PolymorhismDemo {
    Tiger tiger = new Tiger("Toby");
    Animal animal = new Tiger("Terry");
    Dangerous dangerous = new Tiger("Tammy");

    Animal animal1 = tiger;
    Dangerous dangerous1 = tiger;
    Object object = tiger;

    public void testPolymorphismRules() {

        //Tiger reference can access methods on Tiger, Animal, Object and Dangerous
        tiger.animalMethod(); //Method in Animal class
        tiger.getName(); //Method in Animal class
        tiger.getType(); //Method in Animal class
        tiger.tigerMethod(); //Method in Tiger class
        tiger.howToHandle(); //Method in Dangerous Interface
        tiger.toString(); //Method in Object class

        //Tiger reference can access methods on Animal
        animal.animalMethod(); //Method in Animal class
        animal.getName(); //Method in Animal class
        animal.getType(); //Method in Animal class
        //Does not compile because tigerMethod is defined in Tiger class.
        //animal.tigerMethod(); //Method in Tiger class
        //Does not compile because howToHandle is defined in Dangerous interface and not implemented in Animal class.
        //animal.howToHandle(); //Method in Dangerous Interface
        animal.toString(); //Method in Object class

        //Does not compile because animalMethod is defined in Tiger class.
        //dangerous.animalMethod(); //Method in Animal class
        //Does not compile because getName is defined in Tiger class.
        //dangerous.getName(); //Method in Animal class
        //Does not compile because getType is defined in Tiger class.
        //dangerous.getType(); //Method in Animal class
        //Does not compile because getGender is defined in Tiger class.
        //dangerous.getGender(); //Method in Animal class
        //Does not compile because tigerMethod is defined in Tiger class.
        //dangerous.tigerMethod(); //Method in Tiger class
        dangerous.howToHandle(); //Method in Dangerous Interface
        //Interfaces have access to methods on the Object class
        dangerous.toString(); //Method in Object class



        //In the above example, all three concrete objects on the stack are actually Tiger objects
        //but they can only be coded to use the methods of the reference type they are declared as.
        //Explicit cast can overcome this limitation: -
        System.out.println("dangerous.getName(): " + ((Tiger)dangerous).getName());

        //Without an explicit cast, this does not compile: -
        //System.out.println("dangerous.getName(): " + dangerous.getName());

        //Note the level of bracketing required. This won't compile: -
        //System.out.println("dangerous.getName(): " + (Tiger)dangerous.getName());

        //Casting is prone to error though: -
        Animal animal2 = new Lion("Leia");

        //Compiles, but throws an Exception at runtime.
        //Tiger tiger3 = (Tiger)animal2;

        //In the other direction, casting is implicit: -
        Chick chick = new Chick("Charlotte");
        Animal chickAsAnimal = chick;

        if(animal2 instanceof Tiger){
            Tiger tiger3 = (Tiger)animal2;
            //tiger.getClass();

        }

        //Not on OCA exam, but bear in mind that instanceof could be used to test this before the explicit cast.


        //JH - demo instanceof

        //JH - demo below, then override the virtualDemoMethod method in Tiger and re-run.
        //This demonstrates that all non-final, non-static, and non-private Java methods are considered virtual methods,
        // since any of them can be overridden at runtime.
//        System.out.println(animal.virtualDemoMethod());
  //      System.out.println(tiger.virtualDemoMethod());

        //This is from page 285 and relates to the above demo: -
        //You now know the true purpose of overriding a method and how it relates to polymorphism.
        // The nature of the polymorphism is that an object can take on many different forms.
        // By combining your understanding of polymorphism with method overriding,
        // you see that objects may be interpreted in vastly different ways at runtime,
        // especially in methods defined in the superclasses of the objects.

    }

    public static void main(String[] args) {
        PolymorhismDemo demo = new PolymorhismDemo();
        demo.testPolymorphismRules();
    }
}
