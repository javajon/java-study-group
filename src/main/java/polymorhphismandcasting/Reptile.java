package polymorhphismandcasting;

import zoo.animal.Animal;

import java.util.List;

/**
 * Created by rc00028 on 07/07/2017.
 *
 * Polymorphic Parameters
 *
 * One of the most useful applications of polymorphism is the ability to pass instances
 * of a subclass or interface to a method. For example, you can define a method that takes
 * an instance of an interface as a parameter. In this manner, any class that implements
 * the interface can be passed to the method.
 *
 * Since you’re casting from a subtype to a supertype, an explicit cast is not required.
 * This property is referred to as polymorphic parameters of a method, as demonstrated here.
 */
public class Reptile {
    public String getName() {
        return "Reptile";
    }
}

class Alligator extends Reptile {
    public String getName() {
        return "Alligator";
    }
}

class Crocodile extends Reptile {
    public String getName() {
        return "Crocodile";
    }
}

class ZooWorker {
    public static void feed(Reptile reptile) {
        System.out.println("Feeding reptile " + reptile.getName());
    }

    public static void main(String[] args) {
        feed(new Alligator());
        feed(new Crocodile());
        feed(new Reptile());
    }

    //This demonstrates
    public static void animalListSorter(List<Animal> animalList) {
        for (Animal animal : animalList){
            System.out.println(animal.getName());
        }
    }
}