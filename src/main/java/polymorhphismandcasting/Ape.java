package polymorhphismandcasting;


import exception.InvalidAnimalDataException;
import inheritanceandconstructors.BengalTiger;

/**
 * Created by rc00028 on 07/07/2017.
 * <p>
 * Polymorphism and Method Overriding
 * <p>
 * This example creates ambiguities in the ZooKeeper class.
 * <p>
 * 1. Assuming that getName was allowed to be marked as protected in Gorilla.
 * The reference animal.getName() is allowed because the method is public in the Animal class,
 * but due to polymorphism, the Gorilla object itself has been overridden with a less accessible version, not available to the ZooKeeper class.
 * This creates a contradiction in that the compiler should not allow access to this method, but because it is being referenced as an instance
 * of Animal, it is allowed. Therefore, Java eliminates this contradiction, thus disallowing a method from being overridden by a less
 * accessible version of the method.
 * <p>
 * 2. Likewise, a subclass cannot declare an overridden method with a new or broader exception than in the superclass, since the method may be
 * accessed using a reference to the superclass. For example, if an instance of the subclass is passed to a method using a superclass reference,
 * then the enclosing method would not know about any new checked exceptions that exist on methods for this object, potentially leading to compiled
 * code with “unchecked” checked exceptions. Therefore, the Java compiler disallows overriding methods with new or broader exceptions.
 * <p>
 * 3. Finally, overridden methods must use covariant return types for the same kinds of reasons as just discussed. If an object is cast to a superclass
 * reference and the overridden method is called, the return type must be compatible with the return type of the parent method. If the return type
 * in the child is too broad, it will result an inherent cast exception when accessed through the superclass reference.
 */
public class Ape {
    public BengalTiger getBengalTiger() throws InvalidAnimalDataException {
        BengalTiger bengalTiger = new BengalTiger("Terry", "BengalTiger");
        return bengalTiger;
    }
}

class Gorilla extends Ape {
    //protected BengalTiger getBengalTiger() {  // DOES NOT COMPILE (relates to 1 above).
    //public BengalTiger getBengalTiger() throws InvalidAnimalException {  // DOES NOT COMPILE (relates to 2 above).
    //public Tiger getBengalTiger() {  // DOES NOT COMPILE (relates to 3 above)
    public BengalTiger getBengalTiger() {
        BengalTiger bengalTiger = new BengalTiger("Thia", "BengalTiger");
        return bengalTiger;
    }
}

class ZooKeeper {
    public static void main(String[] args) {
        Ape ape = new Gorilla();
        try {
            System.out.println(ape.getBengalTiger().getName());
        } catch (InvalidAnimalDataException e) {
            e.printStackTrace();
        }
    }
}