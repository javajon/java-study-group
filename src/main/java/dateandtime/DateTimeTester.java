package dateandtime;

import org.apache.log4j.Logger;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static staticFields.StaticAndInstanceVariableTester.START_DATE_OF_FRENCH_REVOLUTION;

/**
 *
 * DateTimeTester
 *
 * Created by jon on 07/06/2017.
 *
 * This class demonstrates working with dates and times using the java.time package.
 * This package was introduced in Java 1.8. to replace java.util.Calendar.
 * The three main classes to look at are: -
 *
 * LocalDate
 * LocalTime
 * LocalDateTime
 *
 * Each of these has lots of different overloaded static 'of' methods to create an instance
 *
 * There are also lots of 'plus' and 'minus'methods with which to change date time values e.g. plusDays().
 *
 * Period (for dates) and Duration (for time) classes represent blocks of date/time.
 */

public class DateTimeTester {

    final static Logger logger = Logger.getLogger(DateTimeTester.class);

    public static void main(String[] args) {

        //Until Java 8, Calendar was the preferred way of creating and manipulating Dates: -
        Calendar cal = Calendar.getInstance();

        Date today = cal.getTime();
        logger.info("Now - from Calendar class: " + today);

        //Before that, Dates were constructed directly
        //NOTE: This is now deprecated.
        Date date = new Date(2017, 11, 1);

        //There are three local classes for date and time.
        LocalDate localDateNow = LocalDate.now();
        logger.info("\nNow - from LocalDate class: " + localDateNow);

        LocalTime localTimeNow = LocalTime.now();
        logger.info("Now - from LocalTime class: " + localTimeNow);

        LocalDateTime localDateTimeNow = LocalDateTime.now();
        logger.info("Now - from LocalDateTime class: " + localDateTimeNow);

        //Creating dates and times using a set of 'of' methods
        //Month is an enum
        //Note that there are lots of overloaded of methods to make it as intuitive and flexible as possibe
        LocalDate date1 = LocalDate.of(2015, Month.JANUARY, 20);
        LocalDate date2 = LocalDate.of(2015, 1, 20);
        logger.info("\ndate1: " + date1);
        logger.info("date2: " + date2);

        //Creating times using of
        LocalTime time1 =  LocalTime.of(6, 15); // hour and minute
        LocalTime time2 =  LocalTime.of(6, 15, 30); // + seconds
        LocalTime time3 =  LocalTime.of(6, 15, 30, 200); // + nanoseconds
        logger.info("\ntime1: " + time1);
        logger.info("time2: " + time2);
        logger.info("time3: " + time3);

        //Creating date time using of
        LocalDateTime dateTime1 = LocalDateTime.of(2015, Month.JANUARY, 20, 6, 15, 30);
        logger.info("\ndateTime1: " + dateTime1);

        //Creating date time by combining date and time objects
        LocalDateTime dateTime2 = LocalDateTime.of(date1, time1);
        logger.info("dateTime2: " + dateTime2);

        //LocalDate, LocalTime and LocalDateTime have lots of different overloaded of methods
        //Static methods are used to create Time and Date objects, not constructors. Why? Unecessary object creation.
        //This is also true of the old Calendar object.
        //LocalDate d = new LocalDate(); // DOES NOT COMPILE

        //The java.time classes are quite 'intelligent' and do a lot of the tricky date time manipulation for you.
        //Invalid day of the months. The following line would throw a DateTimeException
        //LocalDate invalidDate = LocalDate.of(2015, Month.JANUARY, 32);     // throws DateTimeException
        //logger.info("\ninvalidDate: " + invalidDate);

        //Manipulating dates
        //There are also lots of plus and minus methods with which to change date time values. The are instance methods, not static.
        //Note that, like String, Dates are immutable, so we have to remember to assign them so as not to lose the particular manipulation we are doing.
        //JH - try some of these with and without variable assignment to show immutability.
        //Note the use of \t as tab
        LocalDate startDate = LocalDate.of(2014, Month.JANUARY, 20);
        logger.info("\nstartDate (expect 20/1/2014):\t" + startDate); // 2014-01-20

        startDate = startDate.plusDays(2);
        logger.info("startDate.plusDays(2):\t\t\t" + startDate); // 2014-01-22

        //Contrast to Calendar class example: -
        Calendar c = new GregorianCalendar(2014, Calendar.JANUARY, 20);
        c.add(Calendar.DAY_OF_MONTH, 2);
        logger.info("Calendar equivalent:\t\t\t" + startDate); // 2014-01-22

        startDate = startDate.plusWeeks(1);
        logger.info("startDate.plusWeeks(1):\t\t\t" + startDate); // 2014-01-29

        //Java is able to determine whether a year is a leap year or not - 2014 is not.
        startDate = startDate.plusMonths(1);
        logger.info("startDate.plusMonths(1):\t\t" + startDate); // 2014-02-28

        startDate = startDate.plusYears(5);
        logger.info("startDate.plusYears(5):\t\t\t" + startDate); // 2019-02-28

        LocalDate leapDate = LocalDate.of(2016, Month.MARCH, 31);
        logger.info("\nleapDate (expect 31/3/2016):\t" + leapDate); // 2016-03-31

        leapDate = leapDate.minusMonths(1);
        //The java.time api includes leap year logic as shown here:
        logger.info("leapDate.minusMonths(1):\t\t" + leapDate); // 2016-02-29:

        leapDate = leapDate.plusMonths(1);
        logger.info("leapDate.plusMonths(1):\t\t\t" + leapDate); // 2016-03-29:

        leapDate = leapDate.plusDays(1);
        logger.info("leapDate.plusDays(1):\t\t\t" + leapDate); // 2016-03-30:

        leapDate = leapDate.minusMonths(1);
        logger.info("leapDate.minusMonths(1):\t\t" + leapDate); // 2016-02-29:

        //Similar manipulations in DateTime
        LocalDate date5 = LocalDate.of(2020, Month.JANUARY, 20);
        LocalTime time5 = LocalTime.of(5, 15);

        //Constructing a LocalDateTime from a LocalDate and a LocalTime
        LocalDateTime localDateTime = LocalDateTime.of(date5, time5);
        logger.info("\nlocalDateTime (expect 20/01/2020 at 05:15):\t" + localDateTime); // 2020-01-20T05:15

        localDateTime = localDateTime.minusDays(1);
        logger.info("localDateTime.minusDays(1):\t\t\t\t\t" + localDateTime); // 2020-01-19T05:15

        localDateTime = localDateTime.minusHours(10);
        logger.info("localDateTime.minusHours(10):\t\t\t\t" + localDateTime); // 2020-01-18T19:15

        localDateTime = localDateTime.minusSeconds(30);
        logger.info("localDateTime.minusSeconds(30):\t\t\t\t" + localDateTime); // 2020-01-18T19:14:30

        //Lots of plus and minus methods are available.
        //These methods can be chained. The below achieves the same as the localDateTime example, but with chained methods: -
        LocalDate date6 = LocalDate.of(2020, Month.JANUARY, 20);
        LocalTime time6 = LocalTime.of(5, 15);
        LocalDateTime chainedDT = LocalDateTime.of(date6, time6).minusDays(1).minusHours(10).minusSeconds(30);
        logger.info("\nchainedDT (expect 18/01/2020 at 19:14:30):\t" + chainedDT); // 2020-01-18T19:14:30

        //Remember that Dates are immutable, so this will print the original Date without the manipulation.
        LocalDate date7 = LocalDate.of(2020, Month.JANUARY, 20);
        date7 = date7.plusDays(10);
        logger.info("\ndate7 (unassigned manipulation): " + date7); //2020-01-20 - The original Date before manipulation.

        LocalDate date8 = LocalDate.of(2020, Month.JANUARY, 20);
        //Be sure not to use the Date and Time methods on the wrong object type: -
        //date = date8.plusMinutes(1);     // DOES NOT COMPILE

        //Dates can be used in conditionals and loops: -
        LocalDate start = LocalDate.of(2015, Month.JANUARY, 1);
        LocalDate end = LocalDate.of(2015, Month.MARCH, 30);

        //isBefore() method
        logger.info("\nUsage of isBefore() in a loop: -");
        while (start.isBefore(end)) {
            logger.info(start + " is before " + end);
            start = start.plusMonths(1);
        }

        logger.info(start + " is after " + end);

        //The above example can also be used with Period.
        start = LocalDate.of(2015, Month.JANUARY, 1);
        end = LocalDate.of(2015, Month.MARCH, 30);

        //Introduce the Period class
        Period period = Period.ofMonths(1);

        logger.info("\nUsage of Period class and isBefore() in a loop: -");
        while (start.isBefore(end)) {
            logger.info(start + " is before " + end);

            //Note: The method here is plus, not plusDays, plusMonth or whatever.
            start = start.plus(period);
        }

        logger.info(start + " is after " + end);

        //Other Period examples: -
        Period annually = Period.ofYears(1); // every 1 year
        Period quarterly = Period.ofMonths(3); // every 3 months
        Period everyThreeWeeks = Period.ofWeeks(3); // every 3 weeks
        Period everyOtherDay = Period.ofDays(2); // every 2 days
        Period everyYearAndAWeek = Period.of(1, 0, 7); // every year and 7 days

        logger.info("\nUsage of the Period class");
        logger.info("Period.ofYears(1):\t" + annually);
        logger.info("Period.ofMonths(3):\t" + quarterly);
        logger.info("Period.ofWeeks(3):\t" + everyThreeWeeks);
        logger.info("Period.ofDays(2):\t" + everyOtherDay);
        logger.info("Period.of(1, 0, 7):\t" + everyYearAndAWeek);

        //Period methods do not work predictably chained. So this does not work: -
        Period wrong = Period.ofYears(1).ofWeeks(1);          // every week - only the last method is effective.
        logger.info("\nPeriod.ofYears(1).ofWeeks(1) (does not work predicatably): " + wrong); //P7D

        //Duration is similar to Period but works on Times
        Duration hourly = Duration.ofMinutes(60); // every hour
        logger.info("\nUsage of the Duration class");
        logger.info("Duration.ofMinutes(60):\t" + hourly);
        logger.info("hourly.plusMinutes(5):\t" + hourly.plusMinutes(5));

        //Make sure you use the right type of Period with the right Date class. The lowest example here compiles but throws a Runtime Exception: -
        LocalDate date9 = LocalDate.of(2015, 1, 20);
        LocalTime time9 = LocalTime.of(6, 15);
        LocalDateTime dateTime9 = LocalDateTime.of(date9, time9);
        Period period9 = Period.ofMonths(1);

        date9 = date9.plus(period9);
        logger.info("\nAdding month Period to LocalDate: " + date9); // 2015-02-20

        dateTime9 = dateTime9.plus(period9);
        logger.info("Adding month Period to LocalDateTime: " + dateTime9); // 2015-02-20T06
        //time9 = time9.plus(period9); // UnsupportedTemporalTypeExceptio

        //Formatting
        //Calling format on the date/time object.
        LocalDate date10 = LocalDate.of(2020, Month.JANUARY, 20);
        LocalTime time10 = LocalTime.of(11, 12, 34);
        LocalDateTime dateTime10 = LocalDateTime.of(date10, time10);

        logger.info("\nUnformatted examples: ");
        logger.info(date10);
        logger.info(time10);
        logger.info(dateTime10);
        logger.info("\nExamples formatted with DateTimeFormatter.ISO_LOCAL_DATE: ");
        logger.info(date10.format(DateTimeFormatter.ISO_LOCAL_DATE));
        logger.info(time10.format(DateTimeFormatter.ISO_LOCAL_TIME));
        logger.info(dateTime10.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

        //Calling format on the DateTimeFormatter.
        logger.debug("\nExamples formatted with FormatStyle.SHORT: ");
        DateTimeFormatter shortDateTime =  DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
        logger.info(shortDateTime.format(dateTime10)); // 1/20/20
        logger.info(shortDateTime.format(date10)); // 1/20/20
        //logger.debug(shortDateTime.format(time10)); // UnsupportedTemporalTypeException

        //Custom DateTimeFormatter.
        logger.info("\nExamples formatted with a custom formatter: ");

        //DateTimeFormatter i sused for Dates Time and DateTimes, but UnsupportedTemporalTypeException may result if the wrong fields are used depending on the type.
        //LocalDateTime example
        DateTimeFormatter myPersonalDateTimeFormatter = DateTimeFormatter.ofPattern("MMMM dd, yyyy, hh:mm");
        logger.info(myPersonalDateTimeFormatter.format(dateTime10));     // January 20, 2020, 11:12
        //logger.debug(myPersonalDateTimeFormatter.format(date10));     // UnsupportedTemporalTypeException
        //logger.debug(myPersonalDateTimeFormatter.format(time10));     // UnsupportedTemporalTypeException

        //LocalDate example
        DateTimeFormatter myPersonalDateFormatter = DateTimeFormatter.ofPattern("MMMM dd, yyyy");
        logger.info(myPersonalDateFormatter.format(date10));     // January 20, 2020

        //LocalTime example
        DateTimeFormatter myPersonalTimeFormatter = DateTimeFormatter.ofPattern("hh:mm");
        logger.info(myPersonalTimeFormatter.format(time10));     // 11:12

        //This example is used with StaticAndInstanceVariableTester to demonstrate the usage of static final variables.
        //Without formatting
        logger.info("\nThe French Revolution started on " + START_DATE_OF_FRENCH_REVOLUTION);
        //With formatting
        logger.info("\nThe French Revolution started on " + myPersonalDateFormatter.format(START_DATE_OF_FRENCH_REVOLUTION));

    }
}
