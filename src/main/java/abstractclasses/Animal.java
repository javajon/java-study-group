package abstractclasses;

public abstract class Animal {
    String name;
    String type;

    public Animal(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public abstract void eat();

}
