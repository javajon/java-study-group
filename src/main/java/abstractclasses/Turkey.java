package abstractclasses;

/**
 * Turkey - concrete implementation of an abstract class.
 *
 * Comment out the class body to show that a class that extends an abstract class must either: -
 * 1. Declare itself abstract.
 * or
 * 2. Implement all abstract methods.
 *
 * Created by rc00028 on 28/06/2017.
 */
public class Turkey extends Animal {

    public Turkey(String name, String type) {
        super(name, type);
    }

    @Override
    public void eat() {
        System.out.println("I'm more concerned about what eats ME!");
    }
}
