package abstractclasses;


/**
 * Turtle - example of an abstract class.
 *
 * Abstract Class Deﬁnition Rules:
 * 1. Abstract classes cannot be instantiated directly.
 * 2. Abstract classes may be defined with any number, including zero, of abstract and non-abstract methods.
 * 3. Abstract classes may not be marked as private or final.
 * 4. An abstract class that extends another abstract class inherits all of its abstract methods as its own abstract methods.
 * 5. The first concrete class that extends an abstract class must provide an implementation for all of the inherited abstract methods
 * 6. If a class has any abstract methods, it must be declared abstract.
 *
 * Abstract Method Deﬁnition Rules:
 * 1. Abstract methods may only be defined in abstract classes (same as 6 above).
 * 2. Abstract methods may not be declared private or final.
 * 3. Abstract methods must not provide a method body/implementation in the abstract class for which is it declared.
 * 4. Implementing an abstract method in a subclass follows the same rules for overriding a method.
 *    For example, the name and signature must be the same, and the visibility of the method in the subclass must
 *    be at least as accessible as the method in the parent class.
 *
 * Created by rc00028 on 28/06/2017.
 *
 */
public abstract class Turtle {

    //LIKELY TO BE ON EXAM - The following would not compile if the class has abstract methods.
    //public class Turtle {

    //final and abstract cannot be used together in class declaration.
    //public final abstract class Turtle {  // DOES NOT COMPILE }

    //LIKELY TO BE ON EXAM - Abstract method must define no body and hence no brackets {}.
    //public abstract void swim() {}  // DOES NOT COMPILE

    //final and abstract cannot be used together in method signature.
    //public abstract final void chew();  // DOES NOT COMPILE

    //private and abstract cannot be used together in method signature.
    public abstract void sing();  // DOES NOT COMPILE

    /*
    public abstract int getAge() {  // DOES NOT COMPILE
        return 10;
    }
    */

    public abstract void swim();

    public abstract int getAge();

    //It's fine to have non abstract methods in an abstract class.
    public String returnHelloWorld(){
        return "HelloWorld!";
    }

    public static void main(String[] args) {
        //As Turtle is abstract, you cannot create an instance of an abstract class in the normal way and the following line fails.
        //Turtle turtle = new Turtle();

        //Note that a variable type CAN be an abstract class.
        Animal animal = new Turkey("Turkey", "Turkey");
    }

}