/**
 * MethodStructureDemo
 *
 * Created by jon on 21/06/2017.
 *
 * This test class demonstrates the components of a method.
 *
 * These include: -
 *
 * 1. access modifier - required (can be public, default, protected, private - covered later)
 * 2. optional specifier - optional, not all can be used together (static, abstract, final and not on OCA exam: synchronized, native, strictfp)
 * 3. return type - required (can be primitive, object or void)
 * 4. method name - required (must conform to naming standards)
 * 5. parentheses - required
 * 6. list of parameters - optional (must be comma separated in the parentheses listed above)
 * 7. exception - optional (must be in the format "throws Exception"
 * 8. method body - required must be contained within {}
 */
package methodstructure;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class MethodStructureDemo {

    //The standard main method demonstrates 1, 2, 3, 4, 5, 6 and 8 of the elements listed above.
    public static void main(String[] args) throws Exception{
        System.out.println("Hello and welcome to MethodStructureDemo!");

        //readFile("/home/jon/i_exist.txt");
        readFile("/home/jon/not_exist.txt");

        return;

    }

    //Access Modifiers
    //default access modifier does not have a word.
    //All fine in main, but only public makes it the main method.

    //This is valid and shows that only 1,3,4,5 and 8 from above are required.
    void walk(){

    }

    //DOES NOT COMPILE - order is important.
    //void public race(){
    //}

    //The 8 parts should be in the order listed above - except modifiers can be mixed nd matched.

    //DOES NOT COMPILE - default is a keyword used elsewhere in Java e.g. case statements.
    //default void crawl(){
    //This works and is as minimal a method as you can get! It still contains 1,3,4,5 and 8, but 1 is implied.
    void crawl(){
    }

    //Optional Specifiers and Return Type
    //final - covered in Chapter 5. This method cannot be overridden.
    //This also shows that when your return type is not void, you must return a compatible primitive/object.
    public static final long giveMeFive(){
        return 5;

        ///JH - comment the above line, uncomment below and explain why this fails compilation
        // Does the reverse situation compile?
        //return 5L;

    }

    //Does this compile? Several examples shown in the bok. This is probably the most tricky.

    String walk6 (int a){
        if (a == 4){
            return "I walked 4 miles today";
        }

        return "I didn't walk 4 miles today";
    }


    //In methods that have a void return type, nothing must be returned, but optionally, the keyword return on its own may be used.
    //JH - show this above in main method.

    //abstract - see AbstractAnimal class. Cannot have abstract methods here as the class is not abstract.
    //public abstract void eat(); //DOES NOT COMPILE

    //NOTE: The modifiers abstract and final cannot be used in the same method signature together.
    //They are contradictory. JH - explain why.
    //public final abstract void eatSomeMore(); //DOES NOT COMPILE

    //Try to see which of the following compile.
    public void walk1() {}
    public final void walk2() {}
    public static final void walk3() {}
    public final static void walk4() {}
    //public modifier void walk5() {}
    //public void final walk6() {}
    public final static void walk7() {}

    //Method names
    //See IdentifiersDemo, copied from identifiers project.

    //Parameters List
    //Try to see which of the following compile.
    public void walk51() { }
    //public void walk2 { }
    public void walk3(int a) { }
    //public void walk4(int a; int b) { }
    public void walk5(int a, int b) { }

    //Optional Exception List
        /*
    //The standard main method demonstrates 1, 2, 3, 4, 5, 6 and 8 of the elements listed above.
    public static void main(String[] args) {
        System.out.println("Hello and welcome to MethodStructureDemo!");

    }
    */

    //This method demonstrates all 1, 2, 3, 4, 5, 6, 7 and 8 of the elements listed above.
    //It's also valid to Exceptions thrown from the main method. JH - demo
    public static void readFile(String fileName) throws Exception{
        //throw new FileNotFoundException("There's no point to this exception at all! There is no file to find!");
        BufferedReader br = null;
        FileReader fr = null;

        fr = new FileReader(fileName);
        br = new BufferedReader(fr);

        String sCurrentLine;

        while ((sCurrentLine = br.readLine()) != null) {
            System.out.println(sCurrentLine);
        }
    }

    //Method body
    //Try to see which of the following compile.
    public void walk80() { }
    //public void walk81()
    public void walk82(int a) { }


}
