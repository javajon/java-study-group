package methodstructure;

public abstract class AbstractAnimal {
    String name;
    String type;

    public AbstractAnimal(String name, String type) {
        this.name = name;
        this.type = type;
    }

    //Method body is not declared - this is deferred to a concrete implementation.
    public abstract void eat();

}
